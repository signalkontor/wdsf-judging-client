﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CommonLibrary;

namespace Chairman_Client.Model
{

    public class Reduction : System.ComponentModel.INotifyPropertyChanged
    {
        public string Title { get; set; }
        public double Points { get; set; }
        public string NiceTitle
        {
            get { return Title + " (" + Points + ")"; }
        }
        private bool _selected;
        public bool Selected
        {
            set { _selected = value;
                RaiseEvent("Selected");
            }
            get { return _selected; }
        }
        
        public override bool Equals(object obj)
        {
            if (obj is Reduction)
            {
                var r = (Reduction) obj;
                return r.Title == this.Title;
            }
            else
            {
                // Wenn es nicht von dieesem Typ ist, kann es nicht gleich sein.
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Title.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0} ({1})", Title, Points);
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }
        #endregion
    }


    public class Context : System.ComponentModel.INotifyPropertyChanged
    {
        public List<Reduction> Reductions { get; set; }
        public List<CoupleMarking> Markings { get; set; }
        public CoupleMarking Current { get; set; }

        private int _currentIndex = 0;

        public Context()
        {
            Reductions = new List<Reduction>();
            Markings = new List<CoupleMarking>();            
        }

        public void LoadReductions(System.IO.StreamReader file)
        {
            if (file == null) return;
            while (!file.EndOfStream)
            {
                var data = file.ReadLine().Split(';');
                if (data.Length == 2)
                {
                    var reduction = new Reduction() {Title = data[0], Points = data[1].ParseString()};
                    reduction.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(reduction_PropertyChanged);
                    Reductions.Add(reduction);
                }
            }
        }

        void reduction_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is Reduction)
            {
                var r = (Reduction) sender;
                if (r.Selected)
                {
                    Current.AddReduction(r);
                }
                else
                {
                    Current.RemoveReduction(r);
                }
            }
        }

        public bool NextCouple()
        {
            _currentIndex++;
            if (_currentIndex == Markings.Count)
                return false;
            Current = Markings[_currentIndex];

            foreach (var reduction in Reductions)
            {
                reduction.Selected = false;
            }

            RaiseEvent("Current");
            return true;
        }

        public void LoadCouples(System.IO.StreamReader reader)
        {
            if (reader == null) return;
            reader.ReadLine(); // Judge Name -> Brauchen wir nicht
            reader.ReadLine(); // Event Name -> Brauchen wir auch nicht
            while (!reader.EndOfStream)
            {
                var data = reader.ReadLine().Split(';');
                if (data.Length > 3)
                {
                    string dance = data[1];
                    for (int i = 3; i < data.Length; i++)
                    {
                        var couple = new CoupleMarking() {Couple = data[i], Dance = dance};
          
                        Markings.Add(couple);
                        
                    }
                }
            }

            Current = Markings[0];
        }

        public void WriteResult(System.IO.StreamWriter writer)
        {
            foreach (var coupleMarking in Markings)
            {
                writer.WriteLine(String.Format("CH;{0};{1};{2}", coupleMarking.Dance, coupleMarking.Couple, coupleMarking.Total));
            }
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }
        #endregion
    }

    public class CoupleMarking : System.ComponentModel.INotifyPropertyChanged
    {
        public string Couple { get; set; }
        public string Dance { get; set; }
        private List<Reduction> Reductions { get; set; }
        
        public  CoupleMarking()
        {
            Reductions = new List<Reduction>();
        }

        public void AddReduction(Reduction reduction)
        {
            if (!Reductions.Contains(reduction))
            {
                Reductions.Add(reduction);
                RaiseEvent("Total");
            }
        }

        public void RemoveReduction(Reduction reduction)
        {
            if (Reductions.Contains(reduction))
            {
                Reductions.Remove(reduction);
                RaiseEvent("Total");
            }
        }

        public double Total { 
            get
            {
                var total = Reductions.Sum(p => p.Points);
                return total;
            }
        }
    
        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler  PropertyChanged;

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }
        #endregion
    }
}
