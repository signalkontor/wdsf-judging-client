﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComentatorClient
{
    public class ResultData
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public int Place { get; set; }
        public double Points { get; set; }
    }

    public class CoupleResult
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public double Point1 { get; set; }
        public double Point2 { get; set; }
        public double Point3 { get; set; }
        public double Point4 { get; set; }
        public double Points { get; set; }
        public int Place { get; set; }
    }
}
