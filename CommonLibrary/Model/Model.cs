﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;

namespace CommonLibrary.Model
{
    /// <summary>
    /// Represents a heat
    /// </summary>
    public class Heat
    {
        /// <summary>
        /// Gets or sets the component1.
        /// </summary>
        /// <value>
        /// The component1.
        /// </value>
        public int Component1 { get; set; }
        
        public int Component2 { get; set; }
        
        public int[] Couples { get; set; }    
    }

    public class Dance
    {
        public string LongName { get; set; }

        public string ShortName { get; set; }

        public Heat[] Heats { get; set; }
    }

    public class Component
    {
        public string ShortName { get; set; }

        public string LongName { get; set; }

        public string HelpText { get; set; }
    }

    public class ClientModel
    {
        public string Version { get; set; }

        public bool IsShowdance { get; set; }

        public string DataPath { get; set; }

        public Dance[] Dances { get; set; }

        public Component[] Components { get; set; }

        public string Judge { get; set; }

        public string Sign { get; set; }

        public double MinScore { get; set; }

        public double MaxScore { get; set; }

        public double Intervall { get; set; }
    }
}
