﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVGraphics.Models
{
    public class CoupleResultModel
    {
        public string NameMan { get; set; }
        public string NameWoman { get; set; }
        public string Country { get; set; }
        public double MarkA { get; set; }
        public double MarkB { get; set; }
        public double MarkC { get; set; }
        public double MarkD { get; set; }
        public double MarkE { get; set; }
        public double Sum { get; set; }
        public string Place { get; set; }
    }
}
