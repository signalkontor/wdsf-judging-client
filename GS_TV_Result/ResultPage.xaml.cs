﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace TVGraphics
{
    /// <summary>
    /// Interaction logic for ResultPage.xaml
    /// </summary>
    public partial class ResultPage : Page
    {
        public ResultPage(string title, List<Models.ListResultElement> model)
        {
            InitializeComponent();

            for (int i = 0; i < model.Count; i++)
            {
                CreateLine(i, model[i]);
            }

            this.Title.Content = title;
        }

        public static UIElement cloneElement(UIElement orig)
        {

            if (orig == null)

                return (null);

            string s = XamlWriter.Save(orig);

            StringReader stringReader = new StringReader(s);

            XmlReader xmlReader = XmlTextReader.Create(stringReader, new XmlReaderSettings());

            return (UIElement)XamlReader.Load(xmlReader);

        }

        private void CreateLine(int index, Models.ListResultElement element)
        {
            double top = 300 + (index * 65);

            var place = (Label) cloneElement(this.TempPlace);
            place.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(place);
            Canvas.SetTop(place, top);
            place.Content = element.Place + ".";
            // 
            var points = (Label)cloneElement(this.TempSum);
            points.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(points);
            Canvas.SetTop(points, top);
            points.Content = element.Sum > 0 ? String.Format("{0:#0.00}", element.Sum) : "";

            var couple = (Label)cloneElement(this.TempCouple);
            couple.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(couple);
            Canvas.SetTop(couple, top);
            couple.Content = element.NameMan + " - " + element.NameWoman;
            if (element.Sum == 0)
            {
                points.Content = "";
                Canvas.SetLeft(couple, 60);
            }

            var country = (Label)cloneElement(this.TempCountry);
            country.Visibility = System.Windows.Visibility.Visible;
            MainCanvas.Children.Add(country);
            Canvas.SetTop(country, top);
            country.Content = element.Country;

        }
    }
}
