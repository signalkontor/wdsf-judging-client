﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace GrandSlamTransmitter.Model
{
    public class Competition
    {

        public int Id { get; set; }

        public string Title { get; set; }

        public string State { get; set; }

        public string WDSF_Id { get; set; }

        public bool CheckinAllowed { get; set; }

        public bool TPSAllowed { get; set; }

        public Event Event { get; set; }

        public List<Competitor> Couples { get; set; }

        public List<string> Dances { get; set; }

        public List<OfficalWithRole> Judges { get; set; }

        public bool TransmitToWDSF { get; set; }

        public bool IsFormation { get; set; }

        public double WeightA { get; set; }
        public double WeightB { get; set; }
        public double WeightC { get; set; }
        public double WeightD { get; set; }

    }

}
