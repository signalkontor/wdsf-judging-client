﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Wdsf.Api.Client.Models;

namespace GrandSlamTransmitter.Transmitter
{
    class TeamTransmitter : ResultTransmitAbstract
    {

        public TeamTransmitter(string user, string password)
            : base(user, password)
        {
        
        }

        protected override void UpdateResults(Model.Competition TPScompetition, CompetitionDetail competition, bool clearBeforeSending)
        {
            // now load the registered couples from the database
            var teamsRegistered = apiClient.GetTeamParticipants(competition.Id);

            if (clearBeforeSending)
            {
                // delete all registered teams:
                foreach (var c in teamsRegistered)
                {
                    apiClient.DeleteTeamParticipant(c.Id);
                }
                teamsRegistered.Clear();
            }

            if (teamsRegistered.Count == 0)
            {
                this.RegisterTeams(TPScompetition, competition);
                teamsRegistered = apiClient.GetTeamParticipants(competition.Id);
            }

            var registeredTeams = new List<ParticipantTeamDetail>();
            foreach (var team in teamsRegistered)
            {
                Console.WriteLine("Loading Details for for {0}", team.Name);
    
                var paricipant = apiClient.GetTeamParticipant(team.Id);
                registeredTeams.Add(paricipant);
            }

            foreach (var team in TPScompetition.Couples)
            {
                Console.WriteLine("Sending Results for {0}", team.Couple.NiceName);
                var wdsf_team = registeredTeams.SingleOrDefault(r => r.StartNumber == team.Number);
                if (wdsf_team == null)
                {
                    Logger.Instance.Log(Logger.Level.Error, "Team " + team.Number + " not registered at WDSF database!");
                    continue;
                }
                wdsf_team.Rank = team.PlaceFrom.ToString();
                wdsf_team.Points = team.TotalMarking.ToString();

                this.UpdateMarks(wdsf_team, team, TPScompetition);

                var isSuccess = apiClient.UpdateTeamParticipant(wdsf_team);
                if (!isSuccess)
                {
                    Logger.Instance.Log(Logger.Level.Error, apiClient.LastApiMessage);
                }
            }
        }

        protected void UpdateMarks(ParticipantTeamDetail couple, Model.Competitor competitor, Model.Competition competition)
        {
            
            if (competitor.Results == null)
            {
                return; // keine Daten
            }

            var round = couple.Rounds.SingleOrDefault(r => r.Name == this._round);
            if (round == null)
            {
                round = new Round() { Name = this._round };
                couple.Rounds.Add(round);
            }
            else
            {
                round.Dances.Clear();
            }

            var results = competitor.Results["F"]; // As a tradition, all results of a round are stored as 'F'

            foreach (var dance in competition.Dances)
            {
                var danceresult = new Dance() { Name = this.translateDanceName[dance] };

                round.Dances.Add(danceresult);

                var list = results.Where(m => m.Dance == dance);

                foreach (var mark in list)
                {
                    var offical = this._officals.SingleOrDefault(o => o.AdjudicatorChar == mark.Judge.WDSFSign);
                    Score score;

                    if (offical == null)
                    {
                        MessageBox.Show("Official with WDSF ID " + mark.Judge.WDSFSign + " not found");
                        return;
                    }

                    score = new OnScale2Score
                    {
                        OfficialId = offical.Id,
                        TQ = (decimal)mark.MarkA,
                        MM = (decimal)mark.MarkB,
                        PS = (decimal)mark.MarkC,
                        CP = (decimal)mark.MarkD,
                        // Reduction currently not used
                        Reduction = 0
                    };

                    danceresult.Scores.Add(score);
                }
            }
        }

        private void RegisterTeams(Model.Competition competition, CompetitionDetail wdsfCompetition)
        {
            foreach (var couple in competition.Couples)
            {
                if (couple.Couple.WDSFID == "")
                {
                    Logger.Instance.Log(Logger.Level.Error, "No WDSF Id for team " + couple.Couple.NiceName);
                    continue;
                }

                var participant = new ParticipantTeamDetail()
                {
                    CompetitionId = wdsfCompetition.Id,
                    TeamId = Int32.Parse(couple.Couple.WDSFID),
                    StartNumber = couple.Number
                };

                try
                {
                    apiClient.SaveTeamParticipant(participant);
                }
                catch (Exception ex)
                {
                    Logger.Instance.Log(ex);
                }
            }
        }
    }
}
