﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GrandSlamTransmitter.Model;


namespace GrandSlamTransmitter.Transmitter
{
    static class TransmitterFactory
    {
        public static ResultTransmitAbstract GetTransmitter(Competition competition, string user, string password)
        {
            if (competition.IsFormation)
            {
                return new TeamTransmitter(user, password);
            }

            return new CoupleTransmitter(user, password);
        }
    }
}
