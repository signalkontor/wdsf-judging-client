﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Js3APIServer.Models;

namespace Js3APIServer.Controllers
{
    public class Js3ApiController : ApiController
    {

        [System.Web.Mvc.HttpPost]
        [System.Web.Http.Route("Js3Api/Results")]
        public ActionResult Results(Score[] data)
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Http.Route("Js3Api/State")]
        public ActionResult State(State data)
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("Js3Api/GetData")]
        public HeatData GetData(string pin)
        {
            return Models.HeatDataFactory.GetHeatData();
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route("Js3Api/GetCommand")]
        public string GetCommand()
        {
            return "SETTIMER 00:01:30";
        }
    }
}