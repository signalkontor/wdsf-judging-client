﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Js3APIServer.Models
{
    public class State
    {
        public double[] scores { get; set; }

        public double[] subScores { get; set; }

        public int currentHeatIndex { get; set; }

        public int currentDanceIndex { get; set; }
    }

    public class Heat
    {
        public string name { get; set; }

        public int[] couples { get; set; }

        public double[] scores { get; set; }

        public double[] subScores { get; set; }
    }

    public class Dance
    {
        public string danceName { get; set; }

        public string danceShortName { get; set; }

        public Heat[] heats;

        public string componentName { get; set; }

        public int componentIndex { get; set; }
    }


    public class HeatData
    {
        public string name { get; set; }

        public string sign { get; set; }
        public Dance[] dances { get; set; }        
    }

    public class Score
    {
        public string sign { get; set; }

        public int couple { get; set; }

        public double score { get; set; }

        public int component { get; set; }
    }

    public static class HeatDataFactory
    {
        public static HeatData GetHeatData()
        {
            var data = new HeatData
            {
                name = "Peter Mueller",
                sign = "AB",
                dances = new Dance[2]
                {
                    new Dance()
                    {
                        componentName = "MM",
                        componentIndex = 3,
                        danceName = "Slow Waltz",
                        danceShortName = "SW",
                        heats = new Heat[2]
                        {
                            new Heat()
                            {
                                name = "1. SW",
                                couples = new[] {1, 2, 3, 4},
                                scores = GetInitializedArray(4),
                                subScores = GetInitializedArray(4)
                            },
                            new Heat()
                            {
                                name = "2. SW",
                                couples = new[] {5, 6, 7, 8},
                                scores = GetInitializedArray(4),
                                subScores = GetInitializedArray(4)
                            }
                        }
                    },
                    new Dance()
                    {
                        componentName = "TQ",
                        componentIndex = 1,
                        danceName = "Tango",
                        danceShortName = "TG",
                        heats = new Heat[2]
                        {
                            new Heat()
                            {
                                name = "1. SW",
                                couples = new[] {1, 2, 3, 4},
                                scores = GetInitializedArray(4),
                                subScores = GetInitializedArray(4)
                            },
                            new Heat()
                            {
                                name = "2. SW",
                                couples = new[] {5, 6, 7, 8},
                                scores = GetInitializedArray(4),
                                subScores = GetInitializedArray(4)
                            }
                        }
                    },
                }
            };


            return data;
        }

        private static double[] GetInitializedArray(int size)
        {
            var data = new double[size];

            for (var i = 0; i < size; i++)
            {
                data[i] = -1;
            }

            return data;
        }
    }
}