﻿(function(global) {
    System.config({

        defaultJSExtensions: true,

        map: {

            "/app/": "app",

            "angular": "/Scripts/angular.min.js",
            "angular-route": "/Scripts/angular-route.js",
            "jquery": "/Scripts/jquery-3.1.1.js",
            //"app.modules": "/app/app.modules.js",
            //"app.routes": "/app/app.routes.js",
            //"app.interfaces": "/app/app.interfaces.js",
            //"/app/app.controllers" : "/app/app.controllers.js",
            //"app.js": "/app/app.js"
        },

        packages: {
            app: {
                
                defaultExtension: 'js',
                format: "register",
            },
            rxjs: {
                defaultExtension: 'js'
            }
        }
    });
})(this);