"use strict";
var Interfaces = require("./app.interfaces");
var State = Interfaces.Interfaces.State;
var Score = Interfaces.Interfaces.Score;
var TsDemoController = (function () {
    function TsDemoController($scope, $http) {
        this.myScope = $scope;
        this.httpService = $http;
        this.timerRunning = false;
        if (this.tryRecoverData()) {
            window.setTimeout(this.setCouplesJudgedIndicator.bind(this), 1000);
        }
        window.setTimeout(this.timerThread.bind(this), 1000);
        $("#confirmButton").hide();
    }
    TsDemoController.prototype.tryRecoverData = function () {
        if (window.localStorage.getItem("JS3Data") !== null) {
            console.debug("Recovering Data");
            this.data = JSON.parse(window.localStorage.getItem("JS3Data"));
            var state = JSON.parse(window.localStorage.getItem("JS3State"));
            this.danceIndex = state.currentDanceIndex;
            this.heatIndex = state.currentHeatIndex;
            this.component = this.data.dances[this.danceIndex].componentName;
            this.judgeName = this.data.name;
            this.couplesInHeat = this.data.dances[this.danceIndex].heats[this.heatIndex].couples;
            if (this.couplesInHeat == null) {
                console.debug("ist null");
            }
            this.selectedCouple = -1;
            this.coupleSelected(this.couplesInHeat[0]);
            this.setCurrentHeatText();
            $("#coupleContainer").show();
            $("#scoreContainer").show();
            $("#score0to75Container").show();
            $("#loginBox").hide();
            return true;
        }
        return false;
    };
    TsDemoController.prototype.logon = function () {
        this.loadData();
    };
    TsDemoController.prototype.setCurrentHeatText = function () {
        this.currentHeat = this.data.dances[this.danceIndex].danceName + " Heat " + (this.heatIndex + 1);
    };
    TsDemoController.prototype.loadData = function () {
        var _this = this;
        this.httpService.get("/Js3Api/GetData?pin=" + this.pin).then(function (res) {
            _this.data = res.data;
            _this.danceIndex = 0;
            _this.heatIndex = 0;
            _this.component = _this.data.dances[_this.danceIndex].componentName;
            _this.judgeName = _this.data.name;
            _this.couplesInHeat = _this.data.dances[_this.danceIndex].heats[_this.heatIndex].couples;
            _this.selectedCouple = -1;
            _this.coupleSelected(_this.couplesInHeat[0]);
            _this.scoreCount = 0;
            for (var i = 0; i < _this.data.dances.length; i++) {
                for (var h = 0; h < _this.data.dances[i].heats.length; h++) {
                    _this.scoreCount += _this.data.dances[i].heats[h].couples.length;
                }
            }
            _this.setCurrentHeatText();
            $("#coupleContainer").show();
            $("#scoreContainer").show();
            $("#score0to75Container").show();
            $("#loginBox").hide();
        });
    };
    TsDemoController.prototype.coupleSelected = function (couple) {
        if (this.selectedCouple > -1) {
            this.removeCoupleSelected(this.selectedCouple);
        }
        this.selectedCouple = couple;
        this.markCoupleSelected(this.selectedCouple);
        var index = this.couplesInHeat.indexOf(this.selectedCouple);
        if (this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index] > -1) {
            this.unselectAllScoreButtons();
            this.selectScoreButton(this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index]);
        }
        else {
            this.unselectAllScoreButtons();
        }
        if (this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index] > -1) {
            this.unselectAllSubScoreButtons();
            this.selectSubScoreButton(this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index]);
        }
        else {
            this.unselectAllSubScoreButtons();
        }
    };
    TsDemoController.prototype.scoreSelected = function (score) {
        console.log("Score selected: " + score);
        var index = this.couplesInHeat.indexOf(this.selectedCouple);
        this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index] = score;
        this.unselectAllScoreButtons();
        this.selectScoreButton(score);
        this.checkCoupleJudged();
        this.storeState();
    };
    TsDemoController.prototype.subScoreSelected = function (subscore) {
        console.log("Sub-Score selected: " + subscore);
        var index = this.couplesInHeat.indexOf(this.selectedCouple);
        this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index] = subscore;
        this.unselectAllSubScoreButtons();
        this.selectSubScoreButton(subscore);
        this.checkCoupleJudged();
        this.storeState();
    };
    TsDemoController.prototype.confirm = function () {
        var _this = this;
        var scores = new Array(this.scoreCount);
        var scoreIndex = 0;
        for (var i = 0; i < this.data.dances.length; i++) {
            for (var h = 0; h < this.data.dances[i].heats.length; h++) {
                for (var c = 0; c < this.data.dances[i].heats[h].couples.length; c++) {
                    scores[scoreIndex] = new Score();
                    scores[scoreIndex].component = this.data.dances[i].componentIndex;
                    scores[scoreIndex].dance = this.data.dances[i].danceShortName;
                    scores[scoreIndex].couple = this.data.dances[i].heats[h].couples[c];
                    scores[scoreIndex].score = this.data.dances[i].heats[h].scores[c] + this.data.dances[i].heats[h].subScores[c];
                    scores[scoreIndex].sign = this.data.sign;
                    scoreIndex++;
                }
            }
        }
        this.httpService.post("/Js3Api/Results", scores)
            .then(function (response) { return _this.handlerResponded(response, _this.data.dances[_this.danceIndex].heats[_this.heatIndex].scores); });
        this.heatIndex++;
        if (this.heatIndex >= this.data.dances[this.danceIndex].heats.length) {
            this.heatIndex = 0;
            this.danceIndex++;
            if (this.danceIndex >= this.data.dances.length) {
                // navigate to some other place
                document.location.replace("done.html");
                return;
            }
        }
        this.component = this.data.dances[this.danceIndex].componentName;
        this.couplesInHeat = this.data.dances[this.danceIndex].heats[this.heatIndex].couples;
        this.selectedCouple = -1;
        this.setCurrentHeatText();
        this.storeState();
        this.coupleSelected(this.couplesInHeat[0]);
        $("#confirmButton").fadeToggle(300);
    };
    TsDemoController.prototype.handlerResponded = function (response, scores) {
        console.log(response);
    };
    TsDemoController.prototype.checkHeatIsJudged = function () {
        for (var i = 0; i < this.couplesInHeat.length; i++) {
            if (this.data.dances[this.danceIndex].heats[this.heatIndex].scores[i] < 0 || this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[i] < 0) {
                console.log("Index: " + i + " score " + this.data.dances[this.danceIndex].heats[this.heatIndex].scores[i] + " subscore " + this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[i]);
                return;
            }
        }
        // All are judges, we show the button
        $("#confirmButton").fadeIn();
    };
    TsDemoController.prototype.unselectAllScoreButtons = function () {
        $(".selected").removeClass("selected");
    };
    TsDemoController.prototype.unselectAllSubScoreButtons = function () {
        $(".subScoreSelected").removeClass("subScoreSelected");
    };
    TsDemoController.prototype.selectSubScoreButton = function (score) {
        $("#subscore" + (score * 100)).addClass("subScoreSelected");
    };
    TsDemoController.prototype.selectScoreButton = function (score) {
        var button = $("#score" + score);
        console.log(button);
        button.addClass("selected");
    };
    TsDemoController.prototype.setCouplesJudgedIndicator = function () {
        if (this.couplesInHeat == null) {
            window.setTimeout(this.setCouplesJudgedIndicator.bind(this), 1000);
            return;
        }
        for (var _i = 0, _a = this.couplesInHeat; _i < _a.length; _i++) {
            var couple = _a[_i];
            this.selectedCouple = couple;
            this.checkCoupleJudged();
        }
        this.coupleSelected(this.couplesInHeat[0]);
    };
    TsDemoController.prototype.checkCoupleJudged = function () {
        var index = this.couplesInHeat.indexOf(this.selectedCouple);
        if (this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index] > -1 && this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index] > -1) {
            this.markCoupleJudged(this.selectedCouple);
            this.checkHeatIsJudged();
        }
    };
    TsDemoController.prototype.storeState = function () {
        var state = new State();
        // todo: fix this to match new data structure
        state.scores = this.data.dances[this.danceIndex].heats[this.heatIndex].scores;
        state.subScores = this.data.dances[this.danceIndex].heats[this.heatIndex].subScores;
        state.currentDanceIndex = this.danceIndex;
        state.currentHeatIndex = this.heatIndex;
        window.localStorage.setItem("JS3State", JSON.stringify(state));
        window.localStorage.setItem("JS3Data", JSON.stringify(this.data));
        // send this to the server now:
        this.httpService.post("/Js3Api/State", state);
    };
    TsDemoController.prototype.restoreState = function () {
        if (window.localStorage.getItem("JS3State") !== null) {
            var state = JSON.parse(window.localStorage.getItem("JS3State"));
            // todo: this mus be done new...
            this.data.dances[this.danceIndex].heats[this.heatIndex].scores = state.scores;
            this.data.dances[this.danceIndex].heats[this.heatIndex].subScores = state.subScores;
        }
    };
    TsDemoController.prototype.markCoupleJudged = function (couple) {
        $("#couple" + couple).addClass("coupleJudged");
    };
    TsDemoController.prototype.markCoupleSelected = function (couple) {
        $("#couple" + couple).addClass("coupleSelected");
    };
    TsDemoController.prototype.removeCoupleSelected = function (couple) {
        $("#couple" + couple).removeClass("coupleSelected");
    };
    TsDemoController.prototype.timerThread = function () {
        var _this = this;
        try {
            if (this.timerRunning) {
                this.timerSeconds -= 1;
                if (this.timerSeconds < 0) {
                    this.timerSeconds = 59;
                    this.timerMinutes -= 1;
                    if (this.timerMinutes < 0) {
                        this.timerRunning = false;
                        this.timerMinutes = 0;
                        this.timerSeconds = 0;
                    }
                }
                this.timerString = this.formatNumber(this.timerMinutes, 2) +
                    ":" +
                    this.formatNumber(this.timerSeconds, 2);
                if (this.timerSeconds < 16 && this.timerMinutes == 0) {
                    $("#timerSpan").addClass("red");
                }
                else {
                    $("#timerSpan").removeClass("red");
                }
            }
            else {
            }
            this.httpService.get("/Js3Api/GetCommand").then(function (res) {
                var command = res.data;
                console.debug(command);
                if (command.toLowerCase() === "clear") {
                    _this.doClear();
                }
                if (command.toLowerCase().indexOf("settimer") > -1 && !_this.timerRunning) {
                    _this.setTimer(command);
                }
            });
        }
        finally {
            window.setTimeout(this.timerThread.bind(this), 1000);
        }
    };
    TsDemoController.prototype.doClear = function () {
        window.localStorage.removeItem("JS3State");
        window.localStorage.removeItem("JS3Data");
    };
    TsDemoController.prototype.setTimer = function (command) {
        var timerValue = command.substring(9).split(":");
        if (timerValue.length < 3) {
            return;
        }
        this.timerRunning = true;
        this.timerMinutes = parseInt(timerValue[1]);
        this.timerSeconds = parseInt(timerValue[2]);
    };
    TsDemoController.prototype.formatNumber = function (number, length) {
        return (Array(length).join("0") + number).slice(-length);
    };
    return TsDemoController;
}());
TsDemoController.$inject = ["$scope", "$http"];
exports.TsDemoController = TsDemoController;
//# sourceMappingURL=app.controllers.js.map