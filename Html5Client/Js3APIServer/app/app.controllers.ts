﻿
import * as Angular from "angular"
import * as Interfaces from "./app.interfaces"
//import $ from "jquery"

import Interfaces1 = Interfaces.Interfaces;
import HeatData = Interfaces.Interfaces.HeatData;
import State = Interfaces.Interfaces.State;
import Score = Interfaces.Interfaces.Score;

export interface IDemoScope extends ng.IScope {

    test: string;
    changeValue();
}


  export class TsDemoController  {

        apiClient: Interfaces1.IJs3Api;

        static $inject = ["$scope", "$http"];

        private myScope: IDemoScope;

        private httpService: ng.IHttpService;

        private data: HeatData;

        private danceIndex: number;

        private heatIndex: number;

        public component: string;

        public judgeName: string;

        public pin: string;

        public currentHeat: string;

        public couplesInHeat: number[];

        private selectedCouple: number;

        private scoreCount: number;

        private timerRunning: boolean;

        public timerMinutes: number;

        public timerSeconds: number;

        public timerString: string;

      constructor($scope: IDemoScope, $http: ng.IHttpService) {

        this.myScope = $scope;
        this.httpService = $http;
        this.timerRunning = false;

        if (this.tryRecoverData()) {
              window.setTimeout(this.setCouplesJudgedIndicator.bind(this), 1000);
        }

          window.setTimeout(this.timerThread.bind(this), 1000);

        $("#confirmButton").hide();
      }

      tryRecoverData(): boolean {

          if (window.localStorage.getItem("JS3Data") !== null) {
              console.debug("Recovering Data");
              this.data = JSON.parse(window.localStorage.getItem("JS3Data"));
              var state: State = JSON.parse(window.localStorage.getItem("JS3State"));
              
              this.danceIndex = state.currentDanceIndex;
              this.heatIndex = state.currentHeatIndex;

              this.component = this.data.dances[this.danceIndex].componentName;
              this.judgeName = this.data.name;

              this.couplesInHeat = this.data.dances[this.danceIndex].heats[this.heatIndex].couples;
              
              if (this.couplesInHeat == null) {
                  console.debug("ist null");
              }
              this.selectedCouple = -1;

              this.coupleSelected(this.couplesInHeat[0]);         

              this.setCurrentHeatText();
              $("#coupleContainer").show();
              $("#scoreContainer").show();
              $("#score0to75Container").show();
              $("#loginBox").hide();

              return true;
          }

          return false;
      }

      logon() {
          this.loadData();
      }

      setCurrentHeatText() {
          this.currentHeat = this.data.dances[this.danceIndex].danceName + " Heat " + (this.heatIndex + 1);
      }

      loadData() {

            this.httpService.get<HeatData>("/Js3Api/GetData?pin=" + this.pin).then((res) => {
                this.data = res.data; 

                this.danceIndex = 0;
                this.heatIndex = 0;

                this.component = this.data.dances[this.danceIndex].componentName;
                this.judgeName = this.data.name;
                this.couplesInHeat = this.data.dances[this.danceIndex].heats[this.heatIndex].couples;
                this.selectedCouple = -1;

                this.coupleSelected(this.couplesInHeat[0]);

                this.scoreCount = 0;
                for (var i = 0; i < this.data.dances.length; i++) {
                    for (var h = 0; h < this.data.dances[i].heats.length;h++) {
                        this.scoreCount += this.data.dances[i].heats[h].couples.length;
                    }
                }

                this.setCurrentHeatText();

                $("#coupleContainer").show();
                $("#scoreContainer").show();
                $("#score0to75Container").show();
                $("#loginBox").hide();
            });        
        }

     
        coupleSelected(couple: number) {

            if (this.selectedCouple > -1) {
                this.removeCoupleSelected(this.selectedCouple);
            }

            this.selectedCouple = couple;
            this.markCoupleSelected(this.selectedCouple);

            var index = this.couplesInHeat.indexOf(this.selectedCouple);
            if (this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index] > -1) {
                this.unselectAllScoreButtons();
                this.selectScoreButton(this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index]);
            } else {
                this.unselectAllScoreButtons();
            }
            if (this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index] > -1) {
                this.unselectAllSubScoreButtons();
                this.selectSubScoreButton(this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index]);
            } else {
                this.unselectAllSubScoreButtons();
            }
        }
      
        scoreSelected(score: number) {
            console.log("Score selected: " + score);
            var index = this.couplesInHeat.indexOf(this.selectedCouple);
            this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index] = score;
            this.unselectAllScoreButtons();
            this.selectScoreButton(score);
            this.checkCoupleJudged();   
            this.storeState();
        }

        subScoreSelected(subscore: number) {
            console.log("Sub-Score selected: " + subscore);
            var index = this.couplesInHeat.indexOf(this.selectedCouple);
            this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index] = subscore;
            this.unselectAllSubScoreButtons();
            this.selectSubScoreButton(subscore);
            this.checkCoupleJudged();
            this.storeState();
        }


        confirm() {

            var scores = new Array<Score>(this.scoreCount);
            var scoreIndex = 0;
            for (var i = 0; i < this.data.dances.length; i++) {
                for (var h = 0; h < this.data.dances[i].heats.length; h++) {
                    for (var c = 0; c < this.data.dances[i].heats[h].couples.length; c++) {
                        scores[scoreIndex] = new Score();
                        scores[scoreIndex].component = this.data.dances[i].componentIndex;
                        scores[scoreIndex].dance = this.data.dances[i].danceShortName;
                        scores[scoreIndex].couple = this.data.dances[i].heats[h].couples[c];
                        scores[scoreIndex].score = this.data.dances[i].heats[h].scores[c] + this.data.dances[i].heats[h].subScores[c];
                        scores[scoreIndex].sign = this.data.sign;

                        scoreIndex++;
                    }
                }
            }

            this.httpService.post("/Js3Api/Results", scores)
                .then((response: any): ng.IPromise<any> => this.handlerResponded(response, this.data.dances[this.danceIndex].heats[this.heatIndex].scores));

            

            this.heatIndex++;
            if (this.heatIndex >= this.data.dances[this.danceIndex].heats.length) {
                this.heatIndex = 0;
                this.danceIndex++;

                if (this.danceIndex >= this.data.dances.length) {
                    // navigate to some other place
                    document.location.replace("done.html");
                    return;
                }
            }

            this.component = this.data.dances[this.danceIndex].componentName;
            this.couplesInHeat = this.data.dances[this.danceIndex].heats[this.heatIndex].couples;
            this.selectedCouple = -1;
            this.setCurrentHeatText();

            this.storeState();

            this.coupleSelected(this.couplesInHeat[0]);

            $("#confirmButton").fadeToggle(300);
        }

        handlerResponded(response: ng.IPromise<void>, scores: number[]): any {
            console.log(response);
        }

        

        checkHeatIsJudged() {
            
            for (var i = 0; i < this.couplesInHeat.length; i++) {
                if (this.data.dances[this.danceIndex].heats[this.heatIndex].scores[i] < 0 || this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[i] < 0) {
                    console.log("Index: " + i + " score " + this.data.dances[this.danceIndex].heats[this.heatIndex].scores[i] + " subscore " + this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[i]);
                    return;
                }
            }
            
            // All are judges, we show the button
            $("#confirmButton").fadeIn();
        }

        private unselectAllScoreButtons() {
            $(".selected").removeClass("selected");
        }

        private unselectAllSubScoreButtons() {
            $(".subScoreSelected").removeClass("subScoreSelected");
        }

        private selectSubScoreButton(score: number) {
            $("#subscore" + (score * 100)).addClass("subScoreSelected");
        }

        private selectScoreButton(score: number) {
            var button = $("#score" + score);
            console.log(button);
            button.addClass("selected");
        }

        private setCouplesJudgedIndicator() {

            if (this.couplesInHeat == null) {
                window.setTimeout(this.setCouplesJudgedIndicator.bind(this), 1000);
                return;
            }

            for (let couple of this.couplesInHeat) {
                this.selectedCouple = couple;
                this.checkCoupleJudged();
            }

            this.coupleSelected(this.couplesInHeat[0]);
        }

        private checkCoupleJudged() {
            var index = this.couplesInHeat.indexOf(this.selectedCouple);
            if (this.data.dances[this.danceIndex].heats[this.heatIndex].scores[index] > -1 && this.data.dances[this.danceIndex].heats[this.heatIndex].subScores[index] > -1) {
                this.markCoupleJudged(this.selectedCouple);
                this.checkHeatIsJudged();
            }
        }

        private storeState() {

            var state = new State();
            // todo: fix this to match new data structure
            state.scores = this.data.dances[this.danceIndex].heats[this.heatIndex].scores;
            state.subScores = this.data.dances[this.danceIndex].heats[this.heatIndex].subScores;
            state.currentDanceIndex = this.danceIndex;
            state.currentHeatIndex = this.heatIndex;

            window.localStorage.setItem("JS3State", JSON.stringify(state));
            window.localStorage.setItem("JS3Data", JSON.stringify(this.data));

            // send this to the server now:
            this.httpService.post("/Js3Api/State", state);
        }

        private restoreState() {

            if (window.localStorage.getItem("JS3State") !== null) {
                var state: State = JSON.parse(window.localStorage.getItem("JS3State"));
                // todo: this mus be done new...
                this.data.dances[this.danceIndex].heats[this.heatIndex].scores = state.scores;
                this.data.dances[this.danceIndex].heats[this.heatIndex].subScores = state.subScores;
            }
        }

        private markCoupleJudged(couple: number) {
            $("#couple" + couple).addClass("coupleJudged");
        }

        private markCoupleSelected(couple: number) {
            $("#couple" + couple).addClass("coupleSelected");
        }

        private removeCoupleSelected(couple: number) {
            $("#couple" + couple).removeClass("coupleSelected");
        }

        private timerThread() {
            try {

                if (this.timerRunning) {
                    this.timerSeconds -= 1;
                    if (this.timerSeconds < 0) {
                        this.timerSeconds = 59;
                        this.timerMinutes -= 1;
                        if (this.timerMinutes < 0) {
                            this.timerRunning = false;
                            this.timerMinutes = 0;
                            this.timerSeconds = 0;
                        }
                    }

                    this.timerString = this.formatNumber(this.timerMinutes, 2) +
                        ":" +
                        this.formatNumber(this.timerSeconds, 2);

                    if (this.timerSeconds < 16 && this.timerMinutes == 0) {
                        $("#timerSpan").addClass("red");
                    } else {
                        $("#timerSpan").removeClass("red");
                    }

                } else {
                    // this.timerString = "";
                }

                

                this.httpService.get<string>("/Js3Api/GetCommand").then((res) => {

                    var command = res.data;
                    console.debug(command);
                    if (command.toLowerCase() === "clear") {
                        this.doClear();
                    }

                    if (command.toLowerCase().indexOf("settimer") > -1 && !this.timerRunning) {
                        this.setTimer(command);
                    }
                });

            } finally {
                window.setTimeout(this.timerThread.bind(this), 1000);
            }
        }

      private doClear() {
        window.localStorage.removeItem("JS3State");
        window.localStorage.removeItem("JS3Data");
      }

      private setTimer(command: string) {
          
          var timerValue = command.substring(9).split(":");
          
          if (timerValue.length < 3) {
              return;
          }

          this.timerRunning = true;
          this.timerMinutes = parseInt(timerValue[1]);
          this.timerSeconds = parseInt(timerValue[2]);
      }

      private formatNumber(number: number, length: number): string {
          return (Array(length).join("0") + number).slice(-length);
      }
    }
