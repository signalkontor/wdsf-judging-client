"use strict";
var Interfaces;
(function (Interfaces) {
    var Score = (function () {
        function Score() {
        }
        return Score;
    }());
    Interfaces.Score = Score;
    var Heat = (function () {
        function Heat() {
        }
        return Heat;
    }());
    Interfaces.Heat = Heat;
    var Dance = (function () {
        function Dance() {
        }
        return Dance;
    }());
    Interfaces.Dance = Dance;
    var HeatData = (function () {
        function HeatData() {
        }
        return HeatData;
    }());
    Interfaces.HeatData = HeatData;
    var State = (function () {
        function State() {
        }
        return State;
    }());
    Interfaces.State = State;
})(Interfaces = exports.Interfaces || (exports.Interfaces = {}));
//# sourceMappingURL=app.interfaces.js.map