﻿export module Interfaces {

    export interface IJs3Api {
        getData();
        sendResult(scores: Array<Score>);
    }

    export class Score {
        sign: string;
        dance: string;
        couple: number;
        score: number;
        component: number;
    }

    export class Heat {
        public name: string;

        public couples: number[];

        public scores: number[];

        public subScores: number[];
    }

    export class Dance {
        public danceName: string;

        public danceShortName: string;

        public heats: Heat[];

        public componentName: string;

        public componentIndex: number;
    }

    export class HeatData {
        public name: string;

        public sign: string;

        public dances: Dance[];
    }

    export class State {
        public scores: number[];

        public subScores: number[];

        public currentHeatIndex: number;

        public currentDanceIndex: number;
    }
}