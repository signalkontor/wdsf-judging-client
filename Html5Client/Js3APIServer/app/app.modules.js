"use strict";
var angular = require("angular");
var app_controllers_1 = require("./app.controllers");
(function () {
    var myModule = angular.module("angularWithTS", []);
    myModule.controller("TsDemoController", (app_controllers_1.TsDemoController));
})();
//# sourceMappingURL=app.modules.js.map