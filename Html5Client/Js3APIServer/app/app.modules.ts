﻿
import * as angular from "angular"
import * as angularRoute from "angular-route"
import * as routes from "./app.routes"
import { TsDemoController } from "./app.controllers"

declare var js3Controller: TsDemoController;

((): void => {
   
    var myModule = angular.module("angularWithTS", []);
    myModule.controller("TsDemoController", (TsDemoController) as any);
})(); 
