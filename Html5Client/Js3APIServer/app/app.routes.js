"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Routes = (function () {
    function Routes() {
    }
    Routes.configureRoutes = function ($routeProvider) {
        $routeProvider.when("/home", { controller: "Controllers.TsDemoController", templateUrl: "/app/views/playlisindex.html", controllerAs: "TsDemoController" });
        $routeProvider.when("/", { controller: "Controllers.TsDemoController", templateUrl: "/app/views/playlisindex.html", controllerAs: "TsDemoController" });
        $routeProvider.otherwise({ redirectTo: "/home" });
    };
    return Routes;
}());
Routes.$inject = ["$routeProvider"];
exports.Routes = Routes;
//# sourceMappingURL=app.routes.js.map