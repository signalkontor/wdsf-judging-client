﻿import * as Angular from "angular"
import * as AngularRoute from "angular-route"

export class Routes {

    static $inject = ["$routeProvider"];

    static configureRoutes($routeProvider: ng.route.IRouteProvider) {
        $routeProvider.when("/home", { controller: "Controllers.TsDemoController", templateUrl: "/app/views/playlisindex.html", controllerAs: "TsDemoController" });
        $routeProvider.when("/", { controller: "Controllers.TsDemoController", templateUrl: "/app/views/playlisindex.html", controllerAs: "TsDemoController" });
        $routeProvider.otherwise({ redirectTo: "/home" });
    }
}
