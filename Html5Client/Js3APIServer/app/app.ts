﻿import * as Modules from "./app.modules"
import * as Interfaces from "./app.interfaces"
import * as Controllers from "./app.controllers"
import * as Routes from "./app.routes"

module Services {

    import Interfaces1 = Interfaces.Interfaces;

    export class Js3ApiClient implements Interfaces1.IJs3Api {

        getData = () => {
            
        }

        sendResult = (scores) => {
            
        }
    }

    alert("Configure Service");
    angular.module("angularWithTS").service("Services.Js3ApiClient", Js3ApiClient);

}

