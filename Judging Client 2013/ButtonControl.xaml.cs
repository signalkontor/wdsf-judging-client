﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Judging_Client
{
    public class ChangedDataEventArgs : EventArgs
    {
        public int Area { get; set; }
        public double NewValue { get; set; }
        public double OldValue { get; set; }
    }

    public class ButtonViewModel : ViewModelBase
    {
        private double value;

        private bool isSelected;

        private readonly Brush backgroundBrushSelected;

        private readonly Brush backgroundBrushUnselected;

        public ButtonViewModel()
        {

            backgroundBrushSelected = (Brush)App.Current.FindResource("ButtonSelectedBrush");
            backgroundBrushUnselected = (Brush)App.Current.FindResource("ButtonUnselectedBrush");

        }

        public double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
                RaisePropertyChanged(() => Value);
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                RaisePropertyChanged(() => IsSelected);
                RaisePropertyChanged(() => BackgroundColorBrush);
            }
        }

        public Brush BackgroundColorBrush
        {
            get
            {
                return isSelected ? backgroundBrushSelected : backgroundBrushUnselected; 
            }
        }
    }

    public delegate void ChangedEventHandler(object sender, ChangedDataEventArgs e);
	/// <summary>
	/// Interaction logic for ButtonControl.xaml
	/// </summary>
	public partial class ButtonControl : UserControl, System.ComponentModel.INotifyPropertyChanged
	{
	    private Brush background0Brush;

        private Brush background25Brush;

        private Brush background50Brush;

        private Brush background75Brush;

        private readonly Brush backgroundBrushSelected;

        private readonly Brush backgroundBrushUnselected;

	    private double decimalValue = 0; 

        public ButtonControl()
        {
            this.InitializeComponent();

            ButtonClickCommand = new RelayCommand<ButtonViewModel>(ButtonClick);

            backgroundBrushSelected = (Brush)App.Current.FindResource("ButtonSelectedBrush");
            backgroundBrushUnselected = (Brush)App.Current.FindResource("ButtonUnselectedBrush");

            buttonsViewModel = new List<ButtonViewModel>();
            LeftColumnValues = new ObservableCollection<ButtonViewModel>();
            RightColumnValues = new ObservableCollection<ButtonViewModel>();

            this.MinimumValue = 0.5;
            this.MaximumValue = 10.0;
            this.Intervall = 0.5;

            this.DataContext = this;

            RenderControl();
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
                "Value", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.0, ValuePropertyChanges){ BindsTwoWayByDefault = true }
            );

        public static readonly DependencyProperty AreaProperty = DependencyProperty.Register(
                "Area", typeof(string), typeof(ButtonControl), new FrameworkPropertyMetadata("", AreaPropertyChanges){ BindsTwoWayByDefault = false }
            );

        public static readonly DependencyProperty MinimumValueProperty = DependencyProperty.Register(
                "MinimumValue", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.5, MinimumValuePropertyChanges) { BindsTwoWayByDefault = false }
            );

        public static readonly DependencyProperty MaximumValueProperty = DependencyProperty.Register(
                "MaximumValue", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.5, MaximumValuePropertyChanges) { BindsTwoWayByDefault = false }
            );

        public static readonly DependencyProperty IntervallProperty = DependencyProperty.Register(
           "Intervall", typeof(double), typeof(ButtonControl), new FrameworkPropertyMetadata(0.5, IntervallValuePropertyChanges) { BindsTwoWayByDefault = false }
       );   

        public double Value
        {
            get { return (double)GetValue(ValueProperty);  }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public string Area
        {
            get { return (string)GetValue(AreaProperty); }
            set { SetValue(AreaProperty, value); }
        }

	    public double MinimumValue
	    {
            get
            {
                return (double)this.GetValue(MinimumValueProperty); 
            }

	        set
	        {
	            SetValue(MinimumValueProperty, value);
	        }
	    }

	    public double Intervall
	    {
	        get
	        {
	            return (double)this.GetValue(IntervallProperty);
	        }
	        set
	        {
	            SetValue(IntervallProperty, value);
	        }
	    }

	    public double MaximumValue
	    {
	        get
	        {
	            return (double)this.GetValue(MaximumValueProperty);
	        }

	        set
	        {
	            SetValue(MaximumValueProperty, value);
	        }
	    }

        public ObservableCollection<ButtonViewModel> LeftColumnValues { get; set; }

        public ObservableCollection<ButtonViewModel> RightColumnValues { get; set; }

        public ICommand ButtonClickCommand { get; set; }

        private List<ButtonViewModel> buttonsViewModel { get; set; }

	    private void CreateModel()
	    {
            // Re-Create the observalble lists:
            LeftColumnValues.Clear();
            RightColumnValues.Clear();
            buttonsViewModel.Clear();

            var value = MaximumValue;
            while (value >= MinimumValue)
            {
                var viewModel = new ButtonViewModel() { IsSelected = false, Value = value };
                LeftColumnValues.Add(viewModel);
                buttonsViewModel.Add(viewModel);

                value--;

            }
	    }

	    private void RenderControl()
	    {
	        foreach (var buttonViewModel in buttonsViewModel)
	        {
	            buttonViewModel.IsSelected = false;
	        }

	        this.decimalValue = this.Value - Math.Floor(this.Value);
            
	        var element = buttonsViewModel.SingleOrDefault(b => Math.Abs(b.Value - Math.Floor(this.Value)) < 0.001);
	        if (element != null)
	        {
	            element.IsSelected = true;
	            this.Background = Brushes.Green;
	        }

            this.Background0Brush = backgroundBrushUnselected;
            this.Background25Brush = backgroundBrushUnselected;
            this.Background50Brush = backgroundBrushUnselected;
            this.Background75Brush = backgroundBrushUnselected;

            this.Background0Brush = Math.Abs(this.decimalValue) < 0.001
	            ? this.backgroundBrushSelected
	            : this.backgroundBrushUnselected;

            this.Background25Brush = Math.Abs(this.decimalValue - 0.25) < 0.001
                ? this.backgroundBrushSelected
                : this.backgroundBrushUnselected;

            this.Background50Brush = Math.Abs(this.decimalValue - 0.5) < 0.001
                ? this.backgroundBrushSelected
                : this.backgroundBrushUnselected;

            this.Background75Brush = Math.Abs(this.decimalValue - 0.75) < 0.001
                ? this.backgroundBrushSelected
                : this.backgroundBrushUnselected;
        }



        void ButtonClick(ButtonViewModel model)
        {
            var floor = Math.Floor(this.Value);

            if (Math.Abs(model.Value - 10) < 0.001)
            {
                // maximum is 10,  so reset the decimal value to 0
                decimalValue = 0;
            }

            if (Math.Abs(model.Value) > -0.1 && Math.Abs(model.Value) < 0.1 && Math.Abs(decimalValue) < 0.01)
            {
                // minimum is 0.25 for technical reasons
                decimalValue = 0.25;
            }

            if (Math.Abs(floor - model.Value) > 0.001)
            {
                SetValue(ValueProperty, model.Value + this.decimalValue);
                HandlePropertyChanged("Value");
                // Set new Background:

                this.Background = Brushes.Green;
            }
            else
            {
                this.Background = Brushes.DarkRed;
                this.Value = -1;
            }

            this.Focus();
            RenderControl();
        }

        public void ResetBackground()
        {
            this.Background = Brushes.DarkRed;
        }

        public Brush Background0Brush
        {
            get { return background0Brush; }
            set
            {
                background0Brush = value;
                this.HandlePropertyChanged(nameof(this.Background0Brush));
            }
        }

        public Brush Background25Brush
	    {
	        get { return background25Brush; }
	        set
	        {
	            background25Brush = value;
	            this.HandlePropertyChanged(nameof(this.Background25Brush));
	        }
	    }

        public Brush Background50Brush
        {
            get { return background50Brush; }
            set
            {
                background50Brush = value;
                this.HandlePropertyChanged(nameof(this.Background50Brush));
            }
        }

        public Brush Background75Brush
        {
            get { return background75Brush; }
            set
            {
                background75Brush = value;
                this.HandlePropertyChanged(nameof(this.Background75Brush));
            }
        }

        private static void ValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var buttonControl = o as ButtonControl;
            if (buttonControl != null)
            {
                buttonControl.RenderControl();
            }
        }

        private static void AreaPropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is ButtonControl)
            {
                ButtonControl bc = (ButtonControl)o;
                bc.AreaLabel.Content = "" + e.NewValue;
                bc.AreaLabel2.Content = "" + e.NewValue;
            }
        }

	    private static void MinimumValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
	    {
	        var buttonControl = o as ButtonControl;
	        if (buttonControl != null)
	        {
                buttonControl.CreateModel();
	            buttonControl.RenderControl();
	        }
	    }

        private static void MaximumValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var buttonControl = o as ButtonControl;
            if (buttonControl != null)
            {
                buttonControl.CreateModel();
                buttonControl.RenderControl();
            }
        }

        private static void IntervallValuePropertyChanges(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var buttonControl = o as ButtonControl;
            if (buttonControl != null)
            {
                buttonControl.CreateModel();
                buttonControl.RenderControl();
            }
        }

        private void HandlePropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(name));
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion

	    private void DecimalButtonClicked(object sender, RoutedEventArgs e)
	    {
	        if (!(sender is Button))
	        {
	            return;
	        }

	        var button = (Button) sender;

	        switch (button.Name)
	        {
                case "Button0":
	                this.decimalValue = 0;
	                break;
                case "Button25":
                    this.decimalValue = 0.25;
                    break;
                case "Button50":
                    this.decimalValue = 0.5;
	                break;
                case "Button75":
                    this.decimalValue = 0.75;
	                break;
	        }

	        var selectedButton = this.buttonsViewModel.FirstOrDefault(b => b.IsSelected);

	        if (selectedButton != null)
	        {
	            if (Math.Abs(selectedButton.Value - 10) < 0.01)
	            {
	                this.decimalValue = 0;
	            }

                if (Math.Abs(selectedButton.Value) < 0.01 && Math.Abs(this.decimalValue) < 0.01)
                {
                    this.decimalValue = 0.25;
                }

                this.Value = selectedButton.Value + this.decimalValue;
	        }
	        else
	        {
	            this.Value = decimalValue;
	        }

            this.RenderControl();
	    }
	}
}