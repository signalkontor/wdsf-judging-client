﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using CommonLibrary;
using Judging_Client.Model;
using Judging_Client.Nancy;
using Application = System.Windows.Application;
using Binding = System.Windows.Data.Binding;
using MessageBox = System.Windows.MessageBox;
using Timer = System.Timers.Timer;

namespace Judging_Client
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private Model.DataModel context = new Model.DataModel();
		System.IO.FileSystemWatcher watcher;
		private System.Timers.Timer timer;
		private TimeSpan _timeLeft;
        private bool _writeState = true;
		private Key lastKey = Key.F6;
		private List<ButtonControl> _buttonControls;
		private object _syncObject;
        private Judging_ClientV1.MainWindowV1 mainWindowV1 = null;
        private System.Collections.Concurrent.ConcurrentQueue<object> _eventQueue;
        private DateTime _lastFileEvent = DateTime.Now;
        private NancyHttpServer nancyHttpServer;

	    private const string RestartCommandLineArg = "restart";

        public MainWindow()
		{
			try
			{
				InitializeComponent();

                MainWindow.DisableWPFTabletSupport();

                // Is another process running???
			    var processes = Process.GetProcessesByName("Judging_Client");

			    if (processes.Count() > 1)
			    {
			        MessageBox.Show("Another process is already running, this process will exit!");
                    this.Close();
			        return;
			    }

                var task = new Task(TryWriteIpAddress);
                task.Start();

				_syncObject = new object();
				AppDomain.CurrentDomain.UnhandledException +=
					new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

				_buttonControls = new List<ButtonControl>();
				_buttonControls.Add(this.ButtonControl1);
				_buttonControls.Add(this.ButtonControl2);
				_buttonControls.Add(this.ButtonControl3);
				_buttonControls.Add(this.ButtonControl4);
				_buttonControls.Add(this.ButtonControl5);
				_buttonControls.Add(this.ButtonControl6);
				_buttonControls.Add(this.ButtonControl7);
				_buttonControls.Add(this.ButtonControl8);
				_buttonControls.Add(this.ButtonControl9);
				_buttonControls.Add(this.ButtonControl10);

                _eventQueue = new System.Collections.Concurrent.ConcurrentQueue<object>();
                
#if DEBUG_LOCAL
                watcher = new System.IO.FileSystemWatcher(@"C:\eJudge\TestingLocal", "*.txt");
#else
			    this.watcher = new FileSystemWatcher(Settings.Default.DataDirectory, "*.txt*");
#endif
				watcher.Changed += new System.IO.FileSystemEventHandler(DataChanged);
				watcher.Created += new System.IO.FileSystemEventHandler(DataChanged);
				watcher.EnableRaisingEvents = true;

			    _writeState = false;

				this.DataContext = context;
				context.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Context_PropertyChanged);

                Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        LockWaiting.Visibility = Visibility.Visible;
                     }));

				HandleNewData(true);
				ReadJudgingData();

				var thread = new System.Threading.Thread(SendPing);
				thread.IsBackground = true;
				thread.Start();

			    _writeState = true;

			    if (Settings.Default.UseHttp)
			    {
                    nancyHttpServer = new NancyHttpServer();
                    nancyHttpServer.StartHost();
                }
            }
            catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
				MessageBox.Show(ex.StackTrace);
				throw ex;
			}
		}

        public DataModel Context
        {
            get { return this.context;  }
        }

		private void SendPing()
		{
			while (true)
			{
				try
				{
					WriteState();
					System.Threading.Thread.Sleep(20000); // sleep 20 sec
				}
				catch (Exception)
				{
				    System.Threading.Thread.Sleep(20000);
				}
			}
		}

	    private void TryWriteIpAddress()
	    {
	        var args = Environment.GetCommandLineArgs();

	        if (args.Length > 1 && args[1] == RestartCommandLineArg)
	        {
                // we are restarting after clear, no need to write IP address.
	            return;
	        }

	        try
	        {
	            if (Settings.Default.UseHttp)
	            {
                    // Male http Request as Ping:
                    var httpClient = new WebClientWithTimeOut();
	                httpClient.DownloadString(new Uri(string.Format("{0}/FileHandler", Settings.Default.ServerBaseUrl)));

                    return;
	            }

	            var ipAddress =
	                NetworkInterface.GetAllNetworkInterfaces()
	                    .SelectMany(ni => ni.GetIPProperties().UnicastAddresses)
	                    .Where(
	                        ua =>
	                        ua.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ua.Address)
	                        && !ua.Address.ToString().StartsWith("169"))
	                    .Select(ua => ua.Address)
	                    .ToList();

	            var fileName = Settings.Default.OutDirectory + "\\AvailableDevices.txt";

	            var stream = new StreamWriter(fileName, true);

	            stream.WriteLine("A;\\\\{0}\\eJudge\\Data;0", ipAddress.First().ToString());

                stream.Close();
	        }
	        catch (Exception ex)
	        {
	            MessageBox.Show("Could not write IP-Address to server: " + ex.Message);
	        }
	    }

		private void ReadJudgingData()
		{
			try
			{
				if (!System.IO.File.Exists(Settings.Default.LocalOutDirectory + "\\Result_" + this.context.Sign + ".txt"))
				{
                    if (File.Exists(Settings.Default.LocalOutDirectory + "\\CurrentState.txt"))
                    {
                        ReadCurrentState();
                        return;
                    }
					// Haben wir Daten
					if (context.Heats != null && context.Heats.Count > 0)
					{
						context.CurrentIndex = 0;
					}

					return;
				}
				var sr = new System.IO.StreamReader(Settings.Default.LocalOutDirectory + "\\Result_" +
													this.context.Sign + ".txt");
				while (!sr.EndOfStream)
				{
					var data = sr.ReadLine().Split(';');

					int couple = int.Parse(data[2]);
					// find the heat:
					var heat =
						context.Heats.SingleOrDefault(h => h.DanceShortName == data[1] && h.Couples.Any(c => c.Couple == data[2]));
					if (heat == null)
					{
						throw new Exception($"could not find heat of couple {data[2]} in dance {data[1]}");
					}
					var j = heat.Couples.Single(c => c.Couple == data[2]);
					if (j == null)
					{
						throw new Exception("Couple with no. " + couple + " not found!");
					}

                    if (context.IsShowdance)
                    {
                        switch (data[3])
                        {
                            case "1": j.Mark = data[4].ParseString();
                                break;
                            case "2": j.Mark2 = data[4].ParseString();
                                break;
                            case "3": j.Mark3 = data[4].ParseString();
                                break;
                            case "4": j.Mark4 = data[4].ParseString();
                                break;
                        }
                    }
                    else
                    {
                        if (j.Mark > -1)
                        {
                            j.Mark2 = data[4].ParseString();
                        }
                        else
                        {
                            j.Mark = data[4].ParseString();
                        }
                    }
                }

                sr.Close();

				// Wir possitionieren nun den Current Index auf das erste Paar
                //// ToDo: wir müssen die aktuelle Heat finden:
                //context.CurrentIndex = 0;
                //foreach (var heat in context.Heats)
                //{
                //    if (heat.Couples.Any(j => j.Mark <= 0))
                //    {
                //        context.CurrentIndex = heat.Id;
                //        return;
                //    }
                //}
                if (File.Exists(Settings.Default.LocalOutDirectory + "\\CurrentState.txt"))
                {
                    ReadCurrentState();
                    return;
                }
				// Still here, no current state, we set index to the first heat
				context.CurrentIndex = 0;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

        public void RestoreWindow()
        {
            this.Dispatcher.Invoke(new Action(Show));
            mainWindowV1 = null;
        }

        private void ReadCurrentState()
        {
            if (context.Heats.Count == 0) return;

            StreamReader inStr = null;

            try
            {
                inStr = new StreamReader(Settings.Default.LocalOutDirectory + "\\CurrentState.txt");
                var data = inStr.ReadLine().Split(';');
                var index = Int32.Parse(data[0]);

                if (index >= context.Heats.Count)
                {
                    MessageBox.Show("CurrentState does not match data");
                    return;
                }

                var heat = context.Heats[index];
                var i = 1;
                while(i < data.Length)
                {
                    var couple = heat.Couples.Single(c => c.Couple == data[i]);
                    couple.Mark = data[i + 1].ParseString();

                    if (heat.Couples.Count == 1)
                    {
                        couple.Mark2 = data[i + 2].ParseString();
                        i++;
                        couple.Mark3 = data[i + 2].ParseString();
                        i++;
                        couple.Mark4 = data[i + 2].ParseString();
                        i++;
                    }

                    i += 2;
                }

                context.CurrentIndex = index;
                inStr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                if(inStr != null) inStr.Close();
            }
        }

		void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			if(e.ExceptionObject is Exception)
			{
				Exception ex = (Exception) e.ExceptionObject;
				MessageBox.Show(ex.Message);
				MessageBox.Show(ex.StackTrace);
			}
		}


		private void WriteState()
		{
		    if (context.CurrentHeat == null)
		    {
		        return;
		    }

		    var task = new Task(() =>
				{
                    string sign;
                    int heat;
                    int danceNo;
                    int area;

				    lock (_syncObject)
				    {
				        sign = context.Sign;
				        heat = context.IsFinished ? context.CurrentHeat.Id + 1 : context.CurrentHeat.Id;
				        danceNo = context.CurrentHeat.DanceNo;
				        area = context.CurrentHeat.JudgementArea;
				    }
				    var data = string.Format("{0};{1};{2};{3};{4};{5}",
				        sign,
				        heat,
				        danceNo,
				        area,
                        SystemInformation.PowerStatus.BatteryLifePercent,
                        SystemInformation.PowerStatus.PowerLineStatus

				    );

                    var file = "State" + context.Judge + ".txt";

                    this.WriteFileToServer(file, data, context.DataPath);

                });

			task.Start();
		}

	    private void WriteFileToServer(string filename, string data, string eventId)
	    {
	        if (Settings.Default.UseHttp)
	        {
	            WriteFileToServerHttp(filename, data, eventId);
	        }
	        else
	        {
	            WriteFileToServerFileSystem(filename, data);
	        }
	    }

        private void WriteFileToServerHttp(string filename, string data, string eventId)
        {
            var httpClient = new WebClientWithTimeOut();

            var url = new Uri(string.Format("{0}/FileHandler/Write/{1}?eventId={2}", Settings.Default.ServerBaseUrl, filename, eventId));
            var errorCount = 0;

            while (true)
            {
                try
                {

                    var result = httpClient.UploadString(url, "POST", data);

                    if (result.StartsWith("OK"))
                    {
                        return;
                    }

                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);

                    errorCount++;
                    if (errorCount > 3)
                    {
                        return;
                    }

                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        private void WriteFileToServerFileSystem(string filename, string data)
        {
            int errorCount = 0;
            StreamWriter wr = null;
            while (wr == null)
            {
                try
                {
                    if (context.DataPath == null) context.DataPath = "";

                    var path = Settings.Default.OutDirectory + "\\" + context.DataPath;

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    
                    wr = new StreamWriter(path + "\\" + filename);
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);
                    errorCount++;
                    if (errorCount > 5)
                    {
                        return;
                    }

                    System.Threading.Thread.Sleep(1000);
                }
            }

            try
            {
               wr.Write(data);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
            }

            wr.Close();
        }

        void Context_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
            if (context == null || context.CurrentHeat == null)
                return;

            if (context.CurrentHeat.Couples == null)
            {
                // MessageBox.Show("Couples null");
                return;
            }

            try
            {
                MarkingChanged(null, null);
                WriteState();
                
                if (e.PropertyName == "CurrentHeat")
                {
                    lock (_syncObject)
                    {
                        Dispatcher.Invoke(new Action(() =>
                        {
                            // MessageBox.Show("lock entered");
                            // We have to rebind our data
                            for (int i = 0; i < _buttonControls.Count; i++)
                            {
                                if (context.CurrentHeat.Couples.Count > i)
                                {
                                    if (context.CurrentHeat.Couples.Count > 1)
                                    {

                                        _buttonControls[i].ResetBackground();
                                        context.CurrentHeat.Couples[i].PropertyChanged += MarkingChanged;
                                        var binding = new Binding("Mark") {Source = context.CurrentHeat.Couples[i]};
                                        _buttonControls[i].SetBinding(ButtonControl.ValueProperty, binding);

                                        _buttonControls[i].Area = context.CurrentHeat.Couples[i].Couple;
                                        _buttonControls[i].Visibility = Visibility.Visible;
                                        _buttonControls[i].MinimumValue = context.MinimumValue;
                                        _buttonControls[i].MaximumValue = context.MaximumValue;
                                        _buttonControls[i].Intervall = context.Intervall;

                                    }
                                    else
                                    {
                                        SetButtonBinding(i, i, "Mark", false);
                                        SetButtonBinding(i +1, i, "Mark2", true);

                                        if (context.IsShowdance)
                                        {
                                            SetButtonBinding(i + 2, i, "Mark3", true);
                                            SetButtonBinding(i + 3, i, "Mark4", true);
                                            i += 3;
                                        }
                                    }
                                }
                                else
                                {
                                    // remove, not needed
                                    if (context.CurrentHeat.Couples.Count > 1 || i > 1)
                                    {
                                        _buttonControls[i].Visibility = Visibility.Collapsed;
                                    }
                                }
                            }
                            int margin = 10;
                            int width = 105;

                            if (context.CurrentHeat.Couples.Count < 9)
                            {
                                margin = 10;
                                width = 118;
                            }
                            if (context.CurrentHeat.Couples.Count < 7)
                            {
                                margin = 30;
                            }

                            foreach (var buttonControl in _buttonControls)
                            {
                                buttonControl.Margin = new Thickness(margin, 0, 0, 0);
                                buttonControl.Width = width;
                            }
                            // Noch anzeigen, was gewertet werden soll:
                            JudgingArea.Content = GetComponentToDisplay();

                            Helpbox.Text =
                                context.HelpStrings[context.Components[context.CurrentHeat.JudgementArea - 1]].Replace(
                                    @"\r\n", "\r\n");
                        }));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
		}

        private void SetButtonBinding(int buttonIndex, int coupleIndex, string propertyPath, bool isSecondComponent)
        {
            _buttonControls[buttonIndex].ResetBackground();
            context.CurrentHeat.Couples[coupleIndex].PropertyChanged += MarkingChanged;
            var binding = new Binding(propertyPath) {Source = context.CurrentHeat.Couples[coupleIndex] };
            _buttonControls[buttonIndex].SetBinding(ButtonControl.ValueProperty, binding);

            _buttonControls[buttonIndex].Area = this.GetButtonControlHeader(context.CurrentHeat, context.CurrentHeat.Couples[coupleIndex].Couple, isSecondComponent, buttonIndex);
            _buttonControls[buttonIndex].Visibility = Visibility.Visible;
            _buttonControls[buttonIndex].MinimumValue = context.MinimumValue;
            _buttonControls[buttonIndex].MaximumValue = context.MaximumValue;
            _buttonControls[buttonIndex].Intervall = context.Intervall;
        }

        private string GetButtonControlHeader(Heat heat, string couple, bool isSecondComponent, int buttonIndex)
	    {
	        if (heat.Couples.Count > 1)
	        {
	            return couple;
	        }

            if (context.IsShowdance)
            {
                return $"{context.Components[buttonIndex]}/{couple}";
            }

	        if (isSecondComponent)
	        {
	            return $"{context.Components[heat.JudgementArea2 - 1]}/{couple}";
	        }

            return $"{context.Components[heat.JudgementArea - 1]}/{couple}";
        }

	    private string GetComponentToDisplay()
	    {
            var result = context.Components[context.CurrentHeat.JudgementArea - 1];

            if (this.context.CurrentHeat.JudgementArea2 > -1)
            {
                result = $"{context.Components[context.CurrentHeat.JudgementArea - 1]} / {context.Components[context.CurrentHeat.JudgementArea2 - 1]}";
            }

	        return result;
	    }

	    private void MarkingChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
	    {
	        // write current heat and marking
	        if (!_writeState) return;

            try
	        {
	            var outStr = new StreamWriter(Settings.Default.LocalOutDirectory + "\\CurrentState.txt");
                outStr.Write(context.CurrentIndex);
                foreach (var couple in context.CurrentHeat.Couples)
                {
                    outStr.Write($";{couple.Couple};{couple.Mark:0.00}");
                    if (context.CurrentHeat.Couples.Count == 1)
                    {
                        outStr.Write($";{couple.Mark2:0.00}");
                        outStr.Write($";{couple.Mark3:0.00}");
                        outStr.Write($";{couple.Mark4:0.00}");
                    }
                }
                outStr.Close();
	        }
	        catch (Exception ex)
	        {

	        }
	    }

        private StreamReader OpenStream(string filename)
        {
            if (!File.Exists(filename)) return null;

            int count = 0;
            System.IO.StreamReader sr = null;
            while (sr == null && count < 30)
            {
                try
                {
                    count++;
                    sr = new StreamReader(filename);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    System.Threading.Thread.Sleep(250);
                }
            }

            return sr;
        }

	    void HandleNewData(bool foundLocal)
		{
			try
			{
                if (File.Exists($"{Settings.Default.DataDirectory}\\dance.txt"))
                {
                    var str = OpenStream(Settings.Default.DataDirectory + "\\dance.txt");
                    str.Close();
                    context.LoadDataJson();


                    Dispatcher.BeginInvoke(
                        (Action)(() =>
                        {
                            if (!foundLocal)
                            {
                                context.CurrentIndex = 0;
                            }

                            LockWaiting.Visibility = Visibility.Hidden;
                        }));

                    return;
                }

                var sr = OpenStream(Settings.Default.DataDirectory + "\\dance.txt");

				if (sr == null)
					return;

                var version = sr.ReadLine();
                if (version == "V1")
                {
                    if (mainWindowV1 != null) return;
                    sr.Close();

                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        mainWindowV1 = new Judging_ClientV1.MainWindowV1(this);
                        mainWindowV1.Show();
                        this.Hide();
                    }));
                    return;
                }

			    if (version == "V2014")
			    {
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        var window = new MainWindow2014();
                        window.Show();
                        this.Hide();
                    }));
                    return;
                }

				context.LoadData(sr);
				sr.Close();
				// Wir suchen die erste noch nicht gewertete Heat heraus
				var newcurrentIndex = 0;

				Dispatcher.BeginInvoke(
					(Action)(() => {
						if(!foundLocal) 
							context.CurrentIndex = newcurrentIndex;
						LockWaiting.Visibility = Visibility.Hidden; }));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
			}
		}

		void HandleNewState()
		{
		    var sr = OpenStream(Settings.Default.DataDirectory + "\\newState.txt");

		    if (sr == null) return;

			string[] data = sr.ReadLine().Split(';');
			sr.Close();
			if (data[0] == "clear")
			{
                if(mainWindowV1 != null)
                    mainWindowV1.CloseWindow();

				// rename local backup and restart the application
			    try
			    {
                    File.Delete(Settings.Default.DataDirectory + "\\newState.txt");
			    }
			    catch (Exception)
			    {
			    }
                try
                {
                    File.Delete(Settings.Default.DataDirectory + "\\dance.txt");
                    File.Delete(Settings.Default.DataDirectory + "\\dance.txt.json");
                }
                catch (Exception)
                {
                }
                try
                {
                    File.Delete(Settings.Default.LocalOutDirectory + "\\CurrentState.txt");
                    var files = Directory.GetFiles(Settings.Default.LocalOutDirectory, "Result_*.txt");
                    foreach (var file in files)
                    {
                        File.Move(file, Settings.Default.LocalOutDirectory + "\\Result-" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt");
                    }
                }
                catch (Exception)
                {
                }
                
				// Start new Instance of Application
				Process.Start(Application.ResourceAssembly.Location, RestartCommandLineArg);
                Dispatcher.Invoke(new Action(Close));

			}
            // When here, set a new state (dance / heat)
            Dispatcher.Invoke(new Action(() => 
			    context.CurrentIndex = Int32.Parse(data[0])
            ));
		}

		void HandleNewTime()
		{
		    var sr = OpenStream(Settings.Default.DataDirectory + "\\time.txt");

			TimeSpan time = TimeSpan.Parse(sr.ReadLine());
			sr.Close();           
			// Nun müssen wir die Uhr starten
			this.Dispatcher.BeginInvoke(
					(Action)(() => { TimeLeft.Content = _timeLeft.ToString("mm\\:ss"); }));

			if (timer != null)
			{
				timer.Enabled = false;
			}
			timer = new Timer(){ AutoReset = false, Enabled = false, Interval = 1000};
			timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);     
			_timeLeft = time;
			timer.Enabled = true;
		}

	    private void HandleNewResult()
	    {
            var sr = OpenStream(Settings.Default.DataDirectory + "\\newResult.txt");

	        if (sr == null) return;

            try
            {
                // Dance;CoupleNo;New Mark
                var data = sr.ReadLine().Split(';');

                var heat =
                    context.Heats.Single(h => h.DanceShortName == data[0] && h.Couples.Any(c => c.Couple == data[1]));
                var judgement = heat.Couples.Single(c => c.Couple == data[1]);
                judgement.Mark = data[2].ParseString();
            }
            catch (Exception ex)
            {
                
            }
	    }

		void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			_timeLeft = _timeLeft.Subtract(new TimeSpan(0, 0, 0, 1));
			this.Dispatcher.BeginInvoke(
				   (Action)(() =>
					   {
						   TimeLeft.Content = _timeLeft.ToString("mm\\:ss");
						   TimeLeft.Foreground = _timeLeft.TotalSeconds < 15 ? new SolidColorBrush(Color.FromArgb(255, 255, 0, 0)) : new SolidColorBrush(Color.FromArgb(255, 208, 208, 208));
					   }));
			timer.Enabled = _timeLeft.TotalSeconds > 0;
		}

        private void HandleQueudFileSystemEvent(FileSystemEventArgs e)
        {
            // Wir haben eine neue Datendatei, die wir nun einladen wollen
            if (e.Name == "dance.txt" || e.Name == "dance.txt.json")
            {
                HandleNewData(false);
            }

            if (e.Name == "time.txt")
            {
                if (mainWindowV1 != null)
                {
                    mainWindowV1.HandleNewTime();
                    return;
                }
                HandleNewTime();
            }

            if (e.Name == "newState.txt")
            {
                if (mainWindowV1 != null)
                {
                    mainWindowV1.HandleNewState();
                }

                HandleNewState();
            }

            if (e.Name == "newResult.txt")
            {
                HandleNewResult();
            }
        }
        

		void DataChanged(object sender, System.IO.FileSystemEventArgs e)
		{
            // File System Watcher fires sometimes more than one event
            // this might lead to unsafe thread conditions. So
            // filter by time of the last event
            var dif1 = _lastFileEvent.Add(new TimeSpan(0, 0, 0, 1, 0));
            var dif = DateTime.Now.Ticks - _lastFileEvent.Ticks;

            // The last event should be at least 1 second ago
            if (dif1 < DateTime.Now)
            {
                Console.WriteLine("Dif: " + dif + ", " + e.FullPath);
                HandleQueudFileSystemEvent(e);
                _lastFileEvent = DateTime.Now;
            }
            else
            {
                // no, this event is some kind to fresh, we do not handle it.
                Console.WriteLine("This was to short: " + dif + ", " + e.FullPath);
            }
		}

		void ConfirmCanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			if (this.context == null || context.CurrentHeat == null)
			{
				e.CanExecute = false;
				return;
			}

			foreach (var couple in context.CurrentHeat.Couples)
			{
				if (couple.Mark <= 0)
				{
					e.CanExecute = false;
					return;
				}

			    if (context.CurrentHeat.Couples.Count == 1 && couple.Mark2 <= 0)
			    {
                    e.CanExecute = false;
			        return;
			    }

                if (context.IsShowdance)
                {
                    if (couple.Mark3 <= 0 || couple.Mark4 < 0)
                    {
                        e.CanExecute = false;
                        return;
                    }
                }
			}

			e.CanExecute = true;

		}

	    private void ConfirmShowDialog(object sender, ExecutedRoutedEventArgs e)
	    {
	        this.ConfirmGrid.Visibility = Visibility.Visible;
	    }

	    private void CancelConfirm(object sender, RoutedEventArgs e)
	    {
	        this.ConfirmGrid.Visibility = Visibility.Hidden;
	    }

        private void Confirm_Executed(object sender, RoutedEventArgs e)
        {
            this.ConfirmGrid.Visibility = Visibility.Hidden;

            // write results async
            var task = new Task(() =>
									 {
                                         // write local backup
                                         try
                                         {
                                             var localwriter = new System.IO.StreamWriter(Settings.Default.LocalOutDirectory + "\\Result_" +
                                                                                          this.context.Sign + ".txt");
                                             context.WriteResult(localwriter);

                                             localwriter.Close();
                                         }
                                         catch (Exception ex)
                                         {
                                             MessageBox.Show(ex.Message);
                                         }

										 try
										 {
										     var filename = "\\Result_" + this.context.Sign + ".txt";
                                             var memoryStream = new MemoryStream();
                                             var stream = new StreamWriter(memoryStream);

											 context.WriteResult(stream);
											 stream.Close();

										     var data = Encoding.Default.GetString(memoryStream.ToArray());
                                             
                                             this.WriteFileToServer(filename, data, context.DataPath);
										 }
										 catch (Exception ex)
										 {

                                         }
									 });

			task.Start();

            if (context.CurrentIndex < context.Heats.Count - 1)
			{
				context.CurrentIndex++;
			}
			else
			{
			    context.IsFinished = true;
                WriteState();
				this.Dispatcher.BeginInvoke(
					(Action)
					(() => { 
						// We are done: lock the UI
						LockThankYou.Visibility = Visibility.Visible;
						BtnConfirm.IsEnabled = false;
						Dance.Content = "";
						Couple.Content = "";					
					}));
			}
		}


		private void Window_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
		{
		    if (lastKey == Key.Q && e.Key == Key.P)
		    {
                this.Close();
		    }
			else
			{
				lastKey = e.Key;
			}
		}

        public static void DisableWPFTabletSupport()
        {
            // Get a collection of the tablet devices for this window.  
            TabletDeviceCollection devices = System.Windows.Input.Tablet.TabletDevices;

            if (devices.Count > 0)
            {
                // Get the Type of InputManager.
                Type inputManagerType = typeof(System.Windows.Input.InputManager);

                // Call the StylusLogic method on the InputManager.Current instance.
                object stylusLogic = inputManagerType.InvokeMember("StylusLogic",
                            BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                            null, InputManager.Current, null);

                if (stylusLogic != null)
                {
                    //  Get the type of the device class.
                    Type devicesType = devices.GetType();

                    // Loop until there are no more devices to remove.
                    int count = devices.Count + 1;

                    while (devices.Count > 0)
                    {
                        // Remove the first tablet device in the devices collection.
                        devicesType.InvokeMember(
                            "HandleTabletRemoved",
                            BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic,
                            null,
                            devices,
                            new object[] { (uint)0 });

                        count--;

                        if (devices.Count != count)
                        {
                            MessageBox.Show("Unable to remove real-time stylus support.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No Stylus Logic found");
                }
            }
        }

	    private void OnCloseButtonClicked(object sender, RoutedEventArgs e)
	    {
	        this.Close();
	    }
	}
}
