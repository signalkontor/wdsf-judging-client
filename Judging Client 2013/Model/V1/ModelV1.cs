﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Judging_Client.ModelV1
{



    public class DataModel : System.ComponentModel.INotifyPropertyChanged
    {

        private Visibility _componentAVisible;
        private Visibility _componentBVisible;
        private Visibility _componentCVisible;
        private Visibility _componentDVisible;
        private Visibility _componentEVisible;

        public System.Windows.Visibility ComponentAVisibility
        {
            get
            {
                return _componentAVisible;
            }
            set
            {
                _componentAVisible = value;
                RaiseEvent("ComponentAVisibility");
            }
        }

        public System.Windows.Visibility ComponentBVisibility
        {
            get
            {
                return _componentBVisible;
            }
            set
            {
                _componentBVisible = value;
                RaiseEvent("ComponentBVisibility");
            }
        }

        public System.Windows.Visibility ComponentCVisibility
        {
            get
            {
                return _componentCVisible;
            }
            set
            {
                _componentCVisible = value;
                RaiseEvent("ComponentCVisibility");
            }
        }

        public System.Windows.Visibility ComponentDVisibility
        {
            get
            {
                return _componentDVisible;
            }
            set
            {
                _componentDVisible = value;
                RaiseEvent("ComponentDVisibility");
            }
        }

        public System.Windows.Visibility ComponentEVisibility
        {
            get
            {
                return _componentEVisible;
            }
            set
            {
                _componentEVisible = value;
                RaiseEvent("ComponentEVisibility");
            }
        }

        public string NameA { get; set; }
        public string NameB { get; set; }
        public string NameC { get; set; }
        public string NameD { get; set; }
        public string NameE { get; set; }

        public Dictionary<string, string> HelpStrings { get; set; }

        public string DataPath { get; set; }

        public bool IsFinished { get; set; }

        private string _judge;
        public string Judge {
            get { return _judge;}
            set{
                _judge = value;
                RaiseEvent("Judge");
            }
        }

        public string Sign { get; set; }
        public string Country { get; set; }

        private string _event;

        public string Event
        {
          get { return _event; }
          set { _event = value;
                RaiseEvent("Event");
          }
        }

        private Judgement _current;

        public Judgement CurrentJudgement
        {
          get { 
              return _current; 
          }
          set { _current = value; 
                RaiseEvent("CurrentJudgement");
          }
        }

        private int _currentIndex;

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if (value >= Judgements.Count)
                {
                    _currentIndex = value;
                    // Do not set CurrentJudgements -> Exception
                    return;
                }
                _currentIndex = value;
                CurrentJudgement = Judgements[_currentIndex];
                RaiseEvent("CurrentIndex");
                RaiseEvent("CurrentJudgement");
            }
        }

        public DataModel()
        {
            // Set Default Values;
            Judgements = new List<Judgement>();
            Judge = "No Judge";
            Event = "No Event";
            Judgements.Add(new Judgement() { Couple = "Not set", DanceName = "not set", DanceNo = 1, MarkA = 4.0 });
            CurrentIndex = 0;
            IsFinished = false;
        }

        public List<Judgement> Judgements {get; set;}

        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        public void 
            LoadData(System.IO.StreamReader sr)
        {
            DataPath = sr.ReadLine();
           
            // Now we load the components, we have ...
            var components = sr.ReadLine().Split(';');
            NameA = components.Length > 0 ? components[0] : "";
            ComponentAVisibility = components.Length > 0 ? Visibility.Visible : Visibility.Hidden;
 
            NameB = components.Length > 1 ? components[1] : "";
            ComponentBVisibility = components.Length > 1 ? Visibility.Visible : Visibility.Hidden;
            
            NameC = components.Length > 2 ? components[2] : "";
            ComponentCVisibility = components.Length > 2 ? Visibility.Visible : Visibility.Hidden;
            
            NameD = components.Length > 3 ? components[3] : "";
            ComponentDVisibility = components.Length > 3 ? Visibility.Visible : Visibility.Hidden;

            NameE = components.Length > 4 ? components[4] : "";
            ComponentEVisibility = components.Length > 4 ? Visibility.Visible : Visibility.Hidden;

            HelpStrings = new Dictionary<string, string>();
            for (int i = 0; i < components.Length; i++)
            {
                var data = sr.ReadLine().Split(';');
                data[1] = data[1].Replace(@"\r\n", "\r\n");
                HelpStrings.Add(data[0], data[1]);

            }

            var judgedata = sr.ReadLine().Split(';');
            Sign = judgedata[0];
            Judge = judgedata[1];
            Country = judgedata[2];
            // Zweite Zeile die Competition
            Event = sr.ReadLine();
            Judgements = new List<Judgement>();

            while (!sr.EndOfStream)
            {
                string[] data = sr.ReadLine().Split(';');
                for (int i = 3; i < data.Length; i++)
                {
                    Judgement ju = new Judgement() { Couple = data[i], DanceNo = Int32.Parse(data[0]), DanceShortName = data[1],DanceName = data[2], MarkA = 0, MarkB = 0, MarkC = 0, MarkD = 0, MarkE = 0 };
                    ju.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Judgement_PropertyChanged);
                    Judgements.Add(ju);
                }
            }
        }

        void Judgement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaiseEvent("Judgement");
        }

        public void WriteResult(System.IO.StreamWriter writer)
        {
            // Wir könnten unsere Netzwerkverbindung verloren haben,
            // Daher schreiben wir zur Sicherheit einfach alle Ergebnisse raus ...
            foreach (var judgement in Judgements)
            {
                writer.Write(Sign + ";" + judgement.DanceShortName + ";" + judgement.Couple + ";");
                writer.WriteLine(String.Format("{0};{1};{2};{3};{4}", judgement.MarkA, judgement.MarkB, judgement.MarkC, judgement.MarkD, judgement.MarkE));
            }
            
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler  PropertyChanged;

        #endregion
    }

    public class Judgement : System.ComponentModel.INotifyPropertyChanged
    {
        private double _markA;

        public double MarkA
        {
            get { 
                return _markA; 
            }
            set 
            { 
                _markA = value;
                RaiseEvent("MarkA");
                RaiseEvent("MarkTotal");
            }
        }
        private double _markB;

        public double MarkB
        {
            get { return _markB; }
            set
            {
                _markB = value;
                RaiseEvent("MarkB");
                RaiseEvent("MarkTotal");
            }
        }
        private double _markC;

        public double MarkC
        {
            get { return _markC; }
            set
            {
                _markC = value;
                RaiseEvent("MarkC");
                RaiseEvent("MarkTotal");
            }
        }
        private double _markD;

        public double MarkD
        {
            get { return _markD; }
            set
            {
                _markD = value;
                RaiseEvent("MarkD");
                RaiseEvent("MarkTotal");
            }
        }
        
        private double _markE;
        public double MarkE
        {
            get { return _markE; }
            set
            {
                _markE = value;
                RaiseEvent("MarkE");
                RaiseEvent("MarkTotal");
            }
        }

        public string Couple { get; set; }
        public string DanceName { get; set; }
        public string DanceShortName { get; set; }
        public int DanceNo { get; set; }

        public double MarkTotal { 
            get
            {
                return MarkA + MarkB + MarkC + MarkD + MarkE;
            }
        }
    
        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler  PropertyChanged;

        #endregion
}
}
