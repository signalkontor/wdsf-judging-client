﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using CommonLibrary;
using CommonLibrary.Model;
using Newtonsoft.Json;
using NLog;
// Model 2013 !

namespace Judging_Client.Model
{
    public class Heat : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public string DanceName { get; set; }
        public string DanceShortName { get; set; }
        public int DanceNo { get; set; }
        public int JudgementArea { get; set; }
        public int JudgementArea2 { get; set; }
        public string DisplayName { get; set; }
        public List<Judgement> Couples { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class DataModel : INotifyPropertyChanged
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private int _currentIndex;
        private string _event;
        private string _judge;

        public DataModel()
        {
            // Set Default Values;
            // Judgements = new List<Judgement>();
            Judge = "No Judge";
            Event = "No Event";
            Heats = new List<Heat>();
            CurrentIndex = 0;
            IsFinished = false;
            MinimumValue = 7.5;
            MaximumValue = 10;
            Intervall = 0.25;
        }

        public string DataPath { get; set; }

        public string Judge
        {
            get { return _judge; }
            set
            {
                _judge = value;
                RaiseEvent("Judge");
            }
        }

        public string Sign { get; set; }
        public string Country { get; set; }
        public Dictionary<string, string> HelpStrings { get; set; }
        public string[] Components { get; set; }

        public bool IsShowdance { get; set; }

        public string Event
        {
            get { return _event; }
            set
            {
                _event = value;
                RaiseEvent("Event");
            }
        }

        public bool IsFinished { get; set; }
        // private Heat _currentHeat;
        public Heat CurrentHeat
        {
            get { return Heats.Count > 0 ? Heats[CurrentIndex] : null; }
        }

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if (value >= Heats.Count)
                {
                    // Do not set CurrentJudgements -> Exception
                    return;
                }
                _currentIndex = value;
                RaiseEvent("CurrentIndex");
                RaiseEvent("CurrentHeat");
            }
        }

        public double MaximumValue { get; set; }
        public double MinimumValue { get; set; }
        public double Intervall { get; set; }
        // public List<Judgement> Judgements {get; set;}

        public List<Heat> Heats { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadDataJson()
        {
            var clientModel =
                JsonConvert.DeserializeObject<ClientModel>(
                    File.ReadAllText(Settings.Default.DataDirectory + "\\dance.txt"));

            DataPath = clientModel.DataPath;
            MinimumValue = clientModel.MinScore;
            MaximumValue = clientModel.MaxScore;
            Intervall = clientModel.Intervall;
            HelpStrings = clientModel.Components.ToDictionary(k => k.ShortName, s => s.HelpText);
            Components = clientModel.Components.Select(c => c.ShortName).ToArray();
            IsShowdance = clientModel.IsShowdance;
            Sign = clientModel.Sign;
            Judge = clientModel.Judge;
            Country = "";
            Event = "";
            Heats = new List<Heat>();

            var totalNumber = 0;

            for (var danceIndex = 0; danceIndex < clientModel.Dances.Count(); danceIndex++)
            {
                var dance = clientModel.Dances[danceIndex];

                for (var heatIndex = 0; heatIndex < dance.Heats.Count(); heatIndex++)
                {
                    totalNumber++;

                    var heat = new Heat
                    {
                        Couples = new List<Judgement>(),
                        JudgementArea = dance.Heats[heatIndex].Component1,
                        JudgementArea2 = dance.Heats[heatIndex].Component2,
                        DanceName = clientModel.Dances[danceIndex].LongName,
                        DanceShortName = clientModel.Dances[danceIndex].ShortName,
                        DanceNo = danceIndex,
                        DisplayName = (heatIndex + 1) + ". Heat",
                        Id = totalNumber
                    };

                    foreach (var couple in dance.Heats[heatIndex].Couples)
                    {
                        heat.Couples.Add(new Judgement
                        {
                            Couple = couple.ToString(),
                            Mark = -1d,
                            Mark2 = -1d
                        });
                    }

                    Heats.Add(heat);
                }
            }

            CurrentIndex = 0;
        }

        public void LoadData(StreamReader sr)
        {
            // Subfolder where to save the data ...
            DataPath = sr.ReadLine();
            // Min, Max and Intervall;
            var separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var minMaxData = sr.ReadLine().Split(';');
            MinimumValue = minMaxData[0].ParseString();
            MaximumValue = minMaxData[1].ParseString();
            Intervall = minMaxData[2].ParseString();
            // Components...
            Components = sr.ReadLine().Split(';');

            logger.Info("Adding Components");

            HelpStrings = new Dictionary<string, string>();
            for (var i = 0; i < Components.Length; i++)
            {
                var data = sr.ReadLine().Split(';');
                data[1] = data[1].Replace(@"\r\n", "\r\n");
                try
                {
                    HelpStrings.Add(data[0], data[1]);
                }
                catch (Exception ex)
                {
                    logger.Error("Could not add Helpstring with Value " + data[0] + ", Message: " + ex.Message);
                }
            }

            logger.Info("Components Added");

            var judgedata = sr.ReadLine().Split(';');
            Sign = judgedata[0];
            Judge = judgedata[1];
            Country = judgedata[2];
            // Zweite Zeile die Competition
            Event = sr.ReadLine();
            Heats = new List<Heat>();

            logger.Info("Reading Heats");

            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');

                var heats = int.Parse(data[3]);
                var index = 4;

                for (var heatIndex = 0; heatIndex < heats; heatIndex++)
                {
                    var heat = new Heat
                    {
                        DanceShortName = data[1],
                        DanceName = data[2],
                        DanceNo = int.Parse(data[0]),
                        Couples = new List<Judgement>(),
                        Id = Heats.Count,
                        DisplayName = (heatIndex + 1) + ". Heat"
                    };

                    Heats.Add(heat);

                    var numCouples = int.Parse(data[index]);
                    heat.JudgementArea = int.Parse(data[index + 1]);

                    index += 2;
                    for (var c = 0; c < numCouples; c++)
                    {
                        var ju = new Judgement {Couple = data[index], Mark = -1d, Mark2 = -1d};
                        ju.PropertyChanged += Judgement_PropertyChanged;
                        heat.Couples.Add(ju);
                        index++;
                    }
                }
            }

            logger.Info("Read Data finished");
        }

        private void Judgement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RaiseEvent("Judgement");
        }

        public void WriteResult(StreamWriter writer)
        {
            // Wir könnten unsere Netzwerkverbindung verloren haben,
            // Daher schreiben wir zur Sicherheit einfach alle Ergebnisse raus ...
            foreach (var heat in Heats)
            {
                foreach (var judgement in heat.Couples)
                {
                    if (IsShowdance)
                    {
                        writer.WriteLine($"{Sign};{heat.DanceShortName};{judgement.Couple};1;{judgement.Mark}");
                        writer.WriteLine($"{Sign};{heat.DanceShortName};{judgement.Couple};2;{judgement.Mark2}");
                        writer.WriteLine($"{Sign};{heat.DanceShortName};{judgement.Couple};3;{judgement.Mark3}");
                        writer.WriteLine($"{Sign};{heat.DanceShortName};{judgement.Couple};4;{judgement.Mark4}");
                    }
                    else
                    {
                        writer.Write(Sign + ";" + heat.DanceShortName + ";" + judgement.Couple + ";");
                        writer.WriteLine($"{heat.JudgementArea};{judgement.Mark}");

                        if (heat.JudgementArea2 < 0) continue;

                        writer.Write(Sign + ";" + heat.DanceShortName + ";" + judgement.Couple + ";");
                        writer.WriteLine($"{heat.JudgementArea2};{judgement.Mark2}");
                    }

                }
            }
        }
    }

    public class Judgement : INotifyPropertyChanged
    {
        private double _mark = -1;
        private double _mark2 = -1;
        private double _mark3 = -1;
        private double _mark4 = -1;

        public double Mark
        {
            get { return _mark; }
            set
            {
                _mark = value;
                RaiseEvent("Mark");
            }
        }

        public double Mark2
        {
            get { return _mark2; }
            set
            {
                _mark2 = value;
                RaiseEvent("Mark");
                RaiseEvent("Mark2");
            }
        }

        public double Mark3
        {
            get { return _mark3; }
            set
            {
                _mark3 = value;
                RaiseEvent("Mark");
                RaiseEvent("Mark3");
            }
        }

        public double Mark4
        {
            get { return _mark4; }
            set
            {
                _mark4 = value;
                RaiseEvent("Mark");
                RaiseEvent("Mark4");
            }
        }

        public string Couple { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void RaiseEvent(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}