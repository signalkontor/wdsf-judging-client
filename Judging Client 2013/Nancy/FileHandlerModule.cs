﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using Judging_Client;

using Nancy;
using Nancy.Extensions;

namespace Judging_Server.Nancy
{
    public class FileHandlerModule : NancyModule
    {
        public FileHandlerModule()
            : base("/FileHandler")
        {
            this.Get["/"] = _ =>
                {
                    // Log the IP Address to local AvailableDevices.txt
                    var source = this.Request.UserHostAddress;

                    return "Htpp Server is running at IP " + source;
                };

            this.Post["/Write/{filename}"] = request =>
                {
                    var filename = request.filename.ToString();

                    var body = this.Request.Body;
                    
                    var folder = Settings.Default.DataDirectory;

                    File.WriteAllText(folder + "\\" + filename, body.AsString());

                    return "OK";
                };
        }
    }
}
