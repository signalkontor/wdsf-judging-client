﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Windows;

using Judging_Server.Nancy;

using Nancy.Hosting.Self;

namespace Judging_Client.Nancy
{
    public class NancyHttpServer
    {
        private NancyHost host;

        private bool isRunning = false;

        public NancyHttpServer()
        {
            var hostConfigs = new HostConfiguration();
            hostConfigs.UrlReservations.CreateAutomatically = true;
            hostConfigs.RewriteLocalhost = true;

            // this is needed in 
            hostConfigs.AllowChunkedEncoding = false;

            var uris = GetUriList(Settings.Default.HttpPort);

            this.host = new NancyHost(new ApplicationBootstrapper(), hostConfigs, uris);
        }

        
        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }
        }

        public void StartHost()
        {
            try
            {
                this.host.Start();
                this.isRunning = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("HTTP Server could not started, please check binding! " + ex.Message);
            }
        }

        public void StopHost()
        {
            this.host.Stop();
            this.isRunning = false;
        }

        public static Uri[] GetUriList(int port)
        {
            var uriParams = new List<Uri>();
            string hostName = Dns.GetHostName();

            // Host name URI
            string hostNameUri = string.Format("http://{0}:{1}", Dns.GetHostName(), port);
            uriParams.Add(new Uri(hostNameUri));

            // Host address URI(s)
            var hostEntry = Dns.GetHostEntry(hostName);
            foreach (var ipAddress in hostEntry.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)  // IPv4 addresses only
                {
                    var addrBytes = ipAddress.GetAddressBytes();
                    string hostAddressUri = string.Format("http://{0}.{1}.{2}.{3}:{4}",
                    addrBytes[0], addrBytes[1], addrBytes[2], addrBytes[3], port);
                    uriParams.Add(new Uri(hostAddressUri));
                }
            }

            // Localhost URI
            uriParams.Add(new Uri(string.Format("http://localhost:{0}", port)));

            return uriParams.ToArray();
        }
    }
}
