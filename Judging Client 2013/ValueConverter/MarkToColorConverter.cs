﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Judging_Client.ValueConverter
{
    class MarkToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double mark)
            {
                if (mark < 2)
                {
                    return Brushes.Red;
                }
            }

            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
