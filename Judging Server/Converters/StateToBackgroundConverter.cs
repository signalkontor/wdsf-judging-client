using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Judging_Server.Model
{
    public class StateToBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            var s = (States)value;
            switch(s)
            {
                case States.Used: return new SolidColorBrush(Colors.White);
                case States.NotUsedMaximum: return new SolidColorBrush(Colors.Red);
                case States.NotUsedMinimum: return new SolidColorBrush(Colors.Yellow);
                case States.NotUsed: return new SolidColorBrush(Colors.Black);
                default: return new SolidColorBrush(Colors.White);
            }
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}