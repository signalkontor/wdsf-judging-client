﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Judging_Server.Model;
using System.Xml.Linq;

namespace Judging_Server.Dialogs
{

    /// <summary>
    /// Interaction logic for OpenCompetition.xaml
    /// </summary>
    public partial class OpenCompetition : Window
    {
        private List<Model.EventData> _events;

        public string SelectedFolder { get; set; }

        public OpenCompetition()
        {
            InitializeComponent();
            LoadData();
            CompetitionList.ItemsSource = _events;
        }

        private void LoadData()
        {
            _events = new List<EventData>();

            var dirs = Directory.GetDirectories(ServerSettings.Default.DataPath);

            foreach (var dir in dirs)
            {
                if (File.Exists(dir + "\\data.txt"))
                {
                    AddCompetition(dir + "\\data.txt");
                }

                if(File.Exists(dir + "\\data.xml"))
                {
                    AddCompetitionXml(dir + "\\data.xml");
                }
            }
        }

        private void AddCompetition(string file)
        {
            var inStr = new StreamReader(file);
            var version = inStr.ReadLine();
            // var markingData = inStr.ReadLine();
            inStr.ReadLine();
            // Read Header:
            var length = Int32.Parse(inStr.ReadLine());
            for (var i = 0; i < length; i++)
            {
                inStr.ReadLine();
            }
            // Now read competition data
            var data = inStr.ReadLine().Split(';');
            var fi = new FileInfo(file);
            var ev = new EventData()
                {
                    Title = data[3],
                    RoundNr = data[2],
                    ScrutinusId = data[1],
                    Created = System.IO.File.GetLastWriteTime(file),
                    Folder = fi.Directory.FullName,
                    Version = version
                };

            _events.Add(ev);
        }

        private void AddCompetitionXml(string file)
        {
            var doc = XDocument.Load(file);

            // Read Event Data
            var root = doc.Element("Data");
            var eventData = root.Element("Event");

            var fileInfo = new FileInfo(file);
            var eventdata = new EventData()
            {
                Title = eventData.Element("Title").Value,
                RoundNr =  eventData.Element("RoundNumber").Value,
                ScrutinusId = eventData.Attribute("Id").Value,
                Created = System.IO.File.GetLastWriteTime(file),
                Folder = fileInfo.Directory.FullName,
                Version = ""
            };

            _events.Add(eventdata);
        }

        private void BtnOK_OnClick(object sender, RoutedEventArgs e)
        {
            if (CompetitionList.SelectedItem == null) return;

            SelectedFolder = ((EventData) CompetitionList.SelectedItem).Folder;

            DialogResult = true;

            Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void CompetitionList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CompetitionList.SelectedItem == null) return;

            SelectedFolder = ((EventData)CompetitionList.SelectedItem).Folder;

            DialogResult = true;

            Close();
        }
    }
}
