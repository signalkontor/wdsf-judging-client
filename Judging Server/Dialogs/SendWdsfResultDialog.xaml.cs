﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Judging_Server.Model;
using Wdsf.Api.Client;
using Wdsf.Api.Client.Models;
using Dance = Judging_Server.Model.Dance;
using MessageBox = System.Windows.MessageBox;

namespace Judging_Server.Dialogs
{
    /// <summary>
    /// Interaction logic for SendWdsfResultDialog.xaml
    /// </summary>
    public partial class SendWdsfResultDialog : Window
    {
        private DataContextClass context;

        private string userName;
        private string password;
        private int droppedOutPlace;

        protected Dictionary<string, string> translateDanceName;

        public SendWdsfResultDialog(DataContextClass context)
        {
            InitializeComponent();

            this.context = context;

            this.translateDanceName = new Dictionary<string, string>
                                          {
                                              { "SW", "WALTZ" },
                                              { "W", "WALTZ" },
                                              { "TG", "TANGO" },
                                              { "VW", "VIENNESE WALTZ" },
                                              { "SF", "SLOW FOXTROT" },
                                              { "QS", "QUICKSTEP" },
                                              { "SB", "SAMBA" },
                                              { "CC", "CHA CHA CHA" },
                                              { "RB", "RUMBA" },
                                              { "PD", "PASO DOBLE" },
                                              { "JV", "JIVE" },
                                              { "SA", "SALSA" },
                                              { "SS", "SHOWDANCE" },
                                              { "SL", "SHOWDANCE" },
                                              { "FL", "FORMATION LATIN" },
                                              { "FS", "FORMATION STANDARD" }
                                          };
        }

        private void OkButtonClicked(object sender, RoutedEventArgs e)
        {
            this.userName = this.WdsfUser.Text;
            this.password = this.WdsfPassword.Password;
            this.droppedOutPlace = int.Parse(this.DroppedOutPlace.Text);

            if (context.IsFormation)
            {
                Task.Factory.StartNew(SendDataTeam);
            }
            else
            {
                Task.Factory.StartNew(SendData);
            }
        }

        private void CancelButtonClicked(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SendData()
        {
            try
            {
                this.SetStatus("Downloading Officials", 0, 0);
#if DEBUG
                var apiClient = new Wdsf.Api.Client.Client(this.userName, this.password, WdsfEndpoint.Services);
#else
                var apiClient = new Wdsf.Api.Client.Client(this.userName, this.password, WdsfEndpoint.Services);
#endif

                var judgesList = apiClient.GetOfficials(int.Parse(context.Event.WDSFId));

                var judges = new List<OfficialDetail>();
                foreach (var official in judgesList)
                {
                    judges.Add(apiClient.GetOfficial(official.Id));
                }

                this.SetStatus("Downloading Couples", 0, 0);

                var couples = apiClient.GetCoupleParticipants(int.Parse(context.Event.WDSFId));
                var index = 0;

                var numbers = this.context.Couples.Select(c => c.Id.ToString());

                foreach (var couple in couples.Where(c => numbers.Contains(c.StartNumber)))
                {
                    var localCouple =
                        this.context.TotalResult.FirstOrDefault(c => c.Couple.Id.ToString() == couple.StartNumber);
                    index++;

                    var coupleDetails = apiClient.GetCoupleParticipant(couple.Id);
                    this.SetStatus("Downloading Couples", index, couples.Count);

                    if (localCouple.Place >= droppedOutPlace)
                    {
                        coupleDetails.Rank = localCouple.Place.ToString();
                        coupleDetails.Points = localCouple.Total.ToString();
                    }
                    else
                    {
                        coupleDetails.Rank = "";
                        coupleDetails.Points = "";
                    }

                    var round = coupleDetails.Rounds.FirstOrDefault(r => r.Name == this.context.Event.RoundNr);
                    if (round == null)
                    {
                        round = new Round()
                        {
                            Name = this.context.Event.RoundNr,
                        };
                        coupleDetails.Rounds.Add(round);
                    }

                    round.MaxDeviation = this.context.Js3CutOffValue.ToString();

                    // round.Dances.Clear();

                    foreach (var dance in this.context.Dances)
                    {
                        var name = this.translateDanceName[dance.ShortName];

                        if (round.Dances.Any(d => d.Name == name))
                        {
                            var toRemove = round.Dances.First(d => d.Name == name);
                            round.Dances.Remove(toRemove);
                        }

                        var result = new Wdsf.Api.Client.Models.Dance()
                        {
                            Name = name,
                            IsGroupDance = GetIsGroupDance(dance),
                        };

                        var scores =
                            this.context.AllJudgements.Where(
                                j => j.Couple.ToString() == couple.StartNumber && j.Dance == dance.ShortName);

                        foreach (var judgements in scores)
                        {
                            var newScore = new OnScale3Score()
                            {
                                IsSet = false,
                                OfficialId = judges.First(j => j.AdjudicatorChar == judgements.Judge).Id,
                                TQ = judgements.MarkA > -1 ? Convert.ToDecimal(judgements.MarkA) : 0,
                                MM = judgements.MarkB > -1 ? Convert.ToDecimal(judgements.MarkB) : 0,
                                PS = judgements.MarkC > -1 ? Convert.ToDecimal(judgements.MarkC) : 0,
                                CP = judgements.MarkD > -1 ? Convert.ToDecimal(judgements.MarkD) : 0
                            };

                            if (newScore.TQ == 0 && newScore.MM == 0 && newScore.CP == 0 && newScore.PS == 0)
                            {
                                Debugger.Break();
                            }

                            result.Scores.Add(newScore);

                            if (result.IsGroupDance)
                            {
                                var count = 0;
                                if (newScore.TQ > 0) count++;
                                if (newScore.MM > 0) count++;
                                if (newScore.PS > 0) count++;
                                if (newScore.CP > 0) count++;

                                if (count > 1)
                                {
                                    MessageBox.Show(
                                        $"In Group-Dance {dance.LongName} Judge {judgements.Judge} gave {count} scores");
                                }
                            }
                        }

                        round.Dances.Add(result);
                    }

                    if (!apiClient.UpdateCoupleParticipant(coupleDetails))
                    {
                        MessageBox.Show(apiClient.LastApiMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Dispatcher.Invoke(new Action(() => this.Close()));
        }

        private void SendDataTeam()
        {
            try
            {
                this.SetStatus("Downloading Officials", 0, 0);
#if DEBUG
                var apiClient = new Wdsf.Api.Client.Client(this.userName, this.password, WdsfEndpoint.Sandbox);
#else
                var apiClient = new Wdsf.Api.Client.Client(this.userName, this.password, WdsfEndpoint.Services);
#endif

                var judgesList = apiClient.GetOfficials(int.Parse(context.Event.WDSFId));

                var judges = new List<OfficialDetail>();
                foreach (var official in judgesList)
                {
                    judges.Add(apiClient.GetOfficial(official.Id));
                }

                this.SetStatus("Downloading Couples", 0, 0);

                var couples = apiClient.GetTeamParticipants(int.Parse(context.Event.WDSFId));
                var index = 0;

                var numbers = this.context.Couples.Select(c => c.Id.ToString());

                foreach (var couple in couples.Where(c => numbers.Contains(c.StartNumber)))
                {
                    var localCouple = this.context.TotalResult.FirstOrDefault(c => c.Couple.Id.ToString() == couple.StartNumber);
                    index++;

                    var teamDetails = apiClient.GetTeamParticipant(couple.Id);
                    this.SetStatus("Downloading Couples", index, couples.Count);

                    if (localCouple.Place >= droppedOutPlace)
                    {
                        teamDetails.Rank = localCouple.Place.ToString();
                        teamDetails.Points = localCouple.Total.ToString();
                    }
                    else
                    {
                        teamDetails.Rank = "";
                        teamDetails.Points = "";
                    }

                    var round = teamDetails.Rounds.FirstOrDefault(r => r.Name == this.context.Event.RoundNr);
                    if (round == null)
                    {
                        round = new Round()
                        {
                            Name = this.context.Event.RoundNr,
                        };
                        teamDetails.Rounds.Add(round);
                    }

                    round.MaxDeviation = context.Js3CutOffValue.ToString();
                    round.Dances.Clear();

                    foreach (var dance in this.context.Dances)
                    {
                        var result = new Wdsf.Api.Client.Models.Dance()
                        {
                            Name = this.translateDanceName[dance.ShortName],
                            IsGroupDance = GetIsGroupDance(dance),
                        };

                        var scores =
                            this.context.AllJudgements.Where(
                                j => j.Couple.ToString() == couple.StartNumber && j.Dance == dance.ShortName);

                        foreach (var judgements in scores)
                        {
                            var newScore = new  OnScale3Score()
                            {
                                IsSet = false,
                                OfficialId = judges.First(j => j.AdjudicatorChar == judgements.Judge).Id,
                                TQ = judgements.MarkA > -1 ? Convert.ToDecimal(judgements.MarkA) : 0,
                                MM = judgements.MarkB > -1 ? Convert.ToDecimal(judgements.MarkB) : 0,
                                PS = judgements.MarkC > -1 ? Convert.ToDecimal(judgements.MarkC) : 0,
                                CP = judgements.MarkD > -1 ? Convert.ToDecimal(judgements.MarkD) : 0
                            };

                            if (newScore.TQ == 0 && newScore.MM == 0 && newScore.CP == 0 && newScore.PS == 0)
                            {
                                Debugger.Break();
                            }

                            result.Scores.Add(newScore);

                            if (result.IsGroupDance)
                            {
                                var count = 0;
                                if (newScore.TQ > 0) count++;
                                if (newScore.MM > 0) count++;
                                if (newScore.PS > 0) count++;
                                if (newScore.CP > 0) count++;

                                if (count > 1)
                                {
                                    MessageBox.Show(
                                        $"In Group-Dance {dance.LongName} Judge {judgements.Judge} gave {count} scores");
                                }
                            }
                        }

                        round.Dances.Add(result);
                    }

                    if (!apiClient.UpdateTeamParticipant(teamDetails))
                    {
                        MessageBox.Show(apiClient.LastApiMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Dispatcher.Invoke(new Action(() => this.Close()));
        }

        private bool GetIsGroupDance(Dance dance)
        {
            return this.context.Heats[dance.ShortName].First().Couples.Count != 1 ;
        }

        private void SetStatus(string status, double progress, double maxProgress)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                this.StatusLabel.Content = status;
                this.ProgressBar.Value = progress;
                this.ProgressBar.Maximum = maxProgress;
            }));
        }

        private void SetProcessingClicked(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var apiClient = new Wdsf.Api.Client.Client(this.WdsfUser.Text, this.WdsfPassword.Password, WdsfEndpoint.Services);
#else
            var apiClient = new Wdsf.Api.Client.Client(this.WdsfUser.Text, this.WdsfPassword.Password, WdsfEndpoint.Services);
#endif

            CompetitionDetail wdsfCompetition = apiClient.GetCompetition(int.Parse(context.Event.WDSFId));

            wdsfCompetition.Status = "Processing";

            bool res = apiClient.UpdateCompetition(wdsfCompetition);
            if (!res)
            {
                MessageBox.Show(apiClient.LastApiMessage);
            }
        }

        private void SetClosedClicked(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var apiClient = new Wdsf.Api.Client.Client(this.WdsfUser.Text, this.WdsfPassword.Password, WdsfEndpoint.Sandbox);
#else
            var apiClient = new Wdsf.Api.Client.Client(this.WdsfUser.Text, this.WdsfPassword.Password, WdsfEndpoint.Services);
#endif

            CompetitionDetail wdsfCompetition = apiClient.GetCompetition(int.Parse(context.Event.WDSFId));

            wdsfCompetition.Status = "Closed";

            bool res = apiClient.UpdateCompetition(wdsfCompetition);
            if (!res)
            {
                MessageBox.Show(apiClient.LastApiMessage);
            }
        }
    }
}
