﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Judging_Server.Helper
{
    public class CellHighlighterConverter : IMultiValueConverter
    {
        public object Convert(
          object[] values,
          Type targetType,
          object parameter,
          CultureInfo culture)
  {
    if (values[0] is DataGridCell)
    {
        //Change the background of any
        //cell with 1.0 to light red.
        var cell = (DataGridCell)values[0];
        
        if(cell.Content is decimal)
        {
            return new SolidColorBrush(Colors.LightSalmon);
        }
        
    }
    return new SolidColorBrush(Colors.White);
  }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
