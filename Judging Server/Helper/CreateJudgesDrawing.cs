﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Judging_Server.Model;


namespace Judging_Server.Helper
{


    public class CreateJudgesDrawing
    {
        public Dictionary<string, List<JudgesDrawing>> Create(List<JudgingArea> judgingAreas, List<Judge> judges,
            Dictionary<string, List<Heat>> heats, int judgesPerArea)
        {
            return this.Create(judgingAreas, judges, heats, judgesPerArea, 0);
        }

        public Dictionary<string, List<JudgesDrawing>> Create(List<JudgingArea> judgingAreas, List<Judge> judges, Dictionary<string, List<Heat>> heats, int judgesPerArea, int areaOffset)
        {
            var numberOfAreas = judgingAreas.Count;

            //if (numberOfAreas*judgesPerArea > judges.Count)
            //{
            //    throw new Exception("Not enough judges for panel size / number of Areas");
            //}

            if(judges.Count == 10)
            {
                judgesPerArea = 5;
            }

            var res = new Dictionary<string, List<JudgesDrawing>>();
            // todo: this needs to be out-commented for non AIMAG Competitions
            var judgesPerComponent = JudgesRandomPerComponent(judges, judgesPerArea);
            // var judgesPerComponent = JudgesFixPerComponent(judges);

            foreach (var dance in heats.Keys)
            {
                var list = new List<JudgesDrawing>();
                res.Add(dance, list);

                foreach (var heat in heats[dance])
                {                    
                    for (int i = 1; i <= numberOfAreas; i++)
                    {
                        var drawing = new JudgesDrawing() { Area = i + areaOffset, Heat = heat };
                        list.Add(drawing);
                        drawing.Judges = new List<Judge>();
                        drawing.Judges.AddRange(judgesPerComponent[i]);
                    }
                }
            }

            return res;
        }

        private static Dictionary<int, IEnumerable<Judge>> JudgesFixPerComponent(List<Judge> judges)
        {
            var judgesPerComponent = new Dictionary<int, IEnumerable<Judge>>();

            judgesPerComponent.Add(1, new List<Judge> {GetJudge("B", judges), GetJudge("E", judges), GetJudge("G", judges)});

            judgesPerComponent.Add(2, new List<Judge> { GetJudge("A", judges), GetJudge("C", judges), GetJudge("D", judges) });

            judgesPerComponent.Add(3, new List<Judge> { GetJudge("I", judges), GetJudge("J", judges), GetJudge("L", judges) });

            judgesPerComponent.Add(4, new List<Judge> { GetJudge("F", judges), GetJudge("H", judges), GetJudge("K", judges) });


            return judgesPerComponent;
        }

        private static Judge GetJudge(string sign, IEnumerable<Judge> judges)
        {
            if (sign == "A" && !judges.Any(j => j.Sign == "A"))
            {
                sign = "A2"; // Turkmenistan Judge in place of Markus 
            }

            return judges.First(j => j.Sign == sign);
        }

        private static Dictionary<int, IEnumerable<Judge>> JudgesRandomPerComponent(List<Judge> judges, int judgesPerArea)
        {
            var random = Random.GetRandomList<Judge>(judges.Where(j => j.ComponentToJudge == null).ToList());

            var judgesPerComponent = new Dictionary<int, IEnumerable<Judge>>();

            judgesPerComponent[1] = random.Take(judgesPerArea).ToList();
            random.RemoveRange(0, judgesPerArea);
            judgesPerComponent[2] = random.Take(judgesPerArea).ToList();
            random.RemoveRange(0, judgesPerArea);

            if(random.Count == 0)
            {
                judgesPerComponent[3] = new List<Judge>();
                judgesPerComponent[4] = new List<Judge>();

                return judgesPerComponent;
            }

            judgesPerComponent[3] = random.Take(judgesPerArea).ToList();
            random.RemoveRange(0, judgesPerArea);
            judgesPerComponent[4] = random.Take(judgesPerArea).ToList();
            random.RemoveRange(0, judgesPerArea);
            return judgesPerComponent;
        }

        public Dictionary<string, List<JudgesDrawing>> CreateFormation(List<JudgingArea> judgingAreas, List<Judge> judges, Dictionary<string, List<Heat>> heats, int JudgesPerArea)
        {
            var randomJudges = Helper.Random.GetRandomList<Judge>(judges);
             
            var judges1 = randomJudges.Take(6).ToList();

            var judges2 = new List<Judge>();
            for (int i = 6; i < 12; i++)
            {
                judges2.Add(randomJudges[i]);
            }

            var areas1 = new List<JudgingArea>();
            var areas2 = new List<JudgingArea>();

            areas1.Add(judgingAreas.First(a => a.Component == "TQ"));
            areas1.Add(judgingAreas.First(a => a.Component == "MM"));
            areas2.Add(judgingAreas.First(a => a.Component == "TS"));
            areas2.Add(judgingAreas.First(a => a.Component == "CP"));

            var res1 = Create(areas1, judges1, heats, JudgesPerArea, 0);
            var res2 = Create(areas2, judges2, heats, JudgesPerArea, 2);

            foreach (var key in res1.Keys)
            {
                var list1 = res1[key];
                var list2 = res2[key];
                list1.AddRange(list2);
            }

            return res1;
        }
    }
}
