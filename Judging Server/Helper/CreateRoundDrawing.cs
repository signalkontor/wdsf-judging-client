﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Judging_Server.Model;

namespace Judging_Server.Helper
{

    public interface IRoundDrawer
    {
        Dictionary<string, List<Heat>> CreateDrawing(List<Model.Couple> couples, List<Model.Dance> dances,
                                                             int heats);
    }

    public class CreateRoundDrawing : IRoundDrawer
    {
        private int firstCoupleId = -1;

        private int lastCoupleId = -1;

        private List<Heat> Create(List<Couple> couples, int heats, Dance dance, int firstHeatId, int firstId, int lastId)
        {
            var res = new List<Heat>();
            // get a random list
            var random = Random.GetRandomList<Couple>(couples);

            if (firstId > -1 && lastId > -1 && heats > 1)
            {
                while (random[0].Id == firstId || random[0].Id == lastId || random[couples.Count - 1].Id == lastId ||
                       random[couples.Count - 1].Id == firstId)
                {
                    random = Random.GetRandomList<Couple>(couples);
                }
            }

            var size = couples.Count/heats;
            var numExtraSized = couples.Count%heats;

            for (int i = 0; i < heats; i++)
            {
                var heat = new Heat()
                    {
                        Id = firstHeatId + i,
                        Dance = dance,
                        Couples = new List<Couple>()
                    };

                res.Add(heat);

                if (numExtraSized > i)
                {
                    heat.Couples.AddRange(random.Take(size + 1));
                    random.RemoveRange(0, size + 1);
                }
                else
                {
                    heat.Couples.AddRange(random.Take(size));
                    random.RemoveRange(0, size);
                    
                }
                // Sortieren
                heat.Couples = heat.Couples.OrderBy(c => c.Id).ToList();
            }

                return res;
        }

        /// <summary>
        /// Creates the drawing.
        /// </summary>
        /// <param name="couples">The couples.</param>
        /// <param name="dances">The dances.</param>
        /// <param name="heats">The heats.</param>
        /// <returns>The heats of all dances</returns>
        public Dictionary<string, List<Heat>> CreateDrawing(List<Model.Couple> couples, List<Model.Dance> dances, int heats)
        {
            var result = new Dictionary<string, List<Heat>> ();
            var firstHeatId = 0;

            foreach (var dance in dances)
            {
                var heatsOfDance = this.Create(couples, heats, dance, firstHeatId, -1, -1);
                result.Add(dance.ShortName, heatsOfDance);

                firstHeatId += heatsOfDance.Count;
            }

            return result;
        }

        private List<Dance> PickSoloDances(List<Dance> dances)
        {
            
            var ret = new List<Dance>();

            ret.Add(dances[0]);
            ret.Add(dances[2]);
            ret.Add(dances[4]);

            return ret;
        }

        internal Dictionary<string, List<Heat>> CreateFinalDrawing(List<Couple> couples, List<Dance> dances)
        {
            // We have: 3 Solo dances, two group
            // Order of Dances is solo - group - solo - group - solo
            // So we pick 3 Solos
            var solos = PickSoloDances(dances);

            var order = new Dance[5];
            var soloCount = 0;
            var groupCount = 0;
            foreach (var dance in dances)
            {
                if (solos.Any(d => d.ShortName == dance.ShortName))
                {
                    // Solo:
                    switch (soloCount)
                    {
                        case 0:
                            order[0] = dance;
                            break;
                        case 1:
                            order[2] = dance;
                            break;
                        case 2:
                            order[4] = dance;
                            break;
                    }
                    soloCount++;
                }
                else
                {
                    // Group Dance -> Index 1 and 3
                    if (groupCount == 0)
                    {
                        order[1] = dance;
                    }
                    else
                    {
                        order[3] = dance;
                    }
                    groupCount++;
                }
            }

            var result = new Dictionary<string, List<Heat>>();
            var firstHeatId = 0;

            foreach (var dance in order)
            {
                List<Heat> heatsOfDance;

                if (solos.Any(d => d.ShortName == dance.ShortName))
                {
                    heatsOfDance = Create(couples, couples.Count, dance, firstHeatId, this.firstCoupleId,
                        this.lastCoupleId);
                    this.firstCoupleId = heatsOfDance[0].Couples[0].Id;
                    this.lastCoupleId = heatsOfDance[couples.Count - 1].Couples[0].Id;
                }
                else
                {
                    heatsOfDance = Create(couples, 1, dance, firstHeatId, -1, -1);
                }
                
                result.Add(dance.ShortName, heatsOfDance);

                firstHeatId += heatsOfDance.Count;
            }

            return result;
        }
    }
}
