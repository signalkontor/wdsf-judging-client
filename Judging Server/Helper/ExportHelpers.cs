﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Judging_Server.Model;
using System.IO;
using System.Net;
using System.Threading;
using CommonLibrary.Model;
using Judging_Server.Nancy;
using Newtonsoft.Json;
using Dance = Judging_Server.Model.Dance;
using Heat = CommonLibrary.Model.Heat;

namespace Judging_Server.Helper
{
    /// <summary>
    /// Export helpers
    /// </summary>
    public class ExportHelpers
    {

        public void WriteHeatsV1(StreamWriter writer, DataContextClass context)
        {
            int i = 0;
            foreach (var dance in context.Heats.Keys)
            {
                var danceEntity = context.Dances.Single(d => d.ShortName == dance);
                writer.Write((i + 1).ToString() + ";" + danceEntity.ShortName + ";" + danceEntity.LongName);
                // Wir brauchen die Paare nun in diesem Tanz, bzw. deren Reihenfolge
                var heats = context.Heats[dance];
                // We only have solo dances so we have x heats with each one couple
                foreach (var heat in heats)
                {
                    foreach (var couple in heat.Couples)
                    {
                        writer.Write(";" + couple.Id);
                    }
                }
                writer.WriteLine();
                i++;
            }
        }

        /// <summary>
        /// Writes the judge drawing.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="path">The path.</param>
        /// <param name="device">The device.</param>
        public void WriteJudgeData(DataContextClass context, string path, string filename, DeviceStates device)
        {
            if (context.JudgeDrawing == null && context.CalculationVersion != CalculationVersionEnum.Version1)
            {
                var drawer = new Helper.CreateJudgesDrawing();
                context.JudgeDrawing = drawer.Create(context.JudgingAreas, context.Judges, context.Heats, ServerSettings.Default.JudgesPerArea);
            }

            if (context.CalculationVersion == CalculationVersionEnum.VersionJs3)
            {
                WriteJS3Data(context, path, filename, device);
                return;
            }

            WriteV2AndV3Data(context, path, filename, device);
        }

        private void WriteJS3Data(DataContextClass context, string path, string filename, DeviceStates device)
        {
            StreamWriter writer;
            MemoryStream memoryStream = new MemoryStream();

            if (path.ToLower().StartsWith("http://"))
            {
                writer = new StreamWriter(memoryStream);
            }
            else
            {
                writer = new StreamWriter(path + "//" + filename);
            }

            // Create our data structure and write it as json:
            var jsonData = CreateV3DataStructure(context, device);

            writer.WriteLine(JsonConvert.SerializeObject(jsonData, Formatting.Indented));

            writer.Close();
            // Write to client if http:
            if (path.ToLower().StartsWith("http://"))
            {
                var data = Encoding.Default.GetString(memoryStream.ToArray());
                WriteToServer(path, filename, data);
            }
        }

        private ClientModel CreateV3DataStructure(DataContextClass context, DeviceStates device)
        {
            Judge judge = context.Judges.First(j => j.Sign == device.Judge);

            var data = new ClientModel()
            {
                Components = new Component[context.JudgingAreas.Count],
                Judge = judge.Name,
                Sign = judge.Sign,
                Dances = new CommonLibrary.Model.Dance[context.Dances.Count],
                DataPath = $"{context.Event.ScrutinusId}_{context.Event.RoundNr}",
                Version = "JS3",
                MinScore = context.MinimumPoints,
                MaxScore = context.MaximumPoints,
                Intervall = context.IntervallPoints,
                IsShowdance = context.IsShowdance
            };

            data.Components = new Component[context.JudgingAreas.Count];

            for(int index=0;index < context.JudgingAreas.Count;index++)
            {
                data.Components[index] = new Component()
                {
                    LongName = "",
                    ShortName = context.JudgingAreas[index].Component,
                    HelpText = context.JudgingAreas[index].Description
                };
            }

            for (int index = 0; index < context.Dances.Count; index++)
            {
                data.Dances[index] = new CommonLibrary.Model.Dance()
                {
                    ShortName = context.Dances[index].ShortName,
                    LongName = context.Dances[index].LongName,
                    Heats = GetJs3HeatsForDance(context, context.Dances[index], device.Judge)
                };
            }

            return data;
        }

        private Heat[] GetJs3HeatsForDance(DataContextClass context, Dance dance, string sign)
        {
            var heats = context.Heats[dance.ShortName];

            var result = new Heat[heats.Count];

            var judgesDrawing = context.JudgeDrawing[dance.ShortName];

            for (int index = 0; index < heats.Count; index++)
            {
                var drawing = judgesDrawing.FirstOrDefault(j => j.Heat.Id == heats[index].Id && j.Judges.Any(o => o.Sign == sign) ) ;

                result[index] = new Heat()
                {
                    Component1 = context.IsFormation ? GetFirstComponentFormation(heats[index].Couples.Count == 1, drawing.Area) 
                                                     : GetFirstComponentStandard(heats[index].Couples.Count == 1, drawing.Area),
                    Component2 = heats[index].Couples.Count == 1 ? GetSecondComponent(drawing.Area, context.IsFormation) : -1,
                    Couples = heats[index].Couples.Select(c => c.Id).ToArray()
                };
            }

            return result;
        }

        private int GetSecondComponent(int area, bool isFormation)
        {
            if (isFormation)
            {
                return this.GetSecondComponentFormation(area);
            }
            else
            {
                return this.GetSecondComponentStandard(area);
            }
        }

        private int GetFirstComponentFormation(bool isSolo, int area)
        {
            if (isSolo)
            {
                switch (area)
                {
                    case 1: return 1;
                    case 2: return 1;
                    case 3: return 3;
                    case 4: return 3;
                }
            }
            else
            {
                switch (area)
                {
                    case 1: return 1;
                    case 2: return 2;
                    case 3: return 1;
                    case 4: return 2;
                }
            }

            return 1;
        }

        private int GetSecondComponentFormation(int area)
        {
            switch (area)
            {
                case 1: return 2;
                case 2: return 2;
                case 3: return 4;
                case 4: return 4;
            }

            throw new ArgumentOutOfRangeException($"Unknown Area {area}");
        }

        private int GetFirstComponentStandard(bool isSolo, int area)
        {
            if (isSolo)
            {
                switch (area)
                {
                    case 1: return 1;
                    case 2: return 2;
                    case 3: return 1;
                    case 4: return 2;
                }
            }
            else
            {
                switch (area)
                {
                    case 1: return 1;
                    case 2: return 2;
                    case 3: return 1;
                    case 4: return 2;
                }
            }

            return 1;
        }

        private int GetSecondComponentStandard(int area)
        {
            switch (area)
            {
                case 1: return 3;
                case 3: return 3;
                case 2: return 4;
                case 4: return 4;
            }

            throw new ArgumentOutOfRangeException($"Unknown Area {area}");
        }
        private void WriteV2AndV3Data(DataContextClass context, string path, string filename, DeviceStates device)
        {
            StreamWriter writer;
            MemoryStream memoryStream = new MemoryStream();

            if (path.ToLower().StartsWith("http://"))
            {
                writer = new StreamWriter(memoryStream);
            }
            else
            {
                writer = new StreamWriter(path + "//" + filename);
            }


            writer.WriteLine(context.CalculationVersion == CalculationVersionEnum.Version1 ? "V1" : "V2");
            // Write the subfolder name where the client should write it's data to ...
            writer.WriteLine("{0}_{1}", context.Event.ScrutinusId, context.Event.RoundNr);

            if (context.CalculationVersion == CalculationVersionEnum.Version2014)
            {
                // Max, Min Values
                writer.WriteLine("{0};{1};{2}", context.MinimumPoints, context.MaximumPoints, context.IntervallPoints);
            }
            // Write the Judging Components:
            writer.Write("{0}", context.JudgingAreas[0].Component);
            for (var j = 1; j < context.JudgingAreas.Count; j++)
            {
                writer.Write(";{0}", context.JudgingAreas[j].Component);
            }
            writer.WriteLine("");
            for (var j = 0; j < context.JudgingAreas.Count; j++)
            {
                writer.WriteLine("{0};{1}", context.JudgingAreas[j].Component, context.JudgingAreas[j].Description);
            }
            // we write the data to the file:
            Model.Judge judge = context.Judges.SingleOrDefault(j => j.Sign == device.Judge);
            // if we do not know the judge, this device might be the chairman's device
            if (judge != null)
            {
                writer.WriteLine("{0};{1};{2}", judge.Sign, judge.Name, judge.Country);
                writer.WriteLine(context.Event.Title);
            }
            else
            {
                MessageBox.Show("Found judge " + device.Judge + " in devices but not in data.txt. Intended?");
                writer.WriteLine("{0};{1};{2}", "CH", "Chairman", "");
                writer.WriteLine(context.Event.Title);
            }
            // write dances
            if (context.CalculationVersion == CalculationVersionEnum.Version1)
            {
                WriteHeatsV1(writer, context);
                writer.Close();
                return;
            }

            int i = 0;
            foreach (var dance in context.Heats.Keys)
            {
                var danceEntity = context.Dances.Single(d => d.ShortName == dance);
                writer.Write((i + 1) + ";" + danceEntity.ShortName + ";" + danceEntity.LongName);
                // Wir brauchen die Paare nun in diesem Tanz, bzw. deren Reihenfolge
                var heats = context.Heats[dance];
                var drawingList = context.JudgeDrawing[dance];
                // Anzahl der Heats geben wir noch aus
                writer.Write(";" + heats.Count);
                foreach (var heat in heats)
                {
                        var drawing = drawingList.Single(j => j.Judges.Any(ju => ju.Sign == judge.Sign && j.Heat.Id == heat.Id));
                    writer.Write(";" + heat.Couples.Count);
                    writer.Write(";" + drawing.Area);

                    foreach (var couple in heat.Couples)
                    {
                        writer.Write(";" + couple.Id);
                    }
                }
                writer.WriteLine();
                i++;
            }
            writer.Close();

            if (path.ToLower().StartsWith("http://"))
            {
                var data = Encoding.Default.GetString(memoryStream.ToArray());
                WriteToServer(path, filename, data);
            }
        }

        public static void WriteToServer(string path, string filename, string data)
        {
            var webClient = new WebClientWithTimeOut();
            var errorCount = 0;
            while (true)
            {
                try
                {
                    webClient.UploadString(new Uri(path + "/FileHandler/Write/" + filename), "POST", data);
                    return;
                }
                catch (Exception)
                {
                    errorCount++;
                    if (errorCount > 0)
                    {
                        return;
                    }
                    Thread.Sleep(1000);
                }
            }
            
        }

        /// <summary>
        /// Writes the heat data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="path">The path.</param>
        public void WriteHeatData(DataContextClass context, string path)
        {
            var sw = new StreamWriter(path);
        
            foreach (var dance in context.Heats.Keys)
            {
                sw.Write(dance + ";" + context.Heats[dance].Count);
                foreach (var heat in context.Heats[dance])
                {
                    sw.Write(";" + heat.Couples.Count);
                    foreach (var couple in heat.Couples)
                    {
                        sw.Write(";" + couple.Id);
                    }
                }
                sw.WriteLine("");
            }

            sw.Close();
        }

        /// <summary>
        /// Writes the judges drawing.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="path">The path.</param>
        public void WriteJudgesDrawing(DataContextClass context, string path)
        {
            if (context.JudgeDrawing == null) return;

            var sw = new StreamWriter(path);
            
            foreach (var dance in context.Heats.Keys)
            {
                var list = context.JudgeDrawing[dance].OrderBy(o => o.Heat.Id).ToList();
                sw.Write(String.Format("{0};{1}", dance, list.Count));
                foreach (var judgesDrawing in list)
                {
                    sw.Write(String.Format(";{0};{1};{2}", judgesDrawing.Heat.Id, judgesDrawing.Area, judgesDrawing.Judges.Count));
                    foreach (var judge in judgesDrawing.Judges)
                    {
                        sw.Write(";" + judge.Sign);
                    }
                }
                sw.WriteLine("");
            }
            sw.Close();
        }

        public static void WriteAshgabatResult(DataContextClass context, string path)
        {
            WriteJudgments(context, path);
            WriteResults(context, path);
        }

        private static void WriteResults(DataContextClass context, string path)
        {
            var stream = new StreamWriter($"{path}\\results_{context.Dances.First().LongName}.csv");
            stream.WriteLine("Team_ID;Bib_No;Place;Component_A;Component_B;Component_C;Component_D;Total_Points");

            foreach (var couple in context.Couples.OrderBy(c => c.Id))
            {
                var result = context.TotalResult.First(r => r.Couple.Id == couple.Id);
                stream.WriteLine($"{result.Couple.Id};{result.Couple.Id};{result.Place};{result.Result_A.ToString().Replace(",", ".")};{result.Result_B.ToString().Replace(",", ".")};{result.Result_C.ToString().Replace(",", ".")};{result.Result_D.ToString().Replace(",", ".")};{result.Total.ToString().Replace(",", ".")}");
            }

            stream.Close();
        }

        private static void WriteJudgments(DataContextClass context, string path)
        {
            // Write the header
            var stream = new StreamWriter($"{path}\\judgements_{context.Dances.First().LongName}.csv");
            stream.Write("Bip,Dance,Total,Place");

            foreach(var judge in context.Judges.OrderBy(j => j.Sign))
            {
                stream.Write($",{judge.Sign}");
            }

            stream.WriteLine("");

            foreach (var couple in context.Couples)
            {
                foreach (var dance in context.Dances)
                {
                    var result = context.DanceResults[dance.ShortName].FirstOrDefault(r => r.Couple.Id == couple.Id);
                    stream.Write($"{couple.Id},{dance.ShortName},{result.Total.ToString().Replace(",", ".")},{result.Place}");
                    // Get the judgements of this dance:
                    foreach (var judge in context.Judges.OrderBy(j => j.Sign))
                    {
                        var isSolo = context.Heats[dance.ShortName].First().Couples.Count == 1;
                        var judgement = context.AllJudgements.FirstOrDefault(j => j.Dance == dance.ShortName && j.Judge == judge.Sign && j.Couple == couple.Id);
                        if (judgement != null)
                        {
                            stream.Write($",{GetJudgementString(judgement, isSolo)}");
                        }
                    }
                    stream.WriteLine();
                }
            }

            stream.Close();
        }

        private static string GetJudgementString(Judgements judgement, bool isSolo)
        {
            var res = judgement.Area + "=";
            switch (judgement.Area)
            {
                case "TQ" :
                    res += judgement.MarkA.ToString().Replace(",", ".");
                    break;
                case "MM":
                    res += judgement.MarkB.ToString().Replace(",", ".");
                    break;
                case "PS":
                    res += judgement.MarkC.ToString().Replace(",", ".");
                    break;
                case "CP":
                    res += judgement.MarkD.ToString().Replace(",", ".");
                    break;
            }

            if(!isSolo)
            {
                return res;
            }

            res += "|";
            switch (judgement.Area)
            {
                case "TQ":
                    res += "PS=" + judgement.MarkC.ToString().Replace(",", ".");
                    break;
                case "MM":
                    res += "CP=" + judgement.MarkD.ToString().Replace(",", ".");
                    break;
                case "PS":
                    res += "TQ=" + judgement.MarkA.ToString().Replace(",", ".");
                    break;
                case "CP":
                    res += "MM=" + judgement.MarkB.ToString().Replace(",", ".");
                    break;
            }

            return res;
        }
    }
}
