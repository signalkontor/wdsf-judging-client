using System;

namespace Judging_Server.Model
{
    public class Couple
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Country { get; set; }
        
        public string WDSF_ID { get; set; }

        public double PointOffset { get; set; }

        public string NiceName
        {
            get { return String.Format("({0}) - {1}", this.Id, this.Name); }
        }
    }
}