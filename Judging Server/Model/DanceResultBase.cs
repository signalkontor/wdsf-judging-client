﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    public abstract class DanceResultBase : System.ComponentModel.INotifyPropertyChanged
    {
        protected List<TotalPerCouple> couplesTotal;
        protected Dictionary<string, TotalPerCouple> lookUpTotalsPerCouple;
        protected Dictionary<string, Judgements> judgements;
        protected double chairmanReduction;
        protected List<JudgingArea> judgmentAreas;
 
        public DanceResultBase(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges)
        {
            judgmentAreas = judgingAreas;
     
            Dance = dance;
            Couple = couple;
            couplesTotal = new List<TotalPerCouple>();
            lookUpTotalsPerCouple = new Dictionary<string, TotalPerCouple>();
            judgements = new Dictionary<string, Judgements>();
            for (int i = 0; i < judges.Count; i++)
            {
                var c = new TotalPerCouple() { Couple = couple.Id, Judge = judges[i].Sign, State = (int)States.Used, Total = 0 };
                couplesTotal.Add(c);
                lookUpTotalsPerCouple.Add(judges[i].Sign, c);
            }
        }

        public string Dance { get; set; }

        public Couple Couple { get; set; }

        protected int _place;

        public int Place
        {
            get { return _place; }
            set
            {
                _place = value;
                RaiseEvent("Place");
            }
        }

        protected void TotalJudgeSetter(string judge, Judgements value)
        {
            lookUpTotalsPerCouple[judge].Total = value.MarkA + value.MarkB + value.MarkC + value.MarkD;
            if (judgements.ContainsKey(judge))
            {
                judgements[judge] = value;
            }
            else
            {
                judgements.Add(judge, value);
            }
            CalculateTotal();
        }

        public double TotalJudgeGetter(string judge)
        {
            if (lookUpTotalsPerCouple.ContainsKey(judge))
                return lookUpTotalsPerCouple[judge].Total;

            return 0;
        }

        public int StateofJudge(string judge)
        {
            if (lookUpTotalsPerCouple.ContainsKey(judge))
                return lookUpTotalsPerCouple[judge].State;

           return 0;
        }

        protected double _total;
        /// <summary>
        /// Grand Total of all Judges but without best and worst judgement
        /// </summary>
        public double Total
        {
            get { return _total; }
            set
            {
                _total = value;
                RaiseEvent("Total");
            }
        }

        public double ChairmanReduction
        {
            get { return chairmanReduction; }
            set
            {
                chairmanReduction = value;
                CalculateTotal();
                RaiseEvent("ChairmanReduction");
            }
        }

        public Dictionary<string, TotalPerCouple> ResultsPerJudge
        {
            get { return lookUpTotalsPerCouple; }
        }

        public double Total_Judge_A
        {
            get { return TotalJudgeGetter("A"); }
            // set { TotalJudgeSetter("A", value); }
        }
       
        protected double _result_A;

        public double Result_A
        { get { return _result_A * (judgmentAreas.Count > 0 ? judgmentAreas[0].Weight : 0); } 
            set
            {
                _result_A = value;
                RaiseEvent("Result_A");
            }
        }

        protected double _result_B;
        public double Result_B
        {
            get { return _result_B * (judgmentAreas.Count > 1 ? judgmentAreas[1].Weight : 0); }
            set
            {
                _result_B = value;
                RaiseEvent("Result_B");
            }
        }

        protected double _result_C;
        public double Result_C
        {
            get { return _result_C * (judgmentAreas.Count > 2 ? judgmentAreas[2].Weight : 0); }
            set
            {
                _result_C = value;
                RaiseEvent("Result_C");
            }
        }

        protected double _result_D;
        public double Result_D
        {
            get { return _result_D * (judgmentAreas.Count > 3 ? judgmentAreas[3].Weight : 0); }
            set
            {
                _result_D = value;
                RaiseEvent("Result_D");
            }
        }

        protected double _result_E;
        public double Result_E
        {
            get { return _result_E * (judgmentAreas.Count > 4 ? judgmentAreas[4].Weight : 0); }
            set
            {
                _result_E = value;
                RaiseEvent("Result_E");
            }
        }


        public void UpdateByJudge(string judge, Judgements judgements)
        {
            var marks = new List<double>()
                {
                    judgements.MarkA,
                    judgements.MarkB,
                    judgements.MarkC,
                    judgements.MarkD
                };

            if (judgements.MarkA > -1) lookUpTotalsPerCouple[judge].Area = "TQ";
            if (judgements.MarkB > -1) lookUpTotalsPerCouple[judge].Area = "MM";
            if (judgements.MarkC > -1) lookUpTotalsPerCouple[judge].Area = "PS";
            if (judgements.MarkD > -1) lookUpTotalsPerCouple[judge].Area = "CP";

            lookUpTotalsPerCouple[judge].Total = marks.Where(m => m > -1). Sum();

            if (this.judgements.ContainsKey(judge))
            {
                this.judgements[judge] = judgements;
            }
            else
            {
                this.judgements.Add(judge, judgements);
            }

            // Nun müssen wir noch die Gesamtsumme Total über alle Tänze neu berechnen
            CalculateTotal();
            RaiseEvent("ResultsPerJudge");
        }


        protected abstract void CalculateTotal();
        
        
        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

}
