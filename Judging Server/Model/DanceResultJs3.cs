﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Judging_Server.Model
{
    public class DanceResultJs3 : DanceResultBase
    {
        private readonly List<Judge> judges;
        private readonly bool isGroupDance;

        private readonly double maximumDeviation;
        private readonly bool isShowdance;

        public DanceResultJs3(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges, bool isGroupDance, double maximumDeviation, bool isShowdance) : base(judgingAreas, couple, dance, judges)
        {
            this.judges = judges;
            this.isGroupDance = isGroupDance;
            this.maximumDeviation = maximumDeviation;
            this.isShowdance = isShowdance;
        }

        protected override void CalculateTotal()
        {
            if (isGroupDance)
            {
                this.CalcuateGroupDance();
            }
            else
            {
                this.CalculateSoloDance();
            }
        }

        private void CalcuateGroupDance()
        {
            this.CalculateComponents();
            // In group dances we only have two components that get
            // multiplied by two
            this.Result_A *= 2;
            this.Result_B *= 2;
            this.Result_C *= 0;
            this.Result_D = 0;

            this.Total = Result_A + Result_B;

            RaiseEvent("Total");
            RaiseEvent("Result_A");
            RaiseEvent("Result_B");
            RaiseEvent("Result_C");
            RaiseEvent("Result_D");
        }

        private void CalculateSoloDance()
        {
            CalculateComponents();
        }

        private void CalculateComponents()
        {
            var marksA = judgements.Where(j => j.Value.MarkA > -1).OrderBy(j => j.Value.MarkA).Select(j => j.Value.MarkA).ToList();
            var marksB = judgements.Where(j => j.Value.MarkB > -1).OrderBy(j => j.Value.MarkB).Select(j => j.Value.MarkB).ToList();
            var marksC = judgements.Where(j => j.Value.MarkC > -1).OrderBy(j => j.Value.MarkC).Select(j => j.Value.MarkC).ToList();
            var marksD = judgements.Where(j => j.Value.MarkD > -1).OrderBy(j => j.Value.MarkD).Select(j => j.Value.MarkD).ToList();

            var sumA = CalculateComponent(marksA, "A");
            var sumB = CalculateComponent(marksB, "B");
            var sumC = CalculateComponent(marksC, "C");
            var sumD = CalculateComponent(marksD, "D");

            Result_A = sumA;
            Result_B = sumB;
            Result_C = sumC;
            Result_D = sumD;

            double sum = sumA * (judgmentAreas.Count > 0 ? judgmentAreas[0].Weight : 1d) +
                         sumB * (judgmentAreas.Count > 1 ? judgmentAreas[1].Weight : 1d) +
                         sumC * (judgmentAreas.Count > 2 ? judgmentAreas[2].Weight : 1d) +
                         sumD * (judgmentAreas.Count > 3 ? judgmentAreas[3].Weight : 1d) +
                         ChairmanReduction;

            if (sum != this.Total)
            {
                Total = sum;
                RaiseEvent("Total");
                RaiseEvent("Result_A");
                RaiseEvent("Result_B");
                RaiseEvent("Result_C");
                RaiseEvent("Result_D");
            }
        }

        private double CalculateComponent(IList<double> marks, string component)
        {
            Debug.WriteLine($"Count Marks is {marks.Count}");

            var halfOfJudges = this.judges.Count / 2;

            if (isShowdance)
            {
                halfOfJudges = this.judges.Count;
            }

            if (marks.Count < halfOfJudges)
            {
                return 0;
            }

            // step 1: Calculate average of all judgements:
            var avg = CaluclateMedian(marks);

            var marksInRange = GetMarksInRange(marks, avg);

            Debug.Write(component + " / " + base.Couple.Id + " / " + base.Dance + ": ");
            foreach (var mark in marksInRange)
            {
                Debug.Write(" " + mark);
            }
            Debug.WriteLine("");

            return Math.Round(marksInRange.Average(), 3, MidpointRounding.AwayFromZero);
        }

        private  IEnumerable<double> GetMarksInRange(IList<double> marks, double avg)
        {
            var marksInRange = marks.Where(m => Math.Abs(m - avg) <= this.maximumDeviation).ToList();

            if (!marksInRange.Any())
            {
                marksInRange = marks.ToList();
            }

            return marksInRange;
        }

        private static double CaluclateMedian(IList<double> marks)
        {
            var sorted = marks.OrderBy(o => o).ToList();

            if (marks.Count % 2 == 0)
            {
                var index1 = marks.Count / 2;
                var index2 = marks.Count / 2 - 1;

                return (sorted[index1] + sorted[index2]) / 2;
            }

            var index = marks.Count / 2;
            return sorted[index];
        }
    }
}
