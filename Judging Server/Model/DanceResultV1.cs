﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    /// <summary>
    /// DanceResult hält für ein Paar das Ergebnis für einen Tanz. Hierbei wird jeweils das beste und schlechtese Ergebnis der WR's
    /// gelöscht. Welche dies sind, wird im Status gespeichert, so dass dieser entrsprechend nicht angezeigt werden muss (oder anders)
    /// </summary>
    public class DanceResultV1 : DanceResultBase
    {
       
        public DanceResultV1(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges) : base(judgingAreas, couple, dance, judges)
        {
            
        }

        protected override void CalculateTotal()
        {
            foreach (TotalPerCouple p in couplesTotal)
                p.State = (int)States.Used;

            // Regel: Aus jedem der 5 Wertungsbereiche wird jeweils der höchste und
            // niedrigste gestrichen 
            var minA = judgements.Min(j => j.Value.MarkA);
            var minB = judgements.Min(j => j.Value.MarkB);
            var minC = judgements.Min(j => j.Value.MarkC);
            var minD = judgements.Min(j => j.Value.MarkD);

            var maxA = judgements.Max(j => j.Value.MarkA);
            var maxB = judgements.Max(j => j.Value.MarkB);
            var maxC = judgements.Max(j => j.Value.MarkC);
            var maxD = judgements.Max(j => j.Value.MarkD);

            // wir sortieren die Liste Total 
            couplesTotal.Sort(new TotalPerCoupleSorter());
            couplesTotal[0].State = (int)States.NotUsedMinimum;
            couplesTotal[couplesTotal.Count - 1].State = (int)States.NotUsedMaximum;
            // Sicherheitshalber für alle States einen Changes Event raisen
            RaiseEvent("State_Judge_A");
            RaiseEvent("State_Judge_B");
            RaiseEvent("State_Judge_C");
            RaiseEvent("State_Judge_D");
            RaiseEvent("State_Judge_E");
            RaiseEvent("State_Judge_F");
            RaiseEvent("State_Judge_G");
            RaiseEvent("State_Judge_H");
            RaiseEvent("State_Judge_I");
            RaiseEvent("State_Judge_J");
            RaiseEvent("State_Judge_K");
            RaiseEvent("State_Judge_L");
            RaiseEvent("State_Judge_M");

            RaiseEvent("Marking_A");

            double sumA = 0;
            double sumB = 0;
            double sumC = 0;
            double sumD = 0;
            double sumE = 0;

            double numJudges = 0;
            foreach (var judgement in judgements.Values)
            {
                sumA += judgement.MarkA;
                sumB += judgement.MarkB;
                sumC += judgement.MarkC;
                sumD += judgement.MarkD;
                numJudges++;
            }
            sumA = sumA - minA - maxA;
            sumB = sumB - minB - maxB;
            sumC = sumC - minC - maxC;
            sumD = sumD - minD - maxD;
            // Mittelwert auf die Anzahl der Judges
            sumA /= (numJudges - 2);
            sumB /= (numJudges - 2);
            sumC /= (numJudges - 2);
            sumD /= (numJudges - 2);
            sumE /= (numJudges - 2);
            _result_A = sumA;
            _result_B = sumB;
            _result_C = sumC;
            _result_D = sumD;
            _result_E = sumE;
            // We add chairman reductions (if applied) and
            // calculate the sum based on the weight of each component
            double sum = sumA * (judgmentAreas.Count > 0 ? judgmentAreas[0].Weight : 1) +
                         sumB * (judgmentAreas.Count > 1 ? judgmentAreas[1].Weight : 1) +
                         sumC * (judgmentAreas.Count > 2 ? judgmentAreas[2].Weight : 1) +
                         sumD * (judgmentAreas.Count > 3 ? judgmentAreas[3].Weight : 1) +
                         sumE * (judgmentAreas.Count > 4 ? judgmentAreas[4].Weight : 1) +
                         ChairmanReduction;

            sum = Math.Round(sum, 2);
            if (sum != Total)
            {
                Total = sum;
                RaiseEvent("Total");
            }
        }

    }

}
