using System;
using System.Windows.Forms;

namespace Judging_Server.Model
{
    public class DeviceStates : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Position of Device, used for Formation EventsS
        /// </summary>
        public string Position { get; set; }

        public string DeviceId { get; set; }

        private int _dance;

        public int Dance
        {
            get { return this._dance; }
            set { this._dance = value;
                this.RaiseEvent("Dance");    
            }
        }

        private int _heat;
        public int Heat
        {
            get { return this._heat; }
            set { this._heat = value;
                this.RaiseEvent("Heat");
            }
        }
        private string _state;

        public string State
        {
            get { return this._state; }
            set { this._state = value;
                this.RaiseEvent("State");
            }
        }

        private DateTime _time;
        public DateTime Time
        {
            get { return this._time; }
            set { this._time = value; this.RaiseEvent("Time"); }
        }

        private double _mark;
        public double Area
        {
            get { return this._mark; }
            set { this._mark = value; this.RaiseEvent("Area"); }
        }

        private string remainingBatteryLiveTime;
        public string RemainingBatteryLiveTime
        {
            get { return this.remainingBatteryLiveTime; }
            set
            {
                this.remainingBatteryLiveTime = value;
                RaiseEvent("RemainingBatteryLiveTime");
            }
        }

        private string devicePath;
        public string DevicePath {
            get
            {
                return this.devicePath;
            }

            set
            {
                this.devicePath = value;
                RaiseEvent(nameof(this.DevicePath));
            }
        }

        // Sign of assigned Judge
        private string judge { get; set; }
        public string Judge { 
            get { return judge; }
            set { 
                this.judge = value;
                RaiseEvent("Judge");
            }

        }

        private string powerLineState;

        public string PowerLineState
        {
            get { return this.powerLineState;  }
            set
            {
                this.powerLineState = value;
                RaiseEvent("PowerLineState");
            }
        }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (this.PropertyChanged != null && DataContextClass.RaiseEvents)
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}