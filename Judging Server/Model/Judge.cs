namespace Judging_Server.Model
{
    public class Judge
    {
        public string Sign { get; set; }
        public string WDSFSign { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string MIN { get; set; }
        public string ComponentToJudge { get; set; }

        public string Pin { get; set; }
    }
}