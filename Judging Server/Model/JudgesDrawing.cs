﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    public class JudgesDrawing
    {
        public Heat Heat { get; set; }
        public int Area { get; set; }
        public List<Judge> Judges { get; set; }

    }
}
