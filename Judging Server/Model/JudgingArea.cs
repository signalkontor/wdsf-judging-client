namespace Judging_Server.Model
{
    public class JudgingArea
    {
        public string Component { get; set; }

        public string Description { get; set; }

        public double Weight {get; set;}

        public double? MinScore { get; set; }

        public double? MaxScore { get; set; }

        public string JudgeSign { get; set; }
    }
}