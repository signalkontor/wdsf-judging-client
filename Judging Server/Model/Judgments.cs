﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Judging_Server.Model
{
    public class Judgements : System.ComponentModel.INotifyPropertyChanged
    {
        private double markA = -1;

        private double markB = -1;

        private double markC = -1;

        private double markD = -1;

        private double mark;

        private int area = 0;

        private int area2 = 0;


        // Updateing Silent only fires 1 event instead of 5 ...
        public void UpdateSilent(int area, double mark)
        {
            this.mark = mark;

            if (this.area > 0)
            {
                area2 = area;
            }
            else
            {
                this.area = area;
            }

            switch (area)
            {
                case 1:
                    MarkA = mark;
                    break;
                case 2:
                    MarkB = mark;
                    break;
                case 3:
                    MarkC = mark;
                    break;
                case 4:
                    MarkD = mark;
                    break;
            }


            RaiseEvent("MarkA");
            RaiseEvent("MarkB");
            RaiseEvent("MarkC");
            RaiseEvent("MarkD");
            // Total should also calculate the totals ...
            // via CalcualteTotals() Method
            RaiseEvent("Total");
        }

        public double Mark
        {
            get
            {
                return mark;
            }

            set
            {
                this.mark = value;
                RaiseEvent("Mark");
                RaiseEvent("Total");
            }
        }

        public double MarkA
        {
            get { return markA; }
            set
            {
                double old = markA;
                markA = value;
                if (markA == old) return;
                RaiseEvent("MarkA");
                RaiseEvent("Total");
            }
        }

        public double MarkB
        {
            get { return markB; }
            set
            {
                double old = markB;
                markB = value;
                if (markB == old) return;
                RaiseEvent("MarkB");
                RaiseEvent("Total");
            }
        }

        public double MarkC
        {
            get { return markC; }
            set
            {
                var old = markC;
                markC = value;
                if (markC == old) return;
                RaiseEvent("MarkC");
                RaiseEvent("Total");
            }
        }

        public double MarkD
        {
            get { return markD; }
            set
            {
                var old = markD;
                markD = value;
                if (markD == old) return;
                RaiseEvent("MarkD");
                RaiseEvent("Total");
            }
        }

        public double Score
        {
            get
            {
                if(markA > 0) return MarkA;
                if(markB > 0) return MarkB;
                if(markC > 0) return MarkC;
                if(markD > 0) return MarkD;

                return 0;
            }
        }

        public string Area
        {
            get
            {
                switch (area)
                {
                    case 1:
                        return "TQ";
                    case 2:
                        return "MM";
                    case 3:
                        return "PS";
                    case 4:
                        return "CP";
                    case 5:
                        return "FS";
                    case 6:
                        return "BL";
                    case 7:
                        return "PC";
                }

                return "";
            }
        }

        public double Total
        {
            get
            {
                return markA + markB + markC + markD;
            }
        }

        public string Dance { get; set; }
        public string Judge { get; set; }
        public int Couple { get; set; }

        public override string ToString()
        {
            return MarkA + ", " + MarkB + ", " + MarkC + ", " + MarkD;
        }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

}
