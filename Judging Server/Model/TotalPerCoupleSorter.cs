using System.Collections.Generic;

namespace Judging_Server.Model
{
    /// <summary>
    /// Compares the total sums to sort the list by result ...
    /// </summary>
    public class TotalPerCoupleSorter : IComparer<TotalPerCouple>
    {
    
        #region IComparer<TotalPerCouple> Members

        public int  Compare(TotalPerCouple x, TotalPerCouple y)
        {
            return x.Total.CompareTo(y.Total);
        }

        #endregion
    }
}