﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Nancy;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.TinyIoc;

namespace Judging_Server.Nancy
{
    public class ApplicationBootstrapper : DefaultNancyBootstrapper
    {
        public ApplicationBootstrapper()
        {

        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);

            // nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("/", ""));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Content", "Content"));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Scripts", "Scripts"));

            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddFile("/start", "Content/index.html"));
        }

        

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
           
            base.ApplicationStartup(container, pipelines);

            // This is just for login purpose:
            pipelines.BeforeRequest += (ctx) =>
            {
                Debug.WriteLine(
                    "[{0}] {1}: {2} {3}",
                    DateTime.Now.ToShortTimeString(),
                    ctx.Request.UserHostAddress,
                    ctx.Request.Path,
                    ctx.Request.Method);

                return null;
            };
        }
    }
}
