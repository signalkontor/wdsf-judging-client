﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using Nancy;

namespace Judging_Server.Nancy
{
    public class FileHandlerModule : NancyModule
    {
        public FileHandlerModule()
        {
            this.Get["/FileHandler/"] = _ =>
                {
                    // Log the IP Address to local AvailableDevices.txt
                    var source = this.Request.UserHostAddress;

                    try
                    {
                        var writer = File.AppendText(ServerSettings.Default.DataPath + "\\AvailableDevices.txt");
                        writer.WriteLine("A;http://{0}:9210;0", source);
                        writer.Close();
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine(exception.Message);
                    }

                    return "Htpp Server is running";
                };

            this.Post["/FileHandler/Write/{filename}"] = request =>
                {
                    var filename = request.filename.ToString();
                    var eventId = this.Request.Query["EventId"];

                    try
                    {
                        var body = this.Request.Body;
                        int bl = (int)body.Length; //this is a dynamic variable.
                        byte[] data = new byte[bl];
                        body.Read(data, 0, bl);

                        var folder = string.Format("{0}\\{1}", ServerSettings.Default.DataPath, eventId);

                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }

                        File.WriteAllBytes(folder + "\\" + filename, data);

                        return "OK";
                    }
                    catch (Exception exception)
                    {
                       Debug.WriteLine(exception.Message);
                        return "ERROR: " + exception.Message;
                    }
                };

            this.Get["/judge_state.php"] = request =>
            {
                var judgeSign = this.Request.Query["s"];
                var path = this.Request.Query["p"];
                var state = this.Request.Query["r"];
                
                SaveWriteToFile(path, $"Result_{judgeSign}.txt", state);

                return "True";
            };

            this.Post["/save_judge.php"] = request =>
            {
                var judgeSign = Request.Form["s"];
                var path = this.Request.Form["p"];
                var state = judgeSign + ";" + this.Request.Form["r"];

                SaveWriteToFile(path, $"State_{judgeSign}.txt", state);

                return "True";
            };

            this.Post["/save_result.php"] = request =>
            {
                var judgeSign = this.Request.Form["s"];
                var path = this.Request.Form["p"];
                var result = this.Request.Form["r"];

                SaveWriteToFile(path, $"Result_{judgeSign}.txt", result);

                return "True";
            };

            this.Get["/state_changed.php"] = request =>
            {
                var symbol = Request.Query["s"];
                var filename = $"{ServerSettings.Default.DataPath}\\{symbol}\\newState.txt";

                if (File.Exists(filename))
                {
                    return File.ReadAllText(filename);
                }

                return "";
            };

            this.Get["/state_stamp.php"] = request =>
            {
                var symbol = Request.Query["s"];
                var filename = $"{ServerSettings.Default.DataPath}\\{symbol}\\newState.txt";

                if (File.Exists(filename))
                {
                    var stamp = File.GetLastWriteTime(filename);
                    return stamp.ToShortTimeString();
                }

                return "";
            };

            this.Get["music_length.php"] = request =>
            {
                var symbol = Request.Query["s"];
                var filename = $"{ServerSettings.Default.DataPath}\\{symbol}\\time.txt";

                if (File.Exists(filename))
                {
                    return File.ReadAllText(filename);
                }

                return "";
            };

            this.Get["music_stamp.php"] = request =>
            {
                var symbol = Request.Query["s"];
                var filename = $"{ServerSettings.Default.DataPath}\\{symbol}\\time.txt";

                return File.Exists(filename) ? File.GetLastWriteTime(filename).ToShortTimeString() : "";
            };

            this.Get["/eJudge/_{sign}/dance.txt"] = request =>
            {
                var sign = request.sign.ToString();

                var file = $"C:\\eJudge\\eJudgeData\\_{sign}\\dance.txt";

                var content = File.ReadAllText(file);

                return content;
            };
        }

        private static void SaveWriteToFile(string path, string filename, string content)
        {
            var folder = string.Format("{0}\\{1}", ServerSettings.Default.DataPath, path);

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            File.WriteAllBytes(folder + "\\" + filename, Encoding.UTF8.GetBytes(content));
        }
    }
}
