﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Judging_Server.Model;
using Judging_Server.Nancy.Json;
using Nancy;
using Nancy.ModelBinding;

namespace Judging_Server.Nancy
{
    public class HttpClientModule : NancyModule
    {
        public HttpClientModule() : base("/HttpClient")
        {
            this.Get["/Competition/{pin}"] = request =>
            {
                var pin = request.pin.ToString();
                var judge = DataContextClass.Instance.Judges.FirstOrDefault(j => j.Pin == pin);

                if (judge == null)
                {
                    return HttpStatusCode.NotFound;
                }

                return Response.AsJson<RoundData>(DataContextClass.Instance.GetRoundDataForJudge(judge));
            };

            this.Post["/Status"] = StatusMessage;

            this.Post["/Result"] = ResultMessage;
        }

        private Response StatusMessage(dynamic o)
        {
            var status = this.Bind<Json.state>();

            try
            {
                var statusString = string.Format("{0};{1};{2};{3};{4}", status.sign, status.heat, status.dance,
                    status.component, status.battery);

                File.WriteAllText(string.Format("{0}\\State_{1}.txt", ServerSettings.Default.DataPath, status.sign),
                    statusString);
            }
            catch (Exception ex)
            {
                return HttpStatusCode.InternalServerError;
            }

            return HttpStatusCode.OK;
        }

        private Response ResultMessage(dynamic o)
        {
            var result = this.Bind<Json.Results>();
            StreamWriter fileStream = null;
            try
            {
                fileStream = new StreamWriter($"{ServerSettings.Default.DataPath}\\Result_{result.sign}.txt", false);

                foreach (var result1 in result.results)
                {
                    var component = DataContextClass.Instance.JudgingAreas.FirstOrDefault(a => a.Component == result1.component);
                    var componentIndex = DataContextClass.Instance.JudgingAreas.IndexOf(component);

                    fileStream.WriteLine("{0};{1};{2};{3};{4}", result.sign, result1.dance, result1.couple,
                        componentIndex + 1, result1.score);
                }
            }
            catch (Exception ex)
            {
                return HttpStatusCode.InternalServerError;
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                }
            }

            return HttpStatusCode.OK;
        }
        
    }
}
