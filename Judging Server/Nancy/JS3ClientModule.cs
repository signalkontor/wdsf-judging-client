﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Judging_Server.Model;
using Judging_Server.Nancy.Json;
using Judging_Server.Nancy.Json.DTO;
using Nancy;
using Nancy.ModelBinding;

namespace Judging_Server.Nancy
{
    public class Js3ClientModule : NancyModule
    {
        public static List<Command> commands = new List<Command>();

        public Js3ClientModule() : base("/Js3Api")
        {
            After.AddItemToEndOfPipeline((ctx) => ctx.Response
                .WithHeader("Access-Control-Allow-Origin", "*")
                .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type"));

            this.Get["/GetData"] = request =>
            {
                if (DataContextClass.Instance == null || DataContextClass.Instance.Judges == null)
                {
                    return HttpStatusCode.NotFound;
                }

                var pin = Request.Query["pin"];

                // for thread saveness lock this
                lock (DataContextClass.Instance)
                {
                    var judge = DataContextClass.Instance.Judges.FirstOrDefault(j => j.Pin == pin);

                    if (judge == null)
                    {
                        return HttpStatusCode.NotFound;
                    }

                    var deviceState = DataContextClass.Instance.DeviceState.FirstOrDefault(s => s.Judge == judge.Sign);

                    if (deviceState == null)
                    {
                        deviceState = new DeviceStates()
                        {
                            Judge = judge.Sign,
                            DevicePath = this.Request.UserHostAddress
                        };
                        DataContextClass.Instance.DeviceState.Add(deviceState);
                    }

                    deviceState.Time = DateTime.Now;
                    deviceState.DevicePath = this.Request.UserHostAddress;

                    return Response.AsJson<HeatData>(HeatDataFactory.GetHeatData(DataContextClass.Instance, judge)).WithHeader("Access-Control-Allow-Origin", "*"); //DataContextClass.Instance, judge));
                }
            };

            this.Get["/GetCommand"] = request =>
            {
                Command command = null;
                var sign = Request.Query["sign"];
                lock (commands)
                {
                    command = commands.FirstOrDefault(c => c.target == sign);
                    if (command == null)
                    {
                        return HttpStatusCode.ImATeapot;
                    }

                    // remove it so we do not send it again ...
                    commands.Remove(command);
                }

                return Response.AsJson<Command>(command);
            };

            
            this.Options["/SaveData"] = request =>
            {
                return HttpStatusCode.OK;
            };
            

            this.Post["/SaveData"] = request =>
            {
                var results = this.Bind<HeatData>();

                // Save this to a local file:
                var file = $"{ServerSettings.Default.DataPath}\\{DataContextClass.Instance.Event.ScrutinusId}_{DataContextClass.Instance.Event.RoundNr}\\Result_{results.sign}.txt";

                var stream = new StreamWriter(file);

                foreach(var dance in results.dances)
                {
                    foreach(var heat in dance.heats)
                    {
                        foreach(var coupleScore in heat.coupleScores)
                        {
                            stream.WriteLine("{0};{1};{2};{3};{4}", coupleScore.scoreA.judge, coupleScore.scoreA.dance, coupleScore.couple, coupleScore.scoreA.component, coupleScore.scoreA.score);
                            if(coupleScore.scoreB != null)
                            {
                                stream.WriteLine("{0};{1};{2};{3};{4}", coupleScore.scoreB.judge, coupleScore.scoreB.dance, coupleScore.couple, coupleScore.scoreB.component, coupleScore.scoreB.score);
                            }
                        }
                    }
                }

                stream.Close();

                return Response.AsJson<bool>(true).WithHeader("Access-Control-Allow-Origin", "*");
            };

            this.Options["/SetState"] = request =>
            {
                return HttpStatusCode.OK;
            };

            this.Post["/SetState"] = request =>
            {
                var state = this.Bind<State>();

                try
                {
                    var file =
                        $"{ServerSettings.Default.DataPath}\\{DataContextClass.Instance.Event.ScrutinusId}_{DataContextClass.Instance.Event.RoundNr}\\State_{state.sign}.txt";
                    var stream = new StreamWriter(file);
                    stream.WriteLine($"{state.sign};{state.currentHeatIndex};{state.currentDanceIndex};{state.area}");
                    stream.Close();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                return HttpStatusCode.OK; ;
            };
        }

        public static void SetTimerCommand(string timer)
        {
            lock (DataContextClass.Instance)
            {
                foreach (var judge in DataContextClass.Instance.Judges)
                {
                    lock (commands)
                    {
                        commands.Add(new Command()
                        {
                            target = judge.Sign,
                            command = "settime",
                            timer = timer
                        });
                    }
                }
            }
        }

        public static void SetGotoHeatCommand(Model.Heat heat)
        {
            lock (DataContextClass.Instance)
            {
                // find index of dance and heat:
                var indexOfDance = DataContextClass.Instance.Dances.IndexOf(heat.Dance);
                var indexOfHeat = DataContextClass.Instance.Heats[heat.Dance.ShortName].IndexOf(heat);

                foreach (var judge in DataContextClass.Instance.Judges)
                {
                    commands.Add(new Command()
                    {
                        target = judge.Sign,
                        command = "changeheat",
                        dance = indexOfDance,
                        heat = indexOfHeat
                    });
                }
            }
        }
    }
}
