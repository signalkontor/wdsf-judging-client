﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Judging_Server.Model;

namespace Judging_Server.Nancy.Json.DTO
{
    public enum ScoringType
    {
        js3 = 1,
        marking = 2,
        skating = 3
    }

    public class Command
    {
        public string target { get; set; }

        public string command { get; set; }

        public int heat { get; set; }

        public int dance { get; set; }

        public string timer { get; set; } 
    }

    public class State
    {
        public string sign { get; set; }

        public int area { get; set; }

        public int currentHeatIndex { get; set; }

        public int currentDanceIndex { get; set; }

        public string battery { get; set; }
    }

    public class CoupleScore
    {
        public string couple { get; set; }

        public JS3Score scoreA { get; set; }

        public JS3Score scoreB { get; set; }
    }

    public class Heat
    {
        public int id { get; set; }

        public string name { get; set; }

        public CoupleScore[] coupleScores { get; set; }
    }

    public class Dance
    {
        public string danceName { get; set; }

        public string danceShortName { get; set; }

        public Heat[] heats;

        public string componentName { get; set; }

        public int componentIndex { get; set; }
    }


    public class HeatData
    {
        public string competition { get; set; }

        public string judge { get; set; }

        public string name { get; set; }

        public ScoringType scoringType { get; set; }

        public string sign { get; set; }

        public Dance[] dances { get; set; }

    }

    public class JS3Score
    {
        public string couple { get; set; }

        public string judge { get; set; }

        public string dance { get; set; }

        public int component { get; set; }

        public double score { get; set; }        
    }


    public static class HeatDataFactory
    {

        public static HeatData GetHeatData(DataContextClass context, Judge judge)
        {
            var heatData = new HeatData()
            {
                // setup core data:
                name = judge.Name,
                competition = context.Event.Title,
                scoringType = ScoringType.js3,
                sign = judge.Sign,
                judge = judge.Name,
                dances = GetDances(context, judge)
            };
            // Fix the round names:
            foreach(var dance in heatData.dances)
            {
                var heatNo = 1;
                foreach(var heat in dance.heats)
                {
                    heat.name = "Heat " + heatNo;
                    heat.id = heatNo;
                    heatNo++;
                }
            }

            return heatData;
        }

        private static Dance[] GetDances(DataContextClass context, Judge judge)
        {
            var dances = new List<Dance>();

            foreach (var dance in context.Dances)
            {
                var danceDto = new Dance()
                {
                    danceName = dance.LongName,
                    danceShortName = dance.ShortName,
                    componentIndex = GetComponentIndex(context, dance, judge),
                    componentName = GetComponentName(context, dance, judge),
                    heats = GetHeats(context, dance, judge.Sign)
                };

                dances.Add(danceDto);
            }

            return dances.ToArray();
        }

        private static Heat[] GetHeats(DataContextClass context, Model.Dance dance, string judgeSign)
        {
            var heats = context.Heats[dance.ShortName];

            return heats.Select(heat => heat.Couples.Select(c => c.Id).ToArray())
                .Select(couples => new Heat()
                    {
                        name = "Heat Name to be fixed", coupleScores = GetCoupleScores(context, couples, dance, judgeSign)
                    }).ToArray();
        }

        private static CoupleScore[] GetCoupleScores(DataContextClass context, int[] couples, Model.Dance dance, string sign)
        {
            var result = new List<CoupleScore>();

            var judgeDrawings = context.JudgeDrawing[dance.ShortName];
            var heats = context.Heats[dance.ShortName];
            // find the heat index:
            var heat = heats.First(h => h.Couples.Any(c => c.Id == couples[0]));
            
            var judgeDrawing = judgeDrawings.First(j => j.Heat == heat && j.Judges.Any(j2 => j2.Sign == sign));

            foreach (var couple in couples)
            {
                var score = context.AllJudgements.FirstOrDefault(j => j.Dance == dance.ShortName && j.Couple == couple && j.Judge == sign);

                CoupleScore coupleScore;
                if(score != null)
                {
                    coupleScore = GetCoupleScoreFromExistingScore(score, couple, dance.ShortName, couples.Length == 1, judgeDrawing, sign);
                }
                else
                {
                    coupleScore = GetCoupleScoreWithNoScore(couple, dance.ShortName, couples.Length == 1, judgeDrawing, sign);
                }

                result.Add(coupleScore);
            }

            return result.ToArray();
        }

        private static CoupleScore GetCoupleScoreFromExistingScore(Judgements score, int couple, string dance, bool isSolo, JudgesDrawing judgesDrawing, string sign)
        {
            var result = new CoupleScore()
            {
                couple = couple.ToString(),
                scoreA = new JS3Score()
                {
                    score = judgesDrawing.Area == 1 || judgesDrawing.Area == 3 ? score.MarkA : score.MarkB,
                    component = judgesDrawing.Area == 1 || judgesDrawing.Area == 3 ? 1 : 2,
                    couple = couple.ToString(),
                    dance = dance,
                    judge = sign
                },
                scoreB = !isSolo ? null : new JS3Score()
                {
                    score = judgesDrawing.Area == 1 || judgesDrawing.Area == 3 ? score.MarkC : score.MarkD,
                    component = judgesDrawing.Area == 1 || judgesDrawing.Area == 3 ? 3 : 4,
                    couple = couple.ToString(),
                    dance = dance,
                    judge = sign
                }

            };

            return result;
        }

        private static CoupleScore GetCoupleScoreWithNoScore(int couple, string dance, bool isSolo, JudgesDrawing judgesDrawing, string sign)
        {
            var result = new CoupleScore()
            {
                couple = couple.ToString(),
                scoreA = new JS3Score()
                {
                    score = -1,
                    component = judgesDrawing.Area == 1 || judgesDrawing.Area == 3 ? 1 : 2,
                    couple = couple.ToString(),
                    dance = dance,
                    judge = sign
                },
                scoreB = !isSolo ? null : new JS3Score()
                {
                    score = -1,
                    component = judgesDrawing.Area == 1 || judgesDrawing.Area == 3 ? 3 : 4,
                    couple = couple.ToString(),
                    dance = dance,
                    judge = sign
                }

            };

            return result;
        }


        private static string GetComponentName(DataContextClass context, Model.Dance dance, Judge judge)
        {
            var drawings = context.JudgeDrawing[dance.ShortName];
            var draw = drawings.First(d => d.Judges.Contains(judge));

            return context.JudgingAreas[draw.Area - 1].Component;
        }

        private static int GetComponentIndex(DataContextClass context, Model.Dance dance, Judge judge)
        {
            var drawings = context.JudgeDrawing[dance.ShortName];
            var draw = drawings.First(d => d.Judges.Contains(judge));

            return draw.Area;
        }

        public static HeatData GetHeatData()
        {
            var data = new HeatData
            {
                competition = "Test Competition",
                scoringType = ScoringType.js3,
                name = "Peter Mueller",
                sign = "AB",
                dances = new Dance[2]
                {
                    new Dance()
                    {
                        componentName = "MM",
                        componentIndex = 3,
                        danceName = "Slow Waltz",
                        danceShortName = "SW",
                        heats = new Heat[2]
                        {
                            new Heat()
                            {
                                name = "1. SW",
                                coupleScores = GetCoupleScores(new int[]{ 1, 2, 3, 4 }, "SW")
                            },
                            new Heat()
                            {
                                name = "2. SW",
                                coupleScores = GetCoupleScores(new int[]{ 5, 6, 7, 8 }, "SW")
                            }
                        }
                    },
                    new Dance()
                    {
                        componentName = "TQ",
                        componentIndex = 1,
                        danceName = "Tango",
                        danceShortName = "TG",
                        heats = new Heat[2]
                        {
                            new Heat()
                            {
                                name = "1. TG",
                                coupleScores = GetCoupleScores(new int[]{ 1, 2, 3, 4 }, "TG")
                            },
                            new Heat()
                            {
                                name = "2. SW",
                                coupleScores = GetCoupleScores(new int[]{ 5, 6, 7, 8 }, "TG")
                            }
                        }
                    },
                }
            };


            return data;
        }

        private static CoupleScore[] GetCoupleScores(int[] couples, string dance)
        {
            var result = new CoupleScore[couples.Length];

            for(var i = 0; i < couples.Length; i++)
            {
                result[i] = new CoupleScore()
                {
                    couple = couples[i].ToString(),
                    scoreA = new JS3Score()
                    {
                        score = -1,
                        couple = couples[i].ToString(),
                        component = 1,
                        dance = dance
                    },
                    scoreB = null
                };
            }

            return result;
        }

        private static double[] GetInitializedArray(int size)
        {
            var data = new double[size];

            for (var i = 0; i < size; i++)
            {
                data[i] = -1;
            }

            return data;
        }
    }
}

