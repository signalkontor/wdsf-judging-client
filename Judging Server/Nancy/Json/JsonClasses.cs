﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Nancy.Json
{
    class JsonClasses
    {
    }


    public class state
    {
        public string sign { get; set; }
        public string dance { get; set; }
        public string heat { get; set; }
        public string component { get; set; }
        public string battery { get; set; }
    }


    public class RoundData
    {
        public string judge { get; set; }
        public string sign { get; set; }
        public string competition { get; set; }
        public Component[] components { get; set; }
        public Dance[] dances { get; set; }
        public Heat[] heats { get; set; }
    }

    public class Component
    {
        public string shortName { get; set; }
        public string description { get; set; }

        public double minScore { get; set; }

        public double maxScore { get; set; }
    }

    public class Dance
    {
        public string shortName { get; set; }
        public string longName { get; set; }
    }

    public class Heat
    {
        public string component { get; set; }

        public string dance { get; set; }
        public string couples { get; set; }
    }


    public class Results
    {
        public string sign { get; set; }
        public Result[] results { get; set; }
    }

    public class Result
    {
        public string couple { get; set; }
        public string component { get; set; }
        public string dance { get; set; }
        public string score { get; set; }
    }


}
