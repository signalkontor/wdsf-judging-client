﻿using System;
using System.Net;

namespace Judging_Server.Nancy
{
    class WebClientWithTimeOut : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 5 * 1000;
            return w;
        }
    }
}
