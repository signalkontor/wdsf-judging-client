﻿using System;
using System.Collections.Generic;
using System.Printing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace Scrutinus.Reports.BasicPrinting
{
    public abstract class AbstractPrinter
    {
        protected double HeaderHeigthInDots;

        protected FixedDocument Doc;

        public FixedDocument Document
        {
            get
            {
                return this.Doc;
            }
        }

        public double CurrentPageContentStart { get; set; }

        public int CurrentPageNumber { get; set; }

        public bool ShowPageNumbers { get; set; }

        public bool ShowComplexHeader { get; set; }

        public bool ShowSimpleHeader { get; set; }

        public bool ShowFederationHeader { get; set; }

        public string DocumentTitle { get; set; }

        public string CompetitionTitle { get; set; }

        public string RoundTitle { get; set; }

        public bool PrintFloor { get; set; }

        public string PlaceOfCompetition { get; set; }

        public string OrganizerOfCompetition { get; set; }

        public string ExecutorOfCompetition { get; set; }

        public string KindOfCompetition { get; set; }

        public string GroupAndClassOfCompetition { get; set; }

        public string SectionOfCompetition { get; set; }

        public double PageWidth { get; set; }

        public double PageHeight { get; set; }

        public double LeftPageMargin { get; set; }

        public double RightPageMargin { get; set; }

        public double TopPageMargin { get; set; }

        public double ButtomPageMargin { get; set; }

        public bool PrintLogo { get; set; }

        public double UseablePageWidthInDots
        {
            get
            {
                return DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
            }
        }

        public double UseablePageWidthInCm
        {
            get
            {
                return this.PageWidth - this.LeftPageMargin - this.RightPageMargin;
            }
        }

        /// <summary>
        /// Setzt Seitengröße auf A4 und erzeugt ein leeres Dokument
        /// </summary>
        public AbstractPrinter()
        {
            // Set default values
            this.PageHeight = 29.7;
            this.PageWidth = 21;
            this.LeftPageMargin = 1;
            this.RightPageMargin = 1;
            this.TopPageMargin = 1;
            this.ButtomPageMargin = 1.5;
        }

        public AbstractPrinter(bool landscapeMode)
            : this()
        {
            if (landscapeMode)
            {
                this.PageHeight = 21;
                this.PageWidth = 29.7;
            }
        }

        protected void CreateContent()
        {
            this.Doc = new FixedDocument();

            this.Doc.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);

            this.Doc.DocumentPaginator.PageSize = new Size(
                DotsFromCm(this.PageWidth),
                DotsFromCm(this.PageHeight));

            this.CreateCustomContent();

            this.FinalizeDocument();
        }

        protected abstract void CreateCustomContent();

        /// <summary>
        /// Wird aufgerufen nachdem eine Seite erzeugt wurde. Hier kann dann auf die Seite geschrieben werden
        /// wass auf jeder Seite drauf stehen soll wie z.B. eine Überschrift
        /// </summary>
        /// <param name="page"></param>
        protected abstract void AddCommenContent(FixedPage page);

        /// <summary>
        /// Erzeugt eine Seite im Standardlayout (Logo, Adresse, ...)
        /// </summary>
        /// <returns></returns>
        public FixedPage CreatePage()
        {
            this.CurrentPageNumber++;

            PageContent page = new PageContent();
            FixedPage fixedPage = new FixedPage();

            this.HeaderHeigthInDots = 0;

            ((IAddChild)page).AddChild(fixedPage);
            this.Doc.Pages.Add(page);
            fixedPage.Background = Brushes.White;

            

            if (HeaderHeigthInDots == 0)
            {
                this.HeaderHeigthInDots = DotsFromCm(this.TopPageMargin);    
            }

            

            return fixedPage;
        }

        protected void AddFloor(FixedPage page, string floor)
        {
            var border = this.AddText(
                page,
                floor,
                7,
                this.LeftPageMargin,
                300,
                PageWidth - LeftPageMargin - RightPageMargin,
                TextAlignment.Center,
                FontWeights.Bold,
                0) as Border;

            var element = border.Child as TextBlock;
            element.Foreground = Brushes.LightGray;
        }

        protected void MeasurePage(FixedPage page)
        {
            var pageSize = this.Doc.DocumentPaginator.PageSize;
            page.Measure(pageSize);
            page.Arrange(new Rect(new Point(), pageSize));
            page.UpdateLayout();
        }

        private TextBlock AddTextToGrid(
            Grid grid,
            string text,
            FontWeight fontWeight,
            int fontSize,
            int row,
            int column,
            HorizontalAlignment horizontalAlignment
            )
        {
            var textBlock = this.CreateTextBlock(text, fontSize, TextAlignment.Left, fontWeight);
            textBlock.HorizontalAlignment = horizontalAlignment;
            Grid.SetColumn(textBlock, column);
            Grid.SetRow(textBlock, row);
            grid.Children.Add(textBlock);

            return textBlock;
        }

        protected bool ControlFitsOnPage(FixedPage page, FrameworkElement control)
        {
            var maxPageSize = this.Document.DocumentPaginator.PageSize.Height - DotsFromCm(this.TopPageMargin)
                              - DotsFromCm(this.ButtomPageMargin);

            
            this.MeasurePage(page);

            return this.HeaderHeigthInDots + control.ActualHeight < maxPageSize;
        }


        protected FixedPage AddControlToPage(FixedPage page, FrameworkElement control)
        {
            return this.AddControlToPage(page, control, this.LeftPageMargin, CmFromDots(this.HeaderHeigthInDots));
        }

        protected FixedPage AddControlToPage(FixedPage page, FrameworkElement control, double left, double top)
        {
            FixedPage.SetLeft(control, DotsFromCm(left)); // left margin
            FixedPage.SetTop(control, DotsFromCm(top)); // top margin
            page.Children.Add(control);

            if (!this.ControlFitsOnPage(page, control))
            {
                page.Children.Remove(control);
                page = this.CreatePage();
                FixedPage.SetLeft(control, DotsFromCm(this.LeftPageMargin)); // left margin
                FixedPage.SetTop(control, HeaderHeigthInDots); // top margin
                page.Children.Add(control);
            }

            this.HeaderHeigthInDots += control.ActualHeight;

            return page;
        }

        protected void AddDtvHeader(FixedPage page)
        {
            this.HeaderHeigthInDots = DotsFromCm(1);

            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.Width = DotsFromCm(this.PageWidth) - DotsFromCm(this.LeftPageMargin)
             - DotsFromCm(this.RightPageMargin);

            this.AddTextToGrid(grid, "DTV", FontWeights.Bold, 30, 0, 0, HorizontalAlignment.Left);
            this.AddTextToGrid(grid, "Turnierbericht Teil 1", FontWeights.Bold, 20, 0, 1, HorizontalAlignment.Center);

            this.AddTextToGrid(grid, DateTime.Now.ToShortDateString(), FontWeights.Normal, 12, 0, 2, HorizontalAlignment.Right);
            
            this.AddControlToPage(page, grid);

            this.HeaderHeigthInDots += 30;
        }

        

        protected void InsertVerticalLine(FixedPage page)
        {
            this.AddHorizontalLine(
                page,
                CmFromDots(this.HeaderHeigthInDots),
                this.LeftPageMargin,
                this.UseablePageWidthInCm);

            this.HeaderHeigthInDots += 10;
        }

        public static double DotsFromCm(double centimeter)
        {
            return centimeter * 96 / 2.54;
        }

        public static double CmFromDots(double dots)
        {
            return dots * 2.54 / 96;
        }

        protected FrameworkElement AddText(
            FixedPage Page,
            string Text,
            double Top,
            double Left,
            int FontSize,
            FontWeight fontWeight)
        {
            return this.AddText(Page, Text, Top, Left, FontSize, 0, TextAlignment.Left, fontWeight, 0);
        }

        /// <summary>
        /// Text einer Seite hinzufügen
        /// </summary>
        /// <param name="Page">Die Seire</param>
        /// <param name="Text">Text</param>
        /// <param name="Top">in Cm</param>
        /// <param name="Left">in Cm</param>
        /// <param name="FontSize">in Punkten</param>
        /// <param name="Width"></param>
        /// <param name="alignment"></param>
        protected FrameworkElement AddText(
            FixedPage Page,
            string Text,
            double Top,
            double Left,
            int FontSize,
            double Width,
            TextAlignment alignment)
        {
            return this.AddText(Page, Text, Top, Left, FontSize, Width, alignment, FontWeights.Normal, 0);
        }

        /// <summary>
        /// Fügt einen Text hinzu mit der Möglichkeit eines Rahmnens
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Text"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="FontSize"></param>
        /// <param name="Width"></param>
        /// <param name="alignment"></param>
        /// <param name="Weight"></param>
        /// <param name="Border"></param>
        protected FrameworkElement AddText(
            FixedPage Page,
            string Text,
            double Top,
            double Left,
            int FontSize,
            double Width,
            TextAlignment alignment,
            FontWeight Weight,
            int Border)
        {
            var text = this.CreateTextBlock(Text, FontSize, alignment, Weight, Width);

            if (Border > 0)
            {
                text.Padding = new Thickness(4, 2, 2, 2);
            }

            Border b = new Border();
            b.BorderThickness = new Thickness(Border);
            b.BorderBrush = Brushes.Black;
            b.Child = text;
            FixedPage.SetLeft(b, DotsFromCm(Left)); // left margin
            FixedPage.SetTop(b, DotsFromCm(Top)); // top margin
            Page.Children.Add(b);

            return b;
        }

        protected TextBlock CreateTextBlock(
            string Text,
            int FontSize,
            TextAlignment alignment,
            FontWeight Weight,
            double width = 0)
        {
            TextBlock text = new TextBlock();
            text.TextWrapping = TextWrapping.Wrap;
            text.Text = Text;
            text.FontSize = FontSize;
            text.FontFamily = new FontFamily("Arial");
            text.TextAlignment = alignment;
            text.FontWeight = Weight;

            if (width > 0)
            {
                text.Width = DotsFromCm(width);
            }

            return text;
        }

        /// <summary>
        /// Fügt einen Rahmen hinzu
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Heigh"></param>
        /// <param name="Width"></param>
        protected Border AddBox(FixedPage Page, double Top, double Left, double Height, double Width, Brush background)
        {
            Border b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Background = background;
            b.Width = DotsFromCm(Width);
            b.Height = DotsFromCm(Height);
            FixedPage.SetLeft(b, DotsFromCm(Left)); // left margin
            FixedPage.SetTop(b, DotsFromCm(Top)); // top margin
            Page.Children.Add(b);
            return b;
        }

        /// <summary>
        /// Fügt eine horizontale Linie hinzu
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="TopInCm"></param>
        /// <param name="LeftInCm"></param>
        /// <param name="WidthInCm"></param>
        protected UIElement AddHorizontalLine(FixedPage Page, double TopInCm, double LeftInCm, double WidthInCm)
        {
            Border b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Width = WidthInCm * 96 / 2.54;
            b.Height = 1;
            FixedPage.SetLeft(b, 96 * LeftInCm / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * TopInCm); // top margin
            Page.Children.Add((UIElement)b);

            return b;
        }

        /// <summary>
        /// Vertikale Linie
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Top"></param>
        /// <param name="Left"></param>
        /// <param name="Height"></param>
        protected UIElement AddVerticalLine(FixedPage Page, double Top, double Left, double Height)
        {
            System.Windows.Controls.Border b = new Border();
            b.BorderThickness = new Thickness(1);
            b.BorderBrush = Brushes.Black;
            b.Height = Height * 96 / 2.54;
            b.Width = 1;
            FixedPage.SetLeft(b, 96 * Left / 2.54); // left margin
            FixedPage.SetTop(b, 96 / 2.54 * Top); // top margin
            Page.Children.Add((UIElement)b);

            return b;
        }

        protected FrameworkElement AddText(FixedPage Page, string Text, double Top, double Left, int FontSize)
        {
            return this.AddText(Page, Text, Top, Left, FontSize, this.UseablePageWidthInCm, TextAlignment.Left);
        }

        /// <summary>
        /// Wird unmittelbar vor dem Druck aufgerufen und fügt beispielsweise Seitennummern hinzu
        /// </summary>
        protected void FinalizeDocument()
        {
            foreach (var page in this.Document.Pages)
            {
                var content = page.Child as FixedPage;

                var textblocks = FindChildren<TextBlock>(content);

                foreach (var textblock in textblocks)
                {
                    textblock.Text = textblock.Text.Replace("[Pages]", this.Document.Pages.Count.ToString());
                }

                this.MeasurePage(content);
            }
        }

        public static IEnumerable<T> FindChildren<T>(DependencyObject source, bool forceUsingTheVisualTreeHelper = false) where T : DependencyObject
        {
            if (source != null)
            {
                IEnumerable<DependencyObject> childs = GetChildObjects(source, forceUsingTheVisualTreeHelper);
                foreach (DependencyObject source1 in childs)
                {
                    if (source1 != null && source1 is T)
                        yield return (T)source1;
                    foreach (T obj in FindChildren<T>(source1, false))
                        yield return obj;
                }
            }
        }

        public static IEnumerable<DependencyObject> GetChildObjects(DependencyObject parent, bool forceUsingTheVisualTreeHelper = false)
        {
            if (parent != null)
            {
                if (!forceUsingTheVisualTreeHelper && (parent is ContentElement || parent is FrameworkElement))
                {
                    foreach (object obj in LogicalTreeHelper.GetChildren(parent))
                    {
                        DependencyObject depObj = obj as DependencyObject;
                        if (depObj != null)
                            yield return (DependencyObject)obj;
                    }
                }
                else
                {
                    int count = VisualTreeHelper.GetChildrenCount(parent);
                    for (int i = 0; i < count; ++i)
                        yield return VisualTreeHelper.GetChild(parent, i);
                }
            }
        }

        /// <summary>
        /// Druckt die Datei aus bzw. erzeugt eine XPS Datei
        /// </summary>
        /// <param name="toFile">Dateinamen, wenn null wird das Dokument zum Standardrucker gesendet</param>
        /// <param name="printTitle"></param>
        public void PrintDocument(bool showPrintDlg, PrintQueue printQueue, PageOrientation orientation, int copies, string printTitle)
        {
            this.FinalizeDocument();

            var prndlg = new PrintDialog();
    
            prndlg.PrintTicket.PageOrientation = orientation;
            prndlg.PrintTicket.CopyCount = copies;

            if (showPrintDlg)
            {
                if (prndlg.ShowDialog().Value == false)
                {
                    return;
                }
            }
            else
            {
                prndlg.PrintQueue = printQueue;               
            }

            prndlg.PrintDocument(this.Doc.DocumentPaginator, printTitle);
            
        }
    }
}