﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Judging_Server.Print.Controls
{
    /// <summary>
    /// Interaction logic for JudgingSheetControl.xaml
    /// </summary>
    public partial class JudgingSheetControl : UserControl
    {
        public JudgingSheetControl(string couple, int component, string sign)
        {
            this.Couple = couple;
            this.Sign = sign;

            switch (component)
            {
                case 1: this.Component = "TQ";
                    break;
                case 2:
                    this.Component = "MM";
                    break;
                case 3:
                    this.Component = "TS";
                    break;
                case 4:
                    this.Component = "CP";
                    break;
            }

            InitializeComponent();
        }

        public string Couple { get; set; }

        public string Component { get; set; }

        public string Sign { get; set; }
    }
}
