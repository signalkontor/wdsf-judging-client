﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using Judging_Server.Model;
using Judging_Server.Print.Controls;
using Scrutinus.Reports.BasicPrinting;

namespace Judging_Server.Print
{
    public class JudgingSheetPrinter : AbstractPrinter
    {
        private DataContextClass context;

        private FixedPage page;

        public JudgingSheetPrinter(DataContextClass context)
        {
            this.context = context;

            this.CreateContent();
        }

        protected override void CreateCustomContent()
        {
            this.page = CreatePage();

            var drawings = new List<JudgesDrawing>();

            foreach (var drawing in this.context.JudgeDrawing.Values)
            {
                drawings.AddRange(drawing);
            }

            foreach (var judge in this.context.Judges)
            {
                var drawing = drawings.Where(d => d.Judges.Any(j => j.Sign == judge.Sign)).OrderBy(h => h.Heat.Id);

                PrintJudgingSheet(drawing, judge.Sign);
            }
        }

        private void PrintJudgingSheet(IEnumerable<JudgesDrawing> drawing, string sign)
        {
            foreach (var judgesDrawing in drawing)
            {
                var control = new JudgingSheetControl(judgesDrawing.Heat.Couples.First().Id.ToString(), judgesDrawing.Area, sign);
                this.page = this.AddControlToPage(page, control);
                this.HeaderHeigthInDots += 20;
                this.AddHorizontalLine(this.page, CmFromDots(this.HeaderHeigthInDots),
                    this.LeftPageMargin,
                    this.PageWidth - this.LeftPageMargin - this.RightPageMargin);
                this.HeaderHeigthInDots += 20;
            }
        }

        protected override void AddCommenContent(FixedPage page)
        {
            
        }
    }
}
