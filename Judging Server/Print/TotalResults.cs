﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Judging_Server.Model;

namespace Judging_Server.Print
{
    class TotalResults
    {
        private Model.DataContextClass context;

        public void PrintPage(Model.DataContextClass context, string filename = null)
        {
            this.context = context;
            // Now generate the html file
            var result = GenerateHtml();
            // Save this and open Browser
            if(filename == null)
                filename = System.IO.Path.GetTempFileName().Replace(".tmp", ".html");
            var sw = new System.IO.StreamWriter(filename);
            sw.WriteLine(result);
            sw.Close();
            // Now launch Browser ...
            var p = new Process();
            p.StartInfo.FileName = "file:///" + filename;
            p.Start();
        }

        private string GenerateHtml()
        {
            var templateName = context.Dances.Count > 1 ? "ResultPageTemplate.htm" : "ResultPageSingleDance.htm";
            var template = LoadTemplate(templateName);
            // @Title@
            template = template.Replace("@Title@", context.Event.Title);
            // Replace Dances
            for (int i = 0; i < context.Dances.Count; i++)
            {
                template = template.Replace("@Dance" + (i + 1) + "@", context.Dances[i].LongName);
            }
            // Now generate the results of the competitors
            var couples = context.Couples.OrderBy(c => c.Id).ToList();
            string datatemplate = "";
            string resultTable = "";

            bool alternate = true;

            foreach (var couple in couples)
            {
                alternate = !alternate;
                var datatemplateName = context.Dances.Count > 1 ? "FirstLineTemplate.htm" : "FirstLineSingleDance.htm";
                datatemplate = LoadTemplate(datatemplateName);
                // Replace number of judges
                datatemplate = datatemplate.Replace("@rowclass@", alternate ? "Alternate" : "");
                datatemplate = datatemplate.Replace("@JudgeNumber@", context.Judges.Count.ToString());
                datatemplate = datatemplate.Replace("@Judge@", context.Judges[0].Sign);
                datatemplate = datatemplate.Replace("@Number@", couple.Id.ToString());
                datatemplate = datatemplate.Replace("@Couple@", couple.Name);
                var total = context.TotalResult.Single(t => t.Couple.Id == couple.Id);
                datatemplate = datatemplate.Replace("@Place@", total.Place.ToString());
                datatemplate = datatemplate.Replace("@Total@", total.Total.ToString("#0.00"));
                // Now replace the totals and markings
                for (int i = 0; i < context.Dances.Count; i++)
                {
                    var result = context.AllResults.Single(r => r.Couple.Id == couple.Id && r.Dance == context.Dances[i].ShortName);
                    datatemplate = datatemplate.Replace("@P" + (i + 1) + "@", result.TotalJudgeGetter(context.Judges[i].Sign).ToString("#0.00"));
                    datatemplate = datatemplate.Replace("@Total" + (i + 1) + "@", result.Total.ToString("#0.00"));
                    datatemplate = datatemplate.Replace("@RD" + (i + 1) + "@", result.ChairmanReduction.ToString("#0.00"));
                    datatemplate = datatemplate.Replace("@class" + (i + 1) + "@", "");
                }
                // Nun noch alle anderen Wertungen unten dranhängen
                for (int i = 1; i < context.Judges.Count; i++)
                {
                    string row = "<tr class='@class@'>\r\n<td>" +context.Judges[i].Sign + "</td>" ;
                    row = row.Replace("@class@", alternate ? "Alternate" : "");
                    for (int j = 0; j < context.Dances.Count; j++)
                    {
                        var result = context.AllResults.Single(r => r.Couple.Id == couple.Id && r.Dance == context.Dances[j].ShortName);
                        var state = (Model.States) result.StateofJudge(context.Judges[i].Sign);
                        row += "<td>";
                        row += result.TotalJudgeGetter(context.Judges[i].Sign) + "</td>";
                    }
                    row += "</tr>";
                    datatemplate += row + "\r\n";
                }
                resultTable += "\r\n" + datatemplate;
            }

            template = template.Replace("@Result@", resultTable);
            
            // wir erzeugen noch eine Liste von Judges ...
            string judges = "";
            foreach (var judge in context.Judges)
            {
                judges += judge.Sign + ": " + judge.Name + "<br/>";
            }
            template = template.Replace("@Judges@", judges);

            template = template.Replace("@AllJudgments@", GetAllJudgments());

            return template;
        }

        private string GetAllJudgments()
        {
            string html = "";
            for (int i = 0; i < context.Dances.Count; i++)
            {
                html += LoadTemplate("AllJudgments.htm");
                html = html.Replace("@Dance@", context.Dances[i].LongName);
                bool alternate = true;
                for (int j = 0; j < context.Couples.Count; j++)
                {

                    alternate = !alternate;

                    for (int x = 0; x < context.Judges.Count; x++)
                    {

                        // judgements[dance][judge][couple];
                        html += String.Format("<tr class='{0}'>", alternate ? "Alternate" : "" );

                        var judgment =
                            context.judgements[context.Dances[i].ShortName][context.Judges[x].Sign][
                                context.Couples[j].Id];
                        if (x == 0)
                        {
                            html += String.Format("<td rowspan='{0}'>{1}</td>\r\n", context.Judges.Count, context.Couples[j].Id);
                        }

                        // nun folgen die 5 Wertungsgebiete
                        html += "<td>" + judgment.Judge + "</td>\r\n";
                        var a = judgment.MarkA > -1 ? String.Format("{0:#0.00}", judgment.MarkA) : "";
                        var b = judgment.MarkB > -1 ? String.Format("{0:#0.00}", judgment.MarkB) : "";
                        var c = judgment.MarkC > -1 ? String.Format("{0:#0.00}", judgment.MarkC) : "";
                        var d = judgment.MarkD > -1 ? String.Format("{0:#0.00}", judgment.MarkD) : "";

                        html += String.Format("<td>{0}</td>\r\n<td>{1}</td>\r\n<td>{2}</td>\r\n<td>{3}</td>\r\n<td>{4}</td>\r\n\r\n", a,b,c,d, judgment.Total);

                        if (x == 0)
                        {
                            var res =
                                context.AllResults.Single(
                                    r => r.Couple.Id == context.Couples[j].Id && r.Dance == context.Dances[i].ShortName);
                            html += String.Format("<td rowspan='{0}'>{1}</td>\r\n", context.Judges.Count, res.ChairmanReduction);
                            html += String.Format("<td rowspan='{0}'>{1}</td>\r\n", context.Judges.Count, res.Total);
                        }

                        html += "</tr>";
                    }

                }
                html += "</table>\r\n";
                for (int z = 0; z < ServerSettings.Default.NoEmptyLinesResultPrintout;z++)
                {
                    html += "<br/>";
                }
            }
            return html;
        }

        private string LoadTemplate(string file)
        {
            return System.IO.File.ReadAllText("Html\\" + file);
        }
    }
}
