﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Judging_Server.Model;

namespace Judging_Server
{
    class RandomGenerator
    {
        List<Couple> Couples;
        List<Dance> Dances;
        List<Couple[]> Drawing;
        Random Rnd;

        public void PlaceCouple(Couple c, Couple[] placed, int start)
        {
            var free = new List<int>();

            for (int i = start; i < placed.Length; i++)
            {
                if (placed[i] == null) free.Add(i);
            }

            if (free.Count % 2 == 0)
                free.Reverse();

            if (free.Count == 1)
            {
                placed[free[0]] = c;
                return;
            }


            int index = Rnd.Next((free.Count - 1) * 2);
            while (index >= free.Count)
            {
                index = Rnd.Next((free.Count - 1) * 2);
            }
            placed[free[index]] = c;

        }

        public void Draw(List<Couple> couples, List<Dance> dances  )
        {
            Couples = couples;
            Dances = dances;
            // Drawing for first dance:
            Drawing = new List<Couple[]>();
            var list = new Couple[Couples.Count];
            Drawing.Add(list);
            foreach (var c in Couples)
            {
                PlaceCouple(c, list, 0);
            }
            // Now for all other Dances:
            for (int i = 1; i < Dances.Count; i++)
            {
                list = new Couple[Couples.Count];
                Drawing.Add(list);
                PlaceCouple(Drawing[i - 1][Couples.Count - 1], list, 2);
                PlaceCouple(Drawing[i - 1][Couples.Count - 2], list, 2);
                for (var x = 0; x < Couples.Count - 2; x++)
                {
                    PlaceCouple(Drawing[i - 1][x], list, 0);
                }
            }
        }

        public void Initialize()
        {
            Rnd = new Random();
        }

        public void Dump(string path)
        {
            var outS = new System.IO.StreamWriter(path + @"\Drawing.txt");
            // This is for printing
            for (int i = 0; i < Dances.Count; i++)
            {
                outS.Write(Dances[i].LongName + "\t");
                foreach (Couple c in Drawing[i])
                {
                    outS.Write(c.Id + "\t");
                }
                outS.WriteLine();
            }

            outS.Close();
            // We do the same for our data-file (copy + paste)
            outS = new System.IO.StreamWriter(path + @"\Drawing_data.txt");

            for (int i = 0; i < Dances.Count; i++)
            {
                outS.Write(Dances[i].ShortName + ";");
                foreach (Couple c in Drawing[i])
                {
                    outS.Write(c.Id + ";");
                }
                outS.WriteLine();
            }

            outS.Close();
        }
    }
}
