﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.ReportModels
{
    public class JudgesDrawingModel
    {
        public string Dance { get; set; }
        public int Heat { get; set; }
        public string Sign { get; set; }
        public string JudgeName { get; set; }
        public string Component { get; set; }
    }
}
