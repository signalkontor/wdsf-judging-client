﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace ManualEnterJudgements
{
    public class Judgements
    {
        public string Dance { get; set; }

        public string Judge { get; set; }

        public int Couple { get; set; }

        public int Component { get; set; }

        public double Mark { get; set; }

        public double Mark2 { get; set; }
    }

    public class ViewModel : ViewModelBase
    {
        private List<Judgements> judgements = new List<Judgements>();

        private List<int> couples = new List<int> { 4, 9, 6, 13, 10, 3, 7, 2, 8, 11, 1, 5, 12 };

        private int coupleIndex = 0;

        private string currentNumber;

        private double markA;

        private double markB;

        private double markC;

        private double markD;

        private double markE;

        private double markF;

        private double markG;

        private double markH;

        private double markI;

        private double markJ;

        private double markK;

        private double markL;

        private string baseDir;

        public ViewModel()
        {
            this.baseDir = @"C:\eJudge\Data\1234_1";
            this.LoadData(this.baseDir);
            this.ReadMarks();
            this.CurrentNumber = this.couples[this.coupleIndex].ToString();
            UpdateViewData();

            this.MoveNextCommand = new RelayCommand(this.MoveNext);
            this.MoveBackCommand = new RelayCommand(this.MoveBack);
        }

        public ICommand MoveNextCommand { get; set; }

        public ICommand MoveBackCommand { get; set; }

        public double MarkA
        {
            get
            {
                return this.markA;
            }
            set
            {
                this.markA = value;
                RaisePropertyChanged(nameof(MarkA));
            }
        }

        public double MarkB
        {
            get
            {
                return this.markB;
            }
            set
            {
                this.markB = value;
                RaisePropertyChanged(nameof(MarkB));
            }
        }

        public double MarkC
        {
            get
            {
                return this.markC;
            }
            set
            {
                this.markC = value;
                RaisePropertyChanged(nameof(MarkC));
            }
        }

        public double MarkD
        {
            get
            {
                return this.markD;
            }
            set
            {
                this.markD = value;
                RaisePropertyChanged(nameof(MarkD));
            }
        }

        public double MarkE
        {
            get
            {
                return this.markE;
            }
            set
            {
                this.markE = value;
                RaisePropertyChanged(nameof(MarkE));
            }
        }

        public double MarkF
        {
            get
            {
                return this.markF;
            }
            set
            {
                this.markF = value;
                RaisePropertyChanged(nameof(MarkF));
            }
        }

        public double MarkG
        {
            get
            {
                return this.markG;
            }
            set
            {
                this.markG = value;
                RaisePropertyChanged(nameof(MarkG));
            }
        }

        public double MarkH
        {
            get
            {
                return this.markH;
            }
            set
            {
                this.markH = value;
                RaisePropertyChanged(nameof(MarkH));
            }
        }

        public double MarkI
        {
            get
            {
                return this.markI;
            }
            set
            {
                this.markI = value;
                RaisePropertyChanged(nameof(MarkI));
            }
        }

        public double MarkJ
        {
            get
            {
                return this.markJ;
            }
            set
            {
                this.markJ = value;
                RaisePropertyChanged(nameof(MarkJ));
            }
        }

        public double MarkK
        {
            get
            {
                return this.markK;
            }
            set
            {
                this.markK = value;
                RaisePropertyChanged(nameof(MarkK));
            }
        }

        public double MarkL
        {
            get
            {
                return this.markL;
                
            }
            set
            {
                this.markL = value;
                RaisePropertyChanged(nameof(MarkL));
            }
        }

        public string CurrentNumber
        {
            get { return this.currentNumber; }
            set
            {
                this.currentNumber = value;
                RaisePropertyChanged(nameof(this.CurrentNumber));
            }
        }

        private void LoadData(string folder)
        {        
            // Load the judges drawing to get the judges and components
            var stream = new StreamReader(folder + "\\JudgesDrawing.txt");
            while (!stream.EndOfStream)
            {
                var data = stream.ReadLine().Split(';');
                var dance = data[0];
                var count = int.Parse(data[1]);
                var index = 2;

                for (int i = 0; i < count; i++)
                {
                    var heatIndex = Int32.Parse(data[index]);
                    var couple = couples[heatIndex - 1];
                    var component = data[index + 1];
                    var numberJudges = int.Parse(data[index + 2]);

                    for (int j = 0; j < numberJudges; j++)
                    {
                        var judgement = new Judgements()
                        {
                            Judge = data[index + 3 + j],
                            Component = int.Parse(component),
                            Couple = couple,
                            Dance = dance,
                            Mark = -1,
                            Mark2 = -1
                        };
                        this.judgements.Add(judgement);
                    }
                    index += 3 + numberJudges;
                }
            }

            stream.Close();
        }

        private void MoveNext()
        {
            UpdateMarking("A", this.MarkA);
            UpdateMarking("B", this.MarkB);
            UpdateMarking("C", this.MarkC);
            UpdateMarking("D", this.MarkD);
            UpdateMarking("E", this.MarkE);
            UpdateMarking("F", this.MarkF);
            UpdateMarking("G", this.MarkG);
            UpdateMarking("H", this.MarkH);
            UpdateMarking("I", this.MarkI);
            UpdateMarking("J", this.MarkJ);
            UpdateMarking("K", this.MarkK);
            UpdateMarking("L", this.MarkL);

            this.SaveData();

            if (this.coupleIndex < this.couples.Count - 1)
            {
                this.coupleIndex++;
                this.CurrentNumber = this.couples[this.coupleIndex].ToString();
                UpdateViewData();
            }
        }

        private void MoveBack()
        {
            UpdateMarking("A", this.MarkA);
            UpdateMarking("B", this.MarkB);
            UpdateMarking("C", this.MarkC);
            UpdateMarking("D", this.MarkD);
            UpdateMarking("E", this.MarkE);
            UpdateMarking("F", this.MarkF);
            UpdateMarking("G", this.MarkG);
            UpdateMarking("H", this.MarkH);
            UpdateMarking("I", this.MarkI);
            UpdateMarking("J", this.MarkJ);
            UpdateMarking("K", this.MarkK);
            UpdateMarking("L", this.MarkL);

            this.SaveData();

            if (this.coupleIndex > 0)
            {
                this.coupleIndex--;
                this.CurrentNumber = this.couples[this.coupleIndex].ToString();
                UpdateViewData();
            }
        }

        private void SaveData()
        {
            var byJudges = this.judgements.GroupBy(g => g.Judge);

            foreach (var group in byJudges)
            {
                var ordered = group.OrderBy(j => j.Couple);

                var stream = new StreamWriter($"{this.baseDir}\\Result_{group.Key}.txt");

                foreach (var judgement in ordered)
                {
                    stream.WriteLine(
                        $"{group.Key};{judgement.Dance};{judgement.Couple};{judgement.Component};{judgement.Mark}");

                }
                stream.Close();
            }
        }

        private void ReadMarks()
        {
            var byJudges = this.judgements.GroupBy(g => g.Judge);
            foreach (var group in byJudges)
            {
                var file = $"{this.baseDir}\\Result_{group.Key}.txt";

                if (!File.Exists(file))
                {
                    continue;
                }

                var stream = new StreamReader(file);
                while (!stream.EndOfStream)
                {
                    var data = stream.ReadLine().Split(';');
                    var judgement = this.judgements.First(j => j.Judge == group.Key && j.Couple == int.Parse(data[2]));
                    judgement.Mark = double.Parse(data[4]);
                }
            }
        }

        private void UpdateMarking(string judge, double mark)
        {
            var judgement = this.judgements.First(j => j.Judge == judge && j.Couple == this.couples[this.coupleIndex]);
            judgement.Mark = mark;
        }

        private void UpdateViewData()
        {
            var judgement = this.judgements.First(j => j.Judge == "A" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkA = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "B" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkB = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "C" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkC = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "D" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkD = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "E" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkE = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "F" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkF = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "G" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkG = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "H" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkH = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "I" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkI = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "J" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkJ = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "K" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkK = judgement.Mark;

            judgement = this.judgements.First(j => j.Judge == "L" && j.Couple == this.couples[this.coupleIndex]);
            this.MarkL = judgement.Mark;

        }
    }
}
