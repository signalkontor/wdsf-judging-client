﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monitor_Preview
{
    public class ScreenViewModel
    {
        public int Index { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.Screens = new ObservableCollection<ScreenViewModel>();
            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {
                this.Screens.Add(new ScreenViewModel() { Index = i, Name = Screen.AllScreens[i].DeviceName });
            }

            this.SelectedScreen = Screens.FirstOrDefault();

            this.InitializeComponent();

            var thread = new Thread(this.PreviewThread);

            if (Screen.AllScreens.Count() == 1)
            {
                System.Windows.MessageBox.Show("Only one screen found");
                return;
            }

            

            thread.IsBackground = true;
            thread.Start();
        }

        public ObservableCollection<ScreenViewModel> Screens { get; set; } 

        public ScreenViewModel SelectedScreen { get; set; }

        void PreviewThread()
        {
            var screenIndex = 1;

            while (true)
            {
                if (this.SelectedScreen == null)
                {
                    Thread.Sleep(1000);
                    continue;
                }

                var bitmap = new Bitmap(Screen.AllScreens[SelectedScreen.Index].Bounds.Width, Screen.AllScreens[SelectedScreen.Index].Bounds.Height);

                //var bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

                var graphics = Graphics.FromImage(bitmap);
                // Screen.AllScreens[SelectedScreen.Index].Bounds.Left
                graphics.CopyFromScreen(1920, Screen.AllScreens[SelectedScreen.Index].Bounds.Top, 0, 0, bitmap.Size);

                this.Dispatcher.Invoke(() => this.PreviewImage.Source = this.ConvertBitmapToBitmapImage(bitmap));

                Thread.Sleep(500);
            }
        }

        private BitmapImage ConvertBitmapToBitmapImage(Bitmap bitmap)
        {
            var memoryStream = new MemoryStream();
            bitmap.Save(memoryStream, ImageFormat.Png);
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
            bitmapImage.EndInit();

            return bitmapImage;
        }
    }
}
