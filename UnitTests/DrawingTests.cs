﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Judging_Server.Model;

namespace UnitTests
{
    [TestClass]
    public class DrawingTests
    {
        private Judging_Server.Model.DataContextClass _dataContext;
    
        [TestInitialize]
        public void FillData()
        {
            _dataContext = new DataContextClass();

            _dataContext.Couples = new List<Couple>();
            for (int i = 0; i < 25; i++)
            {
                _dataContext.Couples.Add(new Couple() { Country = "GER", Name = "Test", Id = i + 10 });
            }

            
            _dataContext.Dances = new List<Dance>
                {
                    new Dance() {LongName = "Slow Walz", ShortName = "SW"},
                    new Dance() {LongName = "Tango", ShortName = "TG"},
                    new Dance() {LongName = "Vinnes Waltz", ShortName = "VW"},
                    new Dance() {LongName = "Slow Fox", ShortName = "SF"},
                    new Dance() {LongName = "Quickstep", ShortName = "QS"}
                };
            // Lets add 12 judges
            _dataContext.Judges = new List<Judge>();
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "A" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "B" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "C" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "D" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "E" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "F" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "G" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "H" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "I" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "J" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "K" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "L" });
        }

        [TestMethod]
        public void Create()
        {
            // Create the drawing
            Judging_Server.Helper.IRoundDrawer drawer = new Judging_Server.Helper.CreateRoundDrawing();
            var res = drawer.CreateDrawing(_dataContext.Couples, _dataContext.Dances, 3);
            Assert.IsTrue(res["SW"][0].Couples.Count == 9);
            Assert.IsTrue(res["TG"][1].Couples.Count == 8);
            Assert.IsTrue(res["QS"][2].Couples.Count == 8);
            // Finale
            var cl = _dataContext.Couples.Take(6).ToList();
            res = drawer.CreateDrawing(cl, _dataContext.Dances, 6);
            // Check:
            Assert.IsTrue(res["SW"].Count == 6);
            Assert.IsTrue(res["SW"][0].Couples.Count == 1);
        }

        [TestMethod]
        public void CreateJudges()
        {
            var helper = new Judging_Server.Helper.CreateJudgesDrawing();
            var helper2 = new Judging_Server.Helper.CreateRoundDrawing();

            _dataContext.Heats = helper2.CreateDrawing(_dataContext.Couples, _dataContext.Dances, 2);

            var drawing = helper.Create(_dataContext.JudgingAreas, _dataContext.Judges, _dataContext.Heats, 3);

            Assert.IsTrue(drawing["SW"].Count == 8); // 4 Areas, 2 Heats = 8
            Assert.IsTrue(drawing["SW"][0].Judges.Count == 3);
            // Prüfen, dass der Judge nicht in der gleichen Heat mehrfach verwendet wird ...
            foreach (var judge in _dataContext.Judges)
            {
                var list = drawing["SW"].Where(c => c.Judges.Any(j => j.Sign == judge.Sign)).ToList();
                Assert.IsTrue(list.Count == 2);
                Assert.IsTrue(list[0].Heat.Id != list[1].Heat.Id);
            }
            
        }
    }
}
