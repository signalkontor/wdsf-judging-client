﻿using System;
using System.Collections.Generic;
using System.Linq;
using Judging_Server.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class Js3UnitTests
    {
        private Judge[] judges =
        {
            new Judge() {Sign = "A"},
            new Judge() {Sign = "B"},
            new Judge() {Sign = "C"},
            new Judge() {Sign = "D"},
            new Judge() {Sign = "E"},
            new Judge() {Sign = "F"},
            new Judge() {Sign = "G"},
            new Judge() {Sign = "H"},
            new Judge() {Sign = "I"},
            new Judge() {Sign = "J"},
            new Judge() {Sign = "K"},
            new Judge() {Sign = "L"},
        };

        private JudgingArea[] areas =
        {
            new JudgingArea() {Component = "TQ", Weight = 1},
            new JudgingArea() {Component = "MM", Weight = 1},
            new JudgingArea() {Component = "PS", Weight = 1},
            new JudgingArea() {Component = "CP", Weight = 1},
        };


        [TestMethod]
        public void GeneralCalculationTest()
        {
            var couple = new Couple() {Name = "Test", Id = 1};

            var sut = new DanceResultJs3(areas.ToList(), couple, "SW", judges.ToList(), false, 1.3, false);

            sut.UpdateByJudge("A", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("B", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("C", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("D", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("E", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("F", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("G", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("H", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("I", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("J", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("K", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("L", new Judgements() {MarkA = 4, MarkB = 4});

            Assert.IsTrue(sut.Result_A == 4);
            Assert.IsTrue(sut.Result_B == 4);
        }

        [TestMethod]
        public void TwoJudgesGetMaxedOut()
        {
            var couple = new Couple() {Name = "Test", Id = 1};

            var sut = new DanceResultJs3(areas.ToList(), couple, "SW", judges.ToList(), false,1.3, false);

            sut.UpdateByJudge("A", new Judgements() {MarkA = 2, MarkB = 2}); // Marks to low should get out
            sut.UpdateByJudge("B", new Judgements() {MarkA = 2, MarkB = 2}); // Marks to Low should get out
            sut.UpdateByJudge("C", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("D", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("E", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("F", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("G", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("H", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("I", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("J", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("K", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("L", new Judgements() {MarkA = 4, MarkB = 4});

            Assert.IsTrue(sut.Result_A == 4);
            Assert.IsTrue(sut.Result_B == 4);
        }

        [TestMethod]
        public void TwoJudgesAreIn()
        {
            var couple = new Couple() {Name = "Test", Id = 1};

            var sut = new DanceResultJs3(areas.ToList(), couple, "SW", judges.ToList(), false, 1.3, false);

            sut.UpdateByJudge("A", new Judgements() {MarkA = 2.5, MarkB = 2.5}); // Marks to low should get in
            sut.UpdateByJudge("B", new Judgements() {MarkA = 2.5, MarkB = 2.5}); // Marks to Low should get in
            sut.UpdateByJudge("C", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("D", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("E", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("F", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("G", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("H", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("I", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("J", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("K", new Judgements() {MarkA = 4, MarkB = 4});
            sut.UpdateByJudge("L", new Judgements() {MarkA = 4, MarkB = 4});

            Assert.IsTrue(Math.Abs(sut.Result_A - 3.75) < 0.01);
            Assert.IsTrue(Math.Abs(sut.Result_B - 3.75) < 0.01);
        }

        [TestMethod]
        public void AllJudgesOut()
        {
            var couple = new Couple() {Name = "Test", Id = 1};

            var sut = new DanceResultJs3(areas.ToList(), couple, "SW", judges.ToList(), false, 1.3, false);

            sut.UpdateByJudge("A", new Judgements() {MarkA = 2, MarkB = 2}); // Marks to low should get in
            sut.UpdateByJudge("B", new Judgements() {MarkA = 2, MarkB = 2}); // Marks to Low should get in
            sut.UpdateByJudge("C", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("D", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("E", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("F", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("G", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("H", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("I", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("J", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("K", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("L", new Judgements() {MarkA = 8, MarkB = 8});

            // Non of the judges is in our range, so the calculation should fall back on
            // using all judges
            Assert.IsTrue(Math.Abs(sut.Result_A - 5) < 0.01);
            Assert.IsTrue(Math.Abs(sut.Result_B - 5) < 0.01);
        }

        [TestMethod]
        public void TwoJudgesIn()
        {
            var couple = new Couple() {Name = "Test", Id = 1};

            var sut = new DanceResultJs3(areas.ToList(), couple, "SW", judges.ToList(), false, 0.8, false);

            sut.UpdateByJudge("A", new Judgements() {MarkA = 2, MarkB = 2}); // Marks to low should get in
            sut.UpdateByJudge("B", new Judgements() {MarkA = 2, MarkB = 2}); // Marks to Low should get in
            sut.UpdateByJudge("C", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("D", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("E", new Judgements() {MarkA = 2, MarkB = 2});
            sut.UpdateByJudge("F", new Judgements() {MarkA = 3, MarkB = 3});
            sut.UpdateByJudge("G", new Judgements() {MarkA = 3, MarkB = 3});
            sut.UpdateByJudge("H", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("I", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("J", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("K", new Judgements() {MarkA = 8, MarkB = 8});
            sut.UpdateByJudge("L", new Judgements() {MarkA = 8, MarkB = 8});

            // Whenn falling back in the accepted range
            // the two judges with the 3 should be in and the result 
            // should be 3 (while average is close to 5)
            Assert.IsTrue(Math.Abs(sut.Result_A - 3) < 0.01);
            Assert.IsTrue(Math.Abs(sut.Result_B - 3) < 0.01);
        }

        [TestMethod]
        public void TestShowdance()
        {
            var couple = new Couple() { Name = "Test", Id = 1 };

            var sut = new DanceResultJs3(areas.ToList(), couple, "SW", judges.ToList(), false, 0.8, true);

            sut.UpdateByJudge("A", new Judgements() { MarkA = 2, MarkB = 2, MarkC = 2, MarkD = 2}); // Marks to low should get in
            sut.UpdateByJudge("B", new Judgements() { MarkA = 2, MarkB = 2, MarkC = 2, MarkD = 2}); // Marks to Low should get in
            sut.UpdateByJudge("C", new Judgements() { MarkA = 2, MarkB = 2, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("D", new Judgements() { MarkA = 2, MarkB = 2, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("E", new Judgements() { MarkA = 2, MarkB = 2, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("F", new Judgements() { MarkA = 3, MarkB = 3, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("G", new Judgements() { MarkA = 3, MarkB = 3, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("H", new Judgements() { MarkA = 8, MarkB = 8, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("I", new Judgements() { MarkA = 8, MarkB = 8, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("J", new Judgements() { MarkA = 8, MarkB = 8, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("K", new Judgements() { MarkA = 8, MarkB = 8, MarkC = 2, MarkD = 2 });
            sut.UpdateByJudge("L", new Judgements() { MarkA = 8, MarkB = 8, MarkC = 2, MarkD = 2 });

            // When falling back in the accepted range
            // the two judges with the 3 should be in and the result 
            // should be 3 (while average is close to 5)
            Assert.IsTrue(Math.Abs(sut.Result_A - 3) < 0.01);
            Assert.IsTrue(Math.Abs(sut.Result_B - 3) < 0.01);
            Assert.IsTrue(Math.Abs(sut.Result_C - 2) < 0.01);
            Assert.IsTrue(Math.Abs(sut.Result_D - 2) < 0.01);
        }
    }
}
